\cleardoublepage

\chapter{EXPERIMENTACIÓN: WOLFSSL}
\label{section:wolfssl_pruebas}

A continuación se detalla la realización de una prueba de concepto de conexión post-cuántica basada en NTRU utilizando wolfSSL, y su posterior integración con el servicio HTTPS Apache.

Los experimentos realizados en este capítulo se resumen a continuación:

\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li Compilación, análisis y prueba de wolfSSL.
	\li Análisis y prueba de QSH en wolfSSL.
	\li Análisis y prueba de NTRU en wolfSSL.
	\li Integración, compilación y análisis de wolfSSL+NTRU con Apache.
	\li Integración y compilación de wolfSSL con OpenVPN.
\end{enumeratenospace}

\subsection*{Nota de obsolescencia}

Debe aclararse que los experimentos realizados a continuación se llevaron a cabo utilizando la versión 4.3.0 de wolfSSL, última disponible en su momento con soporte para NTRU. En Noviembre de 2021 wolfSSL liberó la versión 5.0.0 de su implementación, y entre los cambios relacionados con criptografía post-cuántica se encuentran los siguientes \citep{wolfssl_inc_wolfssl_2021}:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Integración con liboqs v0.7.0 con los algoritmos KEMs finalistas de la 3ra ronda de estandarización del NIST. Este soporte no estaba disponible al momento de realizar los experimentos.
	\li Integración de algoritmos híbridos basados en OQS y curvas NIST.
	\li Eliminación del soporte para algoritmos post-cuanticos NTRU y QSH.
\end{itemizenospace}

El último punto indica que, con las versiones de wolfSSL >5.0.0 no será posible reproducir los experimentos detallados a continuación. El capítulo decidió mantenerse igualmente dado que permite estudiar las negociaciones QSH/NTRU en un intercambio TLS, y NTRU aún se mantiene como uno de los algoritmos finalistas del proceso de estandarización que poseen mejor rendimiento.

Por otro lado, el presente capítulo concluye con la incorporación de criptografía post-cuántica NTRU en Apache para lograr un servicio HTTPS post-cuántico. Dicha integración fue posible gracias a la incorporación de QSH/NTRU en wolfSSL como se detalla a continuación.


\section{CONSIDERACIONES TÉCNICAS}

Los experimentos realizados con wolfSSL se llevaron a cabo en el mismo equipo virtual Lubuntu utilizado para las pruebas de OQS, y previamente descrito en la Sección \ref{section:techspec_oqs}. Se utilizaron tres directorios base:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li { \verb|/opt/wolfssl| para wolfSSL y su integración con NTRU y con Apache.}
	\li { \verb|/opt/wolfssl_openvpn| para la implementación de Openvpn integrada con wolfSSL.\textbf{(*)}}
	\li { \verb|/opt/keys| para la centralización de claves privadas, públicas y certificados digitales x509 utilizados durante las pruebas \textbf{(*)}.}
\end{itemizenospace}

Los experimentos de integración entre wolfSSL y NTRU con Apache se realizaron en el sistema virtual mencionado, mientras que los experimentos de VPN requirieron un segundo sistema que oficiara de cliente. Se utilizó el mismo sistema cliente descripto en la Sección \ref{section:techspec_oqs}. Los directorios marcados con (\textbf{*}) representan los elementos copiados a este sistema cliente.

\section{INTEGRACIÓN WOLFSSL+QSH/NTRU}
\label{section:wolfssl_experimentos}

\subsection{Compilación e instalación}

Para compilar wolfSSL con NTRU primero instaló la biblioteca NTRU en el sistema operativo. A tal propósito se creó el directorio \verb|/opt/wolfssl/|, dentro del mismo se clonó la librería NTRUEncrypt, se compiló e instaló en el sistema. Estos pasos se muestran en el Alg. \ref{alg:wolf_libntruencrypt}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Instalación de la librería NTRUEncrypt.},
	label={alg:wolf_libntruencrypt},
	%firstnumber=1,
	numbers=none,
	]
# Clonado de NTRUEncrypt:
mkdir /opt/wolfssl/ && cd /opt/wolfssl/
sudo git clone \
	https://github.com/NTRUOpenSourceProject/NTRUEncrypt.git

# Compilación e instalación de NTRUEncrypt:
cd NTRUEncrypt/
./autogen.sh
./configure
make && make install
\end{lstlisting}

De manera predeterminada la librería NTRUEncrypt se instaló en el directorio \verb|/usr/local/lib/| tal y como puede verse en la Fig. \ref{fig:libntru}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/libntru.png}
	\caption{Librería NTRU.}
	\label{fig:libntru}
\end{figure}

El siguiente paso fue compilar wolfSSL. Si bien al momento de realizar esta prueba se encuentra disponible la versión \verb|v4.4.0-stable|, la misma no se encuentra adaptada para su integración con NTRU, y produce errores de compilación. A raíz de esto se optó por utilizar la versión \verb|v4.3.0-stable| para poder llevar a cabo las pruebas. Como puede observarse en el Alg. \ref{alg:wolf_install}, el script \verb|./configure| recibió por argumento algunas opciones de interés:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li \verb|--enable-qsh|: Habilita QSH (véase Sección \ref{section:qsh}).
	\li \verb|--with-ntru=...|: Directorio donde se instaló NTRUEncrypt.
	\li \verb|--enable-apachehttpd|: Activa la integración con Apache (Sección \ref{section:wolfssl_https}).
\end{itemizenospace}


% (ver Fig. \ref{fig:wolfssl_4.4error})
\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Instalación.},
	label={alg:wolf_install},
	%firstnumber=1,
	numbers=none,
	]
# Clonado de wolfSSL:
cd /opt/wolfssl/
mkdir wolfssl-git/ && cd wolfssl-git
git clone https://github.com/wolfSSL/wolfssl.git

# Cambio de rama a v4.3.0-stable (compatible con NTRU):
cd wolfssl-git/ && git checkout v4.3.0-stable

# Compilación e instalación de wolfSSL:
./autogen.sh
./configure --enable-qsh --with-ntru=/usr/local/lib \
	-enable-opensslextra --enable-supportedcurves \
	--enable-apachehttpd --enable-tlsx --enable-all \
	--enable-ecc --enable-psk --enable-aesccm \
	C_EXTRA_FLAGS="-DWOLFSSL_STATIC_RSA -DHAVE_SNI"
make && make install
\end{lstlisting}

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolfssl_4.4error.png}
	\caption{\textbf{Errores de compilación wolfSSL v4.4.0-static con NTRU.}}
	\label{fig:wolfssl_4.4error}
\end{figure}

\end{comment}

Debe notarse también que se habilitó un flag especial \verb|WOLFSSL_STATIC_RSA| para forzar a wolfSSL a utilizar claves RSA estáticas en lugar de efímeras. Si bien esto es desaconsejado en servicios de producción, existen sistemas criptográficos como NTRU que requieren de claves estáticas para funcionar \citep{wolfsslinc.WolfSSLUserManual2019, wolfsslinc.GitHubWolfSSLWolfssl2020}.

\subsection{Negociación SSL/TLS}

wolfSSL incluye un servidor y un cliente de ejemplo para realizar la verificación de conexión y funcionamiento. Además, como se trata de una implementación de código abierto, los fuentes de estos ejemplos sirven para orientar el desarrollo de aplicaciones que hagan uso de SSL/TLS. La compilación de wolfSSL detallada en el apartado anterior generó los binarios ejecutables de servidor y cliente. Para ejecutar servidor y cliente de ejemplo se usaron los comandos mostrados en el Alg. \ref{alg:wolf_exec}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Ejecución de servidor y cliente.},
	label={alg:wolf_exec},
	%firstnumber=1,
	numbers=none,
	]
# En el equipo servidor:
cd /opt/wolfssl/wolfssl-git/ && ./examples/server/server
# En el equipo cliente:
cd /opt/wolfssl/wolfssl-git/ && ./examples/client/client
\end{lstlisting}

El servidor por defecto atiende en el puerto TCP 11111. La salida estándar del mismo, sin ninguna configuración adicional, se puede ver en la Fig. \ref{fig:wolf_server_simple}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_simple.png}
	\caption{Respuesta servidor wolfSSL negociación estándar.}
	\label{fig:wolf_server_simple}
\end{figure}

La interacción del cliente puede observarse en la Fig. \ref{fig:wolf_client_simple}. Como puede apreciarse en ambas capturas, la negociación SSL/TLS estándar utiliza TLS1.2, la cipher suite \verb|TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384|, y usa QSH.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_client_simple.jpg}
	\caption{Respuesta cliente wolfSSL negociación estándar.}
	\label{fig:wolf_client_simple}
\end{figure}

El cliente provee algunos comandos y opciones adicionales que pueden listarse con la opción \verb|-h|. Entre ellos se encuentra el modificador \verb|-e|, que permite listar las cipher suites soportadas. Como se ha realizado la compilación utilizando los flags \verb|NTRU| y \verb|QSH|, estos algoritmos se verán incluidos en la salida, así como también los algoritmos mencionados al principio de esta sección. Se usó el comando \verb|sed| para facilitar la lectura de la lista de cipher suites resultante reemplazando el caracter \verb|":"| por un salto de línea:% , como muestra el Alg. \ref{alg:wolf_client-e}

\begin{lstlisting}[
	language={Bash},
%	caption={wolfSSL - Listar ciphersuites cliente.},
	label={alg:wolf_client-e},
	%firstnumber=1,
	numbers=none,
	]
cd /opt/wolfssl/wolfssl-git
./examples/client/client -e|sed 's/:/\n/g'
\end{lstlisting}

Este comando lista todas las cipher suites soportadas por wolfSSL. Como el interés de este trabajo se centra en criptografía post-cuántica, a continuación se muestra un listado de las cipher suites que son de mayor importancia. El listado completo puede encontrarse en el repositorio Git que acompaña a esta tesis \citep{cordobaTesisMTIDiego2022}.

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li NTRU-RC4-SHA
	\li NTRU-DES-CBC3-SHA
	\li NTRU-AES128-SHA
	\li NTRU-AES256-SHA
	\li QSH
\end{itemizenospace}

La opción \verb|-l| del cliente permite configurar una cipher suite específica, o varias separadas por \verb|":"|. La negociación TLS selecciona una de las cipher suites ofrecidas por el cliente, y el resultado puede verse en la sección \verb|SSL-Session| de la salida por pantalla. El Alg. \ref{alg:wolf_client-l} ilustra este el caso de configurar tres cipher suites desde el lado del cliente. Se incluye también un extracto de la salida. Debido a que no se especificaron cipher suites post-cuánticas, dicha negociación no incluye algoritmos NTRU ni QSH.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Forzado de cipher suites.},
	label={alg:wolf_client-l},
	%firstnumber=1,
	numbers=none,
	]
# Ejecución del cliente:
./examples/client/client -l ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA

# Fragmento de la salida:
[...]
SSL-Session:
	Protocol  : TLSv1.2
	Cipher    : TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA
[...]
\end{lstlisting}

\subsection{Negociación QSH}

wolfSSL implementa la cipher suite QSH (ver Sección \ref{section:qsh}) para proveer algoritmos post-cuánticos en la negociación TLS. Con la intención de entender la implementación de wolfSSL en Cliente y Servidor, y para facilitar la identificación del uso de QSH durante el intercambio TLS, se modificó el servidor agregando la línea $469-470$ mostrada en el Alg. \ref{alg:wolf_serverqsh}. La Fig. \ref{fig:wolf_server_qshno} muestra la nueva salida del servidor luego de establecer la conexión.

\begin{lstlisting}[
	language={C},
	caption={wolfSSL. Comprobación QSH Servidor.},
	label={alg:wolf_serverqsh},
	firstnumber=466,
	%numbers=none,
	]
/* Archivo: examples/server/server.c */
if (ret > 0) {
	input[ret] = 0; /* null terminate message */
	printf("Quantum-Safe Handshake Ciphersuite?? -> %s\n",
		(wolfSSL_isQSH(ssl))?"Yes":"No");                   
	printf("Client message: %s\n", input);
}
\end{lstlisting}

\begin{comment}
	\todo{reshot!}
\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_qshcode.png}
	\caption{\textbf{Modificación del código del servidor para evaluar QSH.}}
	\label{fig:wolf_server_qshcode}
\end{figure}
\end{comment}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_qshno.png}
	\caption{Evaluando QSH en SSL/TLS. Caso negativo.}
	\label{fig:wolf_server_qshno}
\end{figure}

Ahora bien, si el cliente ofrece la cipher suite \verb|QSH|, el servidor la utiliza de manera adicional a una cipher suite tradicional. En el Alg. \ref{alg:wolf_clientqsh} se ve el comando ejecutado por el cliente, donde se agrega la cipher suite \verb|QSH|. Ahora el registro de conexión muestra el mensaje afirmativo para QSH (Fig. \ref{fig:wolf_server_qshyes}).

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Forzado de QSH en el cliente.},
	label={alg:wolf_clientqsh},
	%firstnumber=1,
	numbers=none,
	]
cd /opt/wolfssl/wolfssl-git
./examples/client/client -l ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA:QSH
\end{lstlisting}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_qshyes.png}
	\caption{Evaluando QSH en SSL/TLS. Caso positivo.}
	\label{fig:wolf_server_qshyes}
\end{figure}

Otras opciones interesantes y útiles que pueden utilizarse en el servidor para realizar pruebas son las siguientes:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li \textbf{-x}: Permite imprimir los mensajes de error sin cerrar la conexión.
	\li \textbf{-b}: Atiende el servicio en todas las interfaces de red, no sólo localhost.
	\li \textbf{-i}: Permite mantener el servidor activo para realizar múltiples conexiones desde los clientes.
	\li \textbf{-n}: Habilita el cifrado NTRU en SSL/TLS (ver más adelante).
	\li \textbf{-c}: Especifica la ruta al certificado digital a utilizar en la conexión.
	\li \textbf{-k}: Especifica la ruta a la clave privada a utilizar en la conexión.
\end{itemizenospace}

El cliente también acepta otras opciones que se documentan a continuación, aunque serán de utilidad más adelante:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li \textbf{-l}: Ya mencionada, permite especificar una cipher suite determinada.
	\li \textbf{-h}: Permite especificar, por ip o nombre, el host a conectar.
	\li \textbf{-p}: Permite especificar un puerto destino distinto de 11111.
	\li \textbf{-d}: Desactiva la verificación de par, lo que facilita las pruebas con certificados digitales autofirmados.
	\li \textbf{-g}: Envía un mensaje \verb|HTTP GET| al servidor, útil para https.
\end{itemizenospace}

\subsection{Negociación NTRU}

La suite wolfSSL provee, junto con sus archivos de código fuente y configuraciones, una serie de claves y certificados digitales almacenados en el directorio \verb|certs/| de la raíz del repositorio. Dentro del mismo se encuentran una clave y un certificado digital generados utilizando el algoritmo NTRU:

\begin{lstlisting}[
	language={Bash},
%	caption={},
%	label={alg:wolf_clientqsh},
	%firstnumber=1,
	numbers=none,
	]
certs/ntru-cert.pem		# Certificado digital
certs/ntru-key.raw		# Clave privada
\end{lstlisting}

El binario del servidor permite activar el algoritmo NTRU añadiendo la opción \verb|-n|. Las opciones \verb|-c| y \verb|-k| se utilizan para especificar certificado y clave respectivamente, como se ve en el Alg. \ref{alg:wolf_serverntru}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Servidro NTRU.},
	label={alg:wolf_serverntru},
	%firstnumber=1,
	numbers=none,
	]
cd /opt/wolfssl/wolfssl-git
./examples/server/server -c ./certs/ntru-cert.pem \
	-k ./certs/ntru-key.raw -n
\end{lstlisting}

Luego se lanzó el cliente sin especificar ningún parámetro adicional, y de manera predeterminada estableción la conexión TLS utilizando NTRU, como puede verse en la Fig. \ref{fig:wolf_ntru_cli}. El registro de la conexión en el servidor se ve en la Fig. \ref{fig:wolf_ntru_srv}.

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_ntru_cli.png}
	\caption{Negociación NTRU en TLS v1.2 - Cliente.}
	\label{fig:wolf_ntru_cli}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_ntru_srv.png}
	\caption{Negociación NTRU en TLS v1.2 - Servidor.}
	\label{fig:wolf_ntru_srv}
\end{figure}

Como puede verse en la salida del servidor (Fig. \ref{fig:wolf_ntru_srv}), al negociar una ciphersuite NTRU en SSL/TLS, no se activa QSH, un algoritmo que se habilitaba anteriormente de manera predeterminada (ver Figuras \ref{fig:wolf_server_simple} y \ref{fig:wolf_client_simple}).

Surgió en este punto la duda de si wolfSSL, al habilitar NTRU, desactiva QSH, o desactiva cualquier otra cipher suite distinta de NTRU. Se intentó conectar un cliente especificando una cipher suite estándar no NTRU, y el servidor rechazó la negociación SSL/TLS al no poder verificar la cipher suite, como puede verse en la Fig. \ref{fig:wolf_ntru_other}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_ntru_other.png}
	\caption{Servidor en modo NTRU recibiendo otras cipher suites.}
	\label{fig:wolf_ntru_other}
\end{figure}

Al invocarse al cliente sin utilizar ninguna cipher suite, se negocia NTRU con el servidor. Incluso si se fuerza la negociación de QSH junto con cipher suites NTRU, QSH no se habilita. Por ejemplo, si el cliente especifica la cipher suite \verb|NTRU-AES256-SHA:QSH|, el cifrador resultante de la negociación SSL/TLS será \verb|TLS_NTRU_RSA_WITH_AES_256_CBC_SHA|, que corresponde a la cipher suite propuesta por el cliente, omitiendo QSH, cipher suite que fue especificada explícitamente. El Alg. \ref{alg:wolf_serverntruoutput} representa un fragmento de la salida del servidor que ejemplifica el caso.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Salida NTRU del servidor.},
	label={alg:wolf_serverntruoutput},
	%firstnumber=1,
	numbers=none,
	]
SSL version is TLSv1.2
SSL cipher suite is TLS_NTRU_RSA_WITH_AES_256_CBC_SHA
SSL curve name is FFDHE_3072
Server Random : 7148D9E984C5C4C8A6958B701C93608744194A93B37D408DE986...
Quantum-Safe Handshake Ciphersuite?? -> No
\end{lstlisting}

Este comportamiento es debido a que wolfSSL activa la cipher suite QSH como un complemento resistente a ataques cuánticos junto con algoritmos tradicionales. NTRU ya dispone de un intercambio post-cuántico integrado en la librería, y se activa automáticamente al establecer la conexión.

La cipher suite QSH tiene mayor preferencia que otros protocolos comunes durante la selección de algoritmos. Si un usuario integra NTRU dentro de wolfSSL y ambos extremos de conexión soportan NTRU, entonces la cipher suite NTRU y su intercambio de claves seguro serán seleccionados de manera predeterminada. Sólo en el caso de que el usuario excluya explícitamente a NTRU se activa QSH y se combina con cipher suites tradicionales. El manual de usuario de wolfSSL \citep{wolfsslinc.WolfSSLUserManual2019} menciona que NTRU acelera el proceso de conexión SSL/TLS de 20 a 200 veces sobre un intercambio RSA, y la mejora se incrementa según aumenta el tamaño de la clave utilizada. Es decir, el proceso de negociación de SSL/TLS será más rápido con claves NTRU de 8192 bits que con claves de 1024 bits RSA.

Finalmente, debido a que NTRU no tiene una adopción masiva como ocurre con RSA, wolfSSL lo implementa en modo híbrido con otras cipher suites. Los desarrolladores de wolfSSL propusieron la inclusión de NTRU en modo híbrido dentro de QSH, y así lograr que TLS utilice pares de claves post-cuánticas NTRU por única vez, además de los pares de claves de algoritmos tradicionales \citep{whyteQuantumsafeHybridQSH2017, wolfsslQuantumSafetyWolfSSL2019}.


\begin{comment}
qsh hace uso de ntru para dar cifrado ntru al intercambio de claves, lo que agrega resistencia a ataques cuanticos
hay que explicar el qsh de la ietf, los diferentes mecanismos que dan resistencia cuantica, entre los que se encuentran ntru.
\end{comment}


