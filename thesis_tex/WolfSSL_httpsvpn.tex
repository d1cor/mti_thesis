%\cleardoublepage
\section{WOLFSSL Y SERVICIOS HTTPS: APACHE}
\label{section:wolfssl_https}

\begin{comment}
Integración WolfSSL con servicios Apache
Compilación
Adaptación de Apache/httpd para integrarlo con wolfssl
Testing y verificación
Prueba de concepto
Análisis de tráfico de red.

\end{comment}

Esta sección detalla los experimentos realizados integrando wolfSSL con Apache para montar un servidor HTTPS post-cuántico. Para llevar a cabo esta prueba de concepto fue necesario el código fuente de wolfSSL compilado convenientemente para permitir su integración, y una versión relativamente actualizada de Apache. Por recomendación de los desarrolladores de wolfSSL que colaboraron con su soporte, se utilizó la versión 2.4.42-dev (Unix) de Apache, descargada directamente desde el repositorio SVN del proyecto. Cabe aclarar que wolfSSL tiene soporte para su integración con versiones de Apache posteriores a la v2.4.0\citep{wolfsslWolfSSLApacheHttpd2020}.

Primeramente fue necesario instalar las librerías de parseo de archivos xml, necesarias para la compilación de Apache. Luego se descargó y compiló Apache desde su repositorio. Esto se realizó en el directorio \verb|/opt/wolfssl| creado a tal efecto. Los pasos pueden observarse en el Alg. \ref{alg:wolfapache_download}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y Apache. Dependencias y descarga.},
	label={alg:wolfapache_download},
	%firstnumber=1,
	numbers=none,
	]
# Instalación de dependencias:
sudo apt install libxml2 libxml2-dev

# Descarga de Apache httpd:
cd /opt/wolfssl/wolfssl-git/
svn checkout https://svn.apache.org/repos/asf/httpd/httpd/branches/2.4.x httpd-2.4.x/
cd httpd-2.4.x/
\end{lstlisting}

Dado que el motor SSL/TLS de Apache está desarrollado para su integración con OpenSSL, se debió modificar y adaptar su código para integrarlo con wolfSSL. Estas modificaciones se realizaron sobre el archivo de código fuente \verb|modules/ssl/ssl_engine_init.c|. Los cambios incluyeron modificaciones en los nombres de las funciones \verb|SSL_CTX_*| por su equivalente en wolfSSL, \verb|wolfSSL_CTX*|. En el repositorio que acompaña a la presente tesis se encuentra el archivo \verb|diff| y el \verb|ssl_engine_init.c| resultante.

wolfSSL implementa gran cantidad de funcionalidades SSL, de las cuales solo algunas fue necesario modificar en Apache para lograr la integración en la prueba de concepto básica. Para facilitar la identificación de estas funciones y su secuencia se utilizó como base el código del ejemplo \verb|server.c|. Se realizó la compilación utilizando el modificador \verb|-E| en \verb|gcc| como se muestra en el Alg. \ref{alg:wolfapache_server.i}, lo que permitió interpretar las directivas de preprocesamiento, y obtener el archivo \verb|server.i|, una versión final del código que implementa NTRU. Este archivo intermedio se encuentra disponible en el repositorio Git que acompaña al presente trabajo \citep{cordobaTesisMTIDiego2022}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL. Preprocesado del servidor de ejemplo.},
	label={alg:wolfapache_server.i},
	%firstnumber=1,
	numbers=none,
	]
cd /opt/wolfssl/wolfssl-git
gcc examples/server/server.c -o /tmp/server.i -I. -E
\end{lstlisting}

Además se agregaron dos líneas de depuración para verificar la lectura de la clave NTRU del servidor Apache. Este cambio puede verse en el Alg. \ref{alg:wolfapache_debug}.

\begin{lstlisting}[
	language={C},
	caption={wolfSSL y Apache. Salida NTRU del servidor.},
	label={alg:wolfapache_debug},
	firstnumber=1296,
	%numbers=none,
	]
/* Archivo: /etc/wolfssl/wolfssl-git/httpd-2.4.x/modules/ssl/ssl_engine_init.c */
if (wolfSSL_CTX_use_NTRUPrivateKey_file(mctx->ssl_ctx, keyfile) == WOLFSSL_SUCCESS)
	printf("DEBUG wolfSSL_CTX_use_NTRUPrivateKey_file SUCCESS!!!\n");
\end{lstlisting}

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_httpd_debug.png}
	\caption{\textbf{Código de verificación de NTRU en Apache.}}
	\label{fig:wolf_httpd_debug}
\end{figure}

\end{comment}

Luego de realizados estas modificaciones se procedió a generar los scripts de soporte para la compilación y posterior instalación de Apache. Esto se llevó a cabo mediante los comandos mostrados en el Alg. \ref{alg:wolfapache_buildconfinstall}

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y Apache. Compilación e instalación.},
	label={alg:wolfapache_buildconfinstall},
	%firstnumber=1,
	numbers=none,
	]
cd /etc/wolfssl/wolfssl-git/httpd-2.4.x
./buildconf
./configure \
	--enable-access_compat=shared --enable-actions=shared \
	--enable-alias=shared --enable-allowmethods=shared \
	--enable-auth_basic=shared --enable-authn_core=shared \
	--enable-authn_file=shared --enable-authz_core=shared \
	--enable-authz_groupfile=shared --enable-mime=shared \
	--enable-authz_host=shared --enable-authz_user=shared \
	--enable-autoindex=shared --enable-dir=shared \
	--enable-env=shared --enable-headers=shared \
	--enable-include=shared --enable-log_config=shared \
	--enable-negotiation=shared --enable-proxy=shared \
	--enable-proxy_http=shared --enable-rewrite=shared \
	--enable-setenvif=shared --enable-unixd=shared \
	--enable-ssl=shared --enable-ssl-staticlib-deps \
	--enable-mods-static=all --enable-static-ab \
	--with-included-apr --with-libxml2 \
	--with-wolfssl=/opt/wolfssl/wolfssl-git --enable-ssl
make && make install
\end{lstlisting}

La última línea de configuración invoca la opción \verb|--with-wolfssl|, que indica el directorio de los fuentes de wolfSSL que se utilizará para compilar Apache. Debe recordarse que esta integración es bidireccional, durante la compilación de wolfSSL también se especificó el soporte para su integración con https, tal y como se mencionó en la Sección \ref{section:wolfssl_experimentos}.

Los últimos dos comandos, \verb|make| y \verb|make install|, realizaron la compilación e instalación respectivamente. La ejecución fue satisfactoria y esta implementación de servidor HTTPS se instaló en el directorio predeterminado \verb|/usr/local/apache2|. En la Fig. \ref{fig:wolf_httpd_lib} puede verse la salida del comando \verb|ldd| que muestra al binario de Apache, \verb|httpd|, correctamente enlazado con las librerías de wolfSSL y NTRU.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_httpd_lib.png}
	\caption{Librerías compiladas en httpd.}
	\label{fig:wolf_httpd_lib}
\end{figure}

Luego se editó el archivo de configuración de Apache, \verb|conf/httpd.conf|, para habilitar el módulo SSL. Para ello se activó la línea que incluye la configuración del módulo \verb|httpd-ssl|. Además se activó la entrada de \verb|ServerName| para evitar advertencias en tiempo de ejecución como se ve en el Alg. \ref{alg:wolfapache_configssl}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y Apache. Configuraciones del servidor web.},
	label={alg:wolfapache_configssl},
	%firstnumber=1,
	numbers=none,
	]
# (Dentro de /etc/wolfssl/wolfssl-git/httpd-2.4.x)

# Archivo conf/httpd.conf:
ServerName www.juncotic.com:80
Include conf/extra/httpd-ssl.conf

# Archivo conf/extra/httpd-ssl.conf:
# SSLHonorCipherOrder on
SSLCertificateFile "/opt/keys/wolfssl/ntru-cert.pem"
SSLCertificateKeyFile "/opt/keys/wolfssl/ntru-key.raw"
\end{lstlisting}

El mismo Alg. \ref{alg:wolfapache_configssl} muestra la configuración de SSL/TLS de Apache, dentro del archivo \verb|conf/extra/httpd-ssl.conf|. Allí se editaron las rutas al certificado y clave privada provistos como ejemplo por wolfSSL, y se desactivó la opción \verb|SSLHonorCipherOrder| por no estar soportada aún por el motor SSL/TLS de wolfSSL.
%Esta opción da preferencia a las cipher suites propuestas por el servidor SSLv3 y TLSv1 por sobre aquellas que pueda ofrecer el cliente.

Se copiaron los certificados y claves de wolfSSL, originalmente situados en el directorio \verb|wolfssl/certs|, dentro de \verb|/opt/keys/wolfssl|, donde se almacenan todas las claves utilizadas en el presente proyecto.

Se inició el servidor Apache invocando directamente al binario \verb|bin/httpd|. Se utilizó la opción \verb|-X| para ejecutarlo en modo de depuración, y de esta forma visualizar los mensajes de error en el caso de producirse fallos. Como dato adicional se menciona que dicho binario permite aumentar la cantidad de mensajes de depuración agregando la opción \verb|-e DEBUG|. De manera predeterminada, el servidor quedó atendiendo en el puerto TCP 443. Se utilizó el cliente de ejemplo de wolfSSL utilizado en la sección anterior, esta vez especificando el puerto donde atiende el servidor Apache. Las líneas de ejecución de ambos pueden verse en el Alg. \ref{alg:wolfapache_exec}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y Apache. Ejecución de servidor y cliente https.},
	label={alg:wolfapache_exec},
	%firstnumber=1,
	numbers=none,
	]
# Ejecución del servidor Apache:
cd /usr/local/apache2
./bin/httpd -X

# Ejecución del cliente wolfSSL:
./examples/client/client -p 443 -g -d
\end{lstlisting}

\begin{comment}
	El proceso servidor quedó atendiendo el puerto predeterminado TCP 443 como puede verse en la salida del comando \verb|ss| (Fig. \ref{fig:wolf_httpd_port}).

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_httpd_port.png}
	\caption{\textbf{Apache atendiendo en el puerto TCP 443.}}
	\label{fig:wolf_httpd_port}
\end{figure}

\end{comment}

En la Fig. \ref{fig:wolf_https_cliserv} puede verse el servidor Apache corriendo en la terminal superior, y el cliente conectado en la inferior. En la terminal del cliente se aprecia la cipher suite NTRU negociada entre ambos para TLSv1.2.

Si bien se ha logrado la conexión cliente-servidor utilizando el cliente provisto por wolfSSL y el servidor Apache, en algunos casos aleatorios se detectaron fallas en la negociación, y terminaciones abruptas del proceso servidor. El Alg. \ref{alg:wolfapache_segfault} muestra la respuesta del cliente y servidor en el momento de producirse uno de estos fallos.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_https_cliserv.png}
	\caption{Conexión NTRU en Apache + wolfSSL.}
	\label{fig:wolf_https_cliserv}
\end{figure}

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y Apache. Errores detectados.},
	label={alg:wolfapache_segfault},
	%firstnumber=1,
	numbers=none,
	]
# Ejecución y salida del cliente (fallo):
cd /etc/wolfssl/wolfssl-git/
./examples/client/client -p 443 -g -d
CRL callback url = 
wolfSSL_connect error -308, error state on socket
wolfSSL error: wolfSSL_connect failed

# Salida del servidor (segfault)
Segmentation fault (core dumped)
\end{lstlisting}

El servidor registra los eventos de conexión con gran nivel de detalle en el archivo \verb|log/error_log| local de la implementación. La información relativa a este fallo en el servidor arrojó el mensaje mostrado en el Alg. \ref{alg:wolfapache_segfaultinfo}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y Apache. Info de errores detectados.},
	label={alg:wolfapache_segfaultinfo},
	%firstnumber=1,
	numbers=none,
	]
# Archivo: /etc/wolfssl/wolfssl-git/httpd-2.4.x/log/error_log

[Fri Jun 26 21:07:16.448582 2020] [core:notice] [pid 32013:tid 139640733110464] AH00051: child pid 32484 exit signal Segmentation fault (11), possible coredump in /usr/local/apache2

[Fri Jun 26 21:09:44.250532 2020] [ssl:warn] [pid 1512:tid 139988311048384] AH01882: Init: this version of mod_ssl was compiled against a newer library (4.3.0, version currently loaded is 4.3.0) - may result in undefined or erroneous behavior
\end{lstlisting}


Este error se produjo por conflictos en la integración de wolfSSL con el módulo \verb|mod_ssl| de Apache. Esta es una de las causas por las que esta integración continúa siendo experimental al momento de realizar estas pruebas, y no se recomienda su utilización en servicios en producción.

\begin{comment}
Cuando no se usa ntru, falla a la segunda vez que se carga la suite TLS_CHACHA20_POLY1305_SHA256

./examples/client/client -p 443 -g -d "TLS_CHACHA20_POLY1305_SHA256"
\end{comment}




\begin{comment}

wolfssl manual:
A key file can also be presented to the Context in either format. SSL_FILETYPE_PEM
signifies the file is PEM formatted while SSL_FILETYPE_ASN1 declares the file to be in
DER format. To verify that the key file is appropriate for use with the certificate the
following function can be used:
SSL_CTX_check_private_key(ctx);

https://www.youtube.com/watch?v=9IzHPWMnDHo&t=82s

\end{comment}

\section{WOLFSSL Y SERVICIOS OPENVPN}
\label{section:wolfssl_openvpn}

Esta sección describe la integración de wolfSSL con OpenVPN. Si bien wolfSSL provee soporte para compatibilidad con OpenVPN, ésta integración resulta precaria y aún no brinda, al día de la fecha, la posibilidad de utilizar protocolos de encapsulamiento ni cipher suites post-cuánticos. Igualmente a continuación se muestra la prueba de concepto realizada utilizando algoritmos tradicionales en TLS v1.3 con la intención de profundizar en los detalles y limitaciones de dicha integración.

\subsubsection{Compilación e integración de wolfSSL y OpenVPN}

Se re-compiló wolfSSL para añadir compatibilidad con OpenVPN. Por esta razón se creó un directorio específico para integrar estas aplicaciones, y se replicó luego dicho directorio en el equipo cliente para realizar la conexión. El Alg. \ref{alg:wolfopenvpn_compilewolfssl} muestra los pasos llevados a cabo para compilar e instalar wolfSSL. Según la documentación oficial \citep{sosinowiczSupportWolfSSLOpenVPN2020} la compatibilidad con OpenVPN se logra utilizando la opción \verb|--enable-openvpn|, que se añadió a las ya empleadas en la Sección \ref{section:wolfssl_pruebas}.

\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y OpenVPN. Compilación de wolfSSL.},
	label={alg:wolfopenvpn_compilewolfssl},
	%firstnumber=1,
	numbers=none,
	]
# Creación del directorio base
mkdir /opt/wolfssl_openvpn/
cd /opt/wolfssl_openvpn/

# Clonado del repositorio Git de wolfSSL
git clone https://github.com/wolfSSL/wolfssl.git
cd wolfssl

# Configuración, compilación e instalación de wolfSSL
./autogen.sh
./configure --enable-qsh --with-ntru=/usr/local/lib \
-enable-opensslextra --enable-supportedcurves \
--enable-apachehttpd --enable-tlsx --enable-all \
--enable-ecc --enable-psk --enable-aesccm \
C_EXTRA_FLAGS="-DWOLFSSL_STATIC_RSA -DHAVE_SNI" \
--enable-openvpn
make
sudo make install
\end{lstlisting}

Para compilar OpenVPN se utilizó un fork adaptado para su integración con wolfSSL. Dentro del mismo directorio creado en el paso anterior se clonó el repositorio de OpenVPN, y se cargó la rama \verb|wolfssl-compat| mediante el comando \verb|checkout| de \verb|git|. Luego se generaron los archivos de configuración y se compiló la implementación de OpenVPN. Debe notarse que el script \verb|configure| recibe como argumento una nueva opción para indicar que la compilación debe realizarse utilizando wolfSSL en lugar de la suite OpenSSL. El Alg. \ref{alg:wolfopenvpn_compileopenvpn} detalla estos pasos.

En este caso particular no se instaló OpenVPN en el sistema operativo para evitar generar conflictos con otras implementaciones y versiones. Igualmente el binario de OpenVPN generado pudo invocarse directamente desde su directorio de fuentes, y mostrado en el mismo Alg. \ref{alg:wolfopenvpn_compileopenvpn}.


\begin{lstlisting}[
	language={Bash},
	caption={wolfSSL y OpenVPN. Compilación de OpenVPN.},
	label={alg:wolfopenvpn_compileopenvpn},
	%firstnumber=1,
	numbers=none,
	]
# Clonado del repositorio Git de OpenVPN
cd /opt/wolfssl_openvpn/
git clone https://github.com/julek-wolfssl/openvpn.git
cd openvpn
git checkout wolfssl-compat

# Configuración y compilación de OpenVPN
autoreconf -i -v -f
./configure --with-crypto-library=wolfssl
make

# Directorio de fuentes de OpenVPN:
cd /opt/wolfssl_openvpn/openvpn/src/
\end{lstlisting}

\subsubsection{OpenVPN/wolfSSL - Pruebas de concepto}

Partiendo de la implementación de OpenVPN compilada en la sección anterior, se procedió a realizar la prueba de concepto. En primer lugar puede verificarse que dicho OpenVPN está integrado con la librería TLS de WolfSSL utilizando el modificador \verb|--version|, como puede observarse en la Fig. \ref{fig:wolfssl_openvpn_version}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolfssl_openvpn_version.png}
	\caption{OpenVPN/wolfSSL. Información de versión.}
	\label{fig:wolfssl_openvpn_version}
\end{figure}

Los algoritmos de cifrado soportados son variantes de AES con claves de 128, 192 y 256 bits, y de DES y 3DES, todos en modo CBC. Como se mencionó anteriormente, no soporta cipher suites de TLS en ninguna de sus versiones. Esta información puede extraerse utilizando los modificadores \verb|--show-ciphers| y \verb|--show-tls| respectivamente (véase Fig. \ref{fig:wolfssl_openvpn_ciphers_tls}).

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolfssl_openvpn_ciphers_tls.png}
	\caption{OpenVPN/wolfSSL. Cipher suites y algoritmos TLS.}
	\label{fig:wolfssl_openvpn_ciphers_tls}
\end{figure}

Puede anticiparse que esta versión de OpenVPN, al momento de realizar el presente trabajo, resulta sumamente reducida en comparación con su par PQCrypto-VPN analizado en la Sección \ref{section:oqs111vpn}.

Las pruebas de conexión se realizaron referenciando los certificados y claves RSA utilizados en la Sección \ref{section:oqs111vpn} para PQCrypto-VPN. Los resultados fueron satisfactorios, se omiten en el presente trabajo por no considerarse relevantes para el objetivo del mismo.

\begin{comment}
	Finalmente, y sabiendo que esta suite no soporta hasta el momento algoritmos de firma y encapsulamiento de claves post-cuánticos, igualmente se realizó la prueba con los certificados NTRU. Se consideró este experimento por ser wolfSSL compatible con certificados y claves NTRU, tal y como se vio en la Sección \ref{section:wolfssl}. A tal efecto se cargó, dentro de las configuraciones del servidor, el certificado digital NTRU provisto por wolfSSL en la opción \verb|ca| y \verb|cert|, y la clave asociada en la opción \verb|key|. Si bien no se dispone del certificado digital de la CA utilizado para firmar el del servidor, se utilizó el mismo certificado NTRU con la intención de verificar si OpenVPN era capaz de leerlo.

\begin{code}
ca /opt/keys/wolfssl/ntru-cert.pem
cert /opt/keys/wolfssl/ntru-cert.pem
key /opt/keys/wolfssl/ntru-key.raw
\end{code}

El resultado fue el esperado: errores al intentar leer e interpretar el certificado digital NTRU. La salida por pantalla es bastante extensa, por lo que a continuación se copia un extracto de los datos de interés (se omitió también la estampa de tiempo de cada mensaje):

\begin{code_small}
[...]
... ciphername = 'AES-256-CBC'
... ncp_enabled = DISABLED
... ncp_ciphers = 'AES-256-GCM:AES-128-GCM'
... authname = 'SHA384'
... prng_hash = 'SHA1'
[...]
... cert_file = '/opt/keys/wolfssl/ntru-cert.pem'
... extra_certs_file = '[UNDEF]'
... priv_key_file = '/opt/keys/wolfssl/ntru-key.raw'
[...]
... OpenVPN 2.4.8 [git:wolfssl-compat/854878e7ea32a61c+] x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [MH/PKTINFO] built on Nov  3 2020
... library versions: SSLeay wolfSSL compatibility, LZO 2.10
[...]
... Cannot load certificate file /opt/keys/wolfssl/ntru-cert.pem
... Exiting due to fatal error
\end{code_small}

En esta salida puede verse que se habilitaron las cipher suites \verb|AES-256-GCM| y \verb|AES-128-GCM|, la configuración de rutas de certificados y claves, el mensaje de ejecución de OpenVPN con las librerías de wolfSSL, y finalmente el error esperado: OpenVPN no puede leer el certificado NTRU.
\end{comment}

Este experimento dio cuenta de que la integración wolfSSL y OpenVPN es posible y la compatibilidad entre ambas implementaciones no genera errores ni inconsistencias para experimentos simples. No obstante, los algoritmos de cifrado de tráfico utilizados son escasos comparados con su contraparte OpenSSL, y no soporta ninguna cipher suite TLS.

A diferencia de la adaptación de Apache para su uso con wolfSSL realizada en la Sección \ref{section:wolfssl_https}, en este caso OpenVPN ya es compatible con wolfSSL, y la adaptación al uso de criptografía NTRU (u otros algoritmos post-cuánticos) requiere, por un lado, de la incorporación de cipher suites TLS en la integración, y por otro, de la adaptación de las mismas mediante el agregado de algoritmos post-cuánticos como NTRU.

Si bien OpenVPN y wolfSSL por separado son implementaciones ampliamente utilizadas y probadas, su integración se encuentra en etapas muy tempranas de desarrollo, por lo que su realización excede los alcances del presente trabajo de tesis.

\begin{comment}
	El paso necesario para lograr esta compatibilidad es modificar el código del motor TLS de OpenVPN para adaptarlo a wolfSSL, tal y como se realizó con Apache en la Sección \ref{section:oqs111_https_apache}. En el presente trabajo se omitió dicha adaptación por considerar muy pobre la integración básica entre OpenVPN y wolfSSL, lo que dificultaría obtener resultados aceptables.
\end{comment}