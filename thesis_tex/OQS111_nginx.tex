\section{OQS-OPENSSL V1.1.1 Y HTTPS (NGINX)}
\label{section:oqs111_https_nginx}

Uno de los objetivos principales de la presente tesis es el análisis de integraciones post-cuánticas de OpenSSL con Apache para brindar un protocolo HTTPS seguro ante ataques cuánticos. No obstante, durante el desarrollo del trabajo el proyecto OQS publicó una demo, basada en Docker, de la integración de OQS-OpenSSL v1.1.1 con Nginx v1.16.1. Resulta de especial interés un breve análisis de la misma, dado que Nginx es el servidor web de código abierto más usado después de Apache \citep{q-success_usage_2022}.

Al igual que en el caso anterior, como está integrado con OQS-OpenSSL v1.1.1 cuenta con las ventajas (y limitaciones) de esta versión respecto de la v1.0.2. La conexión en esta demo se realizó también en TLS1.3 con mecanismos de autenticación e intercambio de claves post-cuánticos. Igualmente, carece de soporte para cipher suites resistentes, por lo que no pueden realizarse pruebas de concepto en este sentido.

Para evitar ser reiterativos con los comandos, ya que son similares a los utilizados en la Sección \ref{section:oqs111_https_apache}, se exponen a continuación únicamente los pasos llevados a cabo durante el experimento.

\begin{enumeratenospace}
\li Creación de la red virtual Docker para realizar el experimento.
\li Extracción del certificado de la CA del servidor oqs-nginx contenido en la imagen Docker.
\li Ejecución de un servidor oqs-nginx (imagen Docker de OQS).
\li Ejecución del cliente cURL (imagen Docker de OQS) utilizando el certificado de la CA para realizar la verificación de firma en TLS.
\end{enumeratenospace}

La conexión se llevó a cabo satisfactoriamente, y el cliente cURL pudo verificar, mediante el certificado de la CA, la firma del certificado del servidor. Al igual que los experimentos realizados con OQS-OpenSSL y Apache, pudo analizarse el uso de claves y certificados post-cuánticos (Dilithium3 de manera predeterminada) y algoritmos KEM soportados por liboqs.

\begin{comment}

El primer paso fue crear la red virtual Docker, llamada \verb|nginx-test| en este ejemplo, y luego ejecutar el contenedor Nginx post-cuántico. El Alg. \ref{alg:oqsopenssl111nginx_dockernet} muestra estos comandos, y la ejecución del cliente cURL que ejecuta la prueba de conexión.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Nginx. Ejecución del servidor.},
	label={alg:oqsopenssl111nginx_dockernet},
	%firstnumber=1,
	numbers=none,
	]
	# Creación de la red docker nginx-test:
	docker network create nginx-test
	
	# Ejecución del servidor Nginx: (TCP 4433)
	docker run --network nginx-test --name oqs-nginx \
	-p 4433:4433 openquantumsafe/nginx
	
	# Ejecución del cliente cURL:
	docker run --network nginx-test -it \
	openquantumsafe/curl curl -vvk https://oqs-nginx:4433 \
	--curves frodo640aes
\end{lstlisting}

El modificador \verb|-vv| usado al lanzar el contenedor cURL permitió aumentar el modo explicativo (\textit{verbose}) del comando, y obtener mayor cantidad de información de depuración, mientras que el modificador \verb|--curves| forzó el intercambio de claves a Frodo640-AES. Inicialmente el comando de conexión falló debido a que el cliente no pudo verificar la firma del certificado digital del servidor, realizada por una autoridad certificante. El cliente siempre realiza esta verificación de manera predeterminada, y para lograrlo de manera satisfactoria necesita a su vez el certificado de la CA que se usó para generar la firma. Con la intención de realizar la prueba de conexión igualmente se añadió el modificador \verb|-k| (o \verb|--insecure|), lo que permitió evitar la verificación de la firma del certificado del servidor. Más adelante en esta sección se realizará dicha verificación de manera correcta.

Con las opciones comentadas previamente la conexión se llevó a cabo satisfactoriamente, los dos nodos pudieron conectarse y negociar el intercambio de claves seguro. La Fig. \ref{fig:openssl111_oqsmaster_nginx_certfail} muestra el registro de la conexión y la información del certificado x509 utilizado.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_nginx_certfail.png}
	\caption{Fallo en la verificación del certificado digital.}
	\label{fig:openssl111_oqsmaster_nginx_certfail}
\end{figure}

En dicha figura se ve resaltado el mensaje que indica que la firma de dicho certificado no pudo ser verificada por el cliente, y puede verse que la conexión siguió su curso normal. Si bien esta advertencia no genera ningún inconveniente a los fines de experimentación en un ambiente de pruebas, igualmente se procedió a extraer el certificado de la CA desde el contenedor Docker para facilitarle al cliente la verificación de la firma digital durante la conexión TLS.

Se exportó el certificado almacenado en \verb|cacert/CA.crt| dentro del contenedor Docker con un procedimiento similar al llevado a cabo en la Sección \ref{section:curl_liboqs}. El Alg. \ref{alg:oqsopenssl111nginx_certextract} muestra la extracción del certificado y la creación del archivo \verb|CA.crt| que lo contiene, y muestra la ejecución del contenedor cURL haciendo uso de dicho certificado. El resultado de la negociación TLS ahora puede verse en la Fig. \ref{fig:openssl111_oqsmaster_nginx_certok}.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSL v1.1.1 y Nginx. Extracción de certificado.},
	label={alg:oqsopenssl111nginx_certextract},
	%firstnumber=1,
	numbers=none,
	]
	# Extracción de certificado de la CA:
	docker run -it openquantumsafe/nginx \
	cat cacert/CA.crt > CA.crt
	
	# Ejecución del cliente cURL con dicho certificado:
	docker run --network nginx-test -v `pwd`:/opt/cacert -it openquantumsafe/curl curl --cacert /opt/cacert/CA.crt https://oqs-nginx:4433 --curves frodo640aes -vvI
\end{lstlisting}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_nginx_certok.png}
	\caption{Verificación correcta del certificado digital.}
	\label{fig:openssl111_oqsmaster_nginx_certok}
\end{figure}

El certificado digital utilizado por defecto corresponde con el algoritmo \textbf{Dilithium3}, a diferencia del Dilithium2 utilizado en el caso de Apache httpd. Esto puede verse en la captura de la Fig. \ref{fig:openssl111_oqsmaster_nginx_cliserv}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_nginx_cliserv.png}
	\caption{Conexión OQS-OpenSSL v1.1.1 predeterminada - Nginx.}
	\label{fig:openssl111_oqsmaster_nginx_cliserv}
\end{figure}

Al igual que el caso anterior, puede analizarse el certificado digital utilizando tanto el OQS-OpenSSL v1.1.1 compilado para este trabajo, como con la implementación incluida en el contenedor Docker. Por motivos de practicidad se utilizó el OQS-OpenSSL incluido en el contenedor Docker. El Alg. \ref{alg:oqsopenssl111nginx_certdata} muestra el comando correspondiente, y su salida resumida. Aquí se ve que el certificado y su firma son de tipo Dilithium3.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Nginx. Datos del certificado.},
	label={alg:oqsopenssl111nginx_certdata},
	%firstnumber=1,
	numbers=none,
	]
	# Extracción de datos:
	/opt/openssl111_oqsmaster/openssl/apps/openssl x509 \
	-in CA.crt -noout -text
	
	# Salida (resumida para resaltar información de interés):
	[...]
	Signature Algorithm: dilithium3
	Issuer: CN = oqstest CA
	[...]
	Subject: CN = oqstest CA
	Subject Public Key Info:
	Public Key Algorithm: dilithium3
	dilithium3 Public-Key:
	Signature Algorithm: dilithium3
	[...]
\end{lstlisting}

También se extrajo la configuración de Nginx para determinar la ubicación del certificado y clave propios del servidor. El Alg. \ref{alg:oqsopenssl111nginx_confextract} muestra los comandos necesarios para encontrar certificado y clave del servidor Nginx, su salida, el comando utilizado para descargar el certificado, y la extracción de datos del mismo. Como se aprecia en dicha salida, tanto el certificado como su clave responden al algoritmo Dilithium3.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSL v1.1.1 y Nginx. Extracción de configuración.},
	label={alg:oqsopenssl111nginx_confextract},
	%firstnumber=1,
	numbers=none,
	]
	# Configuración de certificado y clave (https-ssl.conf):
	docker run --network nginx-test --name oqs-nginx \
	-p 4433:4433 openquantumsafe/nginx \
	cat /opt/nginx/nginx-conf/nginx.conf \
	| grep "^ssl_certificate"
	
	# Salida:
	ssl_certificate      /opt/nginx/pki/server.crt;
	ssl_certificate_key  /opt/nginx/pki/server.key;
	
	# Descarga del certificado digital del servidor:
	docker run --network nginx-test --name oqs-nginx \
	-p 4433:4433 openquantumsafe/nginx \
	cat /opt/nginx/pki/server.crt > server.crt
	
	# Extracción de datos:
	/opt/openssl111_oqsmaster/openssl/apps/openssl x509 \
	-in server.crt -noout -text
	
	# Salida (resumida para resaltar información de interés):
	[...]
	Signature Algorithm: dilithium3
	Issuer: CN = oqstest CA
	[...]
	Subject: CN = oqs-nginx
	Public Key Algorithm: dilithium3
	dilithium3 Public-Key:
	[...]
\end{lstlisting}

%\begin{comment}
	# Descarga de la clave del servidor:
	docker run --network nginx-test --name oqs-nginx \
	-p 4433:4433 openquantumsafe/nginx \
	cat /opt/nginx/pki/server.key > server.key
%\end{comment}

\end{comment}