\cleardoublepage
\chapter{PROYECTO WOLFSSL}
\label{section:wolfssl}

WolfSSL (anteriormente denominado CyaSSL) \citep{wolfsslWolfSSLEmbeddedSSL2020} es una biblioteca de cifrado SSL/TLS de código abierto, ligera, portable y escrita en lenguaje C, cuyo objetivo principal es la implementación de protocolos de cifrado SSL/TLS en dispositivos de Internet de las cosas (IoT), software embebido y sistemas operativos de tiempo real, o RTOS (Real Time Operating System debido su reducido tamaño, su velocidad y sus características.

WolfSLL puede correr tanto sobre computadoras de escritorio como en entornos empresariales y en computación de nube. Soporta los estándares de la industria TLS v1.3 y DTLS v1.2, y su tamaño es hasta 20 veces menor que el de otras suites conocidas como OpenSSL. WolfSSL ofrece un API y una capa de compatibilidad con OpenSSL, y soporta el Protocolo de comprobación de certificados digitales OCSP (Online Certificate Status Protocol) y el protocolo de lista de revocación de certificados CRL (Certificate Revocation List).

Como backend criptográfico WolfSSL utiliza la biblioteca \verb|wolfCrypt|, un motor criptográfico ligero escrito en ANSI C y cuyo objetivo también son es el software embebido y sistemas con recursos limitados. WolfCrypt soporta los algoritmos de cifrado más comunes, a saber, RSA, ECC, DSS, Diffie–Hellman, EDH, NTRU, DES, Triple DES, AES (CBC, CTR, CCM, GCM), Camellia, IDEA, ARC4, HC-128, ChaCha20, MD2, MD4, MD5, SHA-1, SHA-2, SHA-3, BLAKE2, RIPEMD-160 y Poly1305, y también soporta algoritmos experimentales como RABBIT, un software de transmisión de códigos de dominio público publicado por el Proyecto eSTREAM de la Unión Europea \citep{ecryptESTREAMPortfolioPage2008}, y sumamente útil para cifrado de transmisiones en entornos de alto rendimiento. WolfSSL también soporta la curva elíptica Curve25519, el esquema de firma digital asociado, Ed25519. 

Además, y un factor importante para el desarrollo del presente proyecto, incluye el algoritmo de criptografía asimétrica post-cuántica NTRU. Al utilizar la mínima cantidad de bits necesaria para proveer un nivel de seguridad equivalente a otros algoritmos asimétricos, NTRU es especialmente útil en dispositivos móviles y sistemas embebidos. En este contexto, WolfSSL también incluye varias suites de cifrado simétrico que hacen uso de NTRU internamente, tales como AES-256, RC4 y HC-128.

\section{WOLFSSL: PRUEBA DE CONCEPTO}
\label{section:wolfssl_pruebas}

A continuación se detalla la realización de una prueba de concepto de conexión post-cuántica basada en NTRU utilizando WolfSSL, y su posterior integración con el servicio HTTPS Apache2.

\subsection*{Compilación e instalación}

Para compilar WolfSSL con NTRU primero instaló la biblioteca NTRU en el sistema operativo. A tal propósito se creó el directorio \verb|/opt/wolfssl/|.

\begin{code}
mkdir /opt/wolfssl/
cd /opt/wolfssl/
\end{code}

Dentro del directorio se descargó, desde los repositorios oficiales, la librería NTRUEncrypt, se compiló e instaló de la siguiente manera:

\begin{code}
sudo git clone \
	https://github.com/NTRUOpenSourceProject/NTRUEncrypt.git	
cd NTRUEncrypt/
./autogen.sh
./configure
make
make install
\end{code}

Por defecto, la librería NTRUEncrypt se instaló en el directorio \verb|/usr/local/lib/| tal y como puede verse en la Figura \ref{fig:libntru}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/libntru.png}
	\caption{\textbf{Librería NTRU}}
	\label{fig:libntru}
\end{figure}

Con la librería NTRUEncrypt compilada e instalada se procedió a compilar WolfSSL. Si bien al momento de realizar esta prueba se encuentra disponible la versión \verb|v4.4.0-stable|, la misma produce errores de compilación al intentar integrar wolfSSL con NTRU (ver Figura \ref{fig:wolfssl_4.4error}), por lo que se optó por utilizar la versión anterior, \verb|v4.3.0-stable|.

\begin{code}
mkdir /opt/wolfssl/wolfssl-git/ && cd wolfssl-git
git clone https://github.com/wolfSSL/wolfssl.git
cd wolfssl/
git checkout v4.3.0-stable
./autogen.sh
./configure --enable-qsh --with-ntru=/usr/local/lib \
	-enable-opensslextra --enable-supportedcurves \
	--enable-apachehttpd --enable-tlsx --enable-all \
	--enable-ecc --enable-psk --enable-aesccm \
	C_EXTRA_FLAGS="-DWOLFSSL_STATIC_RSA -DHAVE_SNI"
make
make install
\end{code}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolfssl_4.4error.png}
	\caption{\textbf{Errores de compilación wolfSSL v4.4.0-static con NTRU}}
	\label{fig:wolfssl_4.4error}
\end{figure}

Como puede observarse, el script de configuración \verb|./configure| recibió por argumento una serie de opciones que incluyen las siguientes:

\begin{itemize}
	\li \verb|--enable-qsh|: Habilita QSH (Quantum-safe hybrid)(Ver Sección \ref{section:qsh}).
	\li \verb|--with-ntru=/usr/local/lib|: Especifica el directorio donde se instaló NTRUEncrypt.
	\li \verb|--enable-apachehttpd|: Habilita a wolfssl su integración con Apache (véase Sección \ref{section:wolfssl_https})
\end{itemize}

Debe notarse también que se habilitó un flag especial \verb|WOLFSSL_STATIC_RSA| para forzar a wolfSSL a utilizar claves RSA estáticas en lugar de efímeras. Si bien esto es desaconsejado en servicios de producción, existen sistemas criptográficos como NTRU que requieren de claves estáticas para funcionar \citep{wolfsslinc.WolfSSLUserManual2019, wolfsslinc.GitHubWolfSSLWolfssl2020}.

\subsection*{Negociación SSL/TLS}

Con wolfSSL compilado y funcional se pudo realizar una prueba de concepto de conexión utilizando los algoritmos por predeterminados. wolfSSL incluye un servidor y un cliente de ejemplo para realizar la verificación de conexión y funcionamiento. Además, como se trata de una implementación de código abierto, los fuentes de estos ejemplos sirven para orientar el desarrollo de aplicaciones que hagan uso de SSL/TLS o, como es el caso del presente trabajo, adaptar el código de otras implementaciones como Apache2 para que haga uso de esta suite en la negociación del intercambio seguro.

La compilación de wolfSSL detallada en el apartado anterior generó los binarios ejecutables de servidor y cliente. Para montar el servidor de ejemplo se utilizó el siguiente comando:

\begin{code}
cd /opt/wolfssl/wolfssl-git/
./exeamples/server/server
\end{code}

El servidor por defecto atiende en el puerto TCP 11111. La salida estándar de servidor, sin ningún tipo de configuración adicional, es la que se ve en la Figura \ref{fig:wolf_server_simple}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_simple.png}
	\caption{\textbf{Respuesta servidor wolfSSL negociación estándar}}
	\label{fig:wolf_server_simple}
\end{figure}

El cliente pudo conectarse de la siguiente manera:

\begin{code}
cd /opt/wolfssl/wolfssl-git/
./exeamples/client/client
\end{code}

La respuesta de la negociación del lado del cliente se ve en la Figura \ref{fig:wolf_client_simple}. Como puede apreciarse en ambas capturas, la negociación SSL/TLS estándar utiliza TLS1.2, la ciphersuite \verb|TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384|, y hace uso de QSH.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_client_simple.png}
	\caption{\textbf{Respuesta cliente wolfSSL negociación estándar}}
	\label{fig:wolf_client_simple}
\end{figure}

El cliente provee algunos comandos y opciones adicionales que pueden listarse con la opción \verb|-h|. Entre ellos se encuentra el modificador \verb|-e|, que permite listar las ciphersuites disponibles que soporta esta versión del protocolo. Como se ha realizado la compilación utilizando los flags \verb|NTRU| y \verb|QSH|, estos algoritmos se vieron incluidos en la salida, así como también los algoritmos mencionados al principio de esta sección.

Para listar las ciphersuites de una manera más clara se editó la salida del comando usando \verb|sed| para reemplazar el caracter \verb|":"| por un salto de línea:

\begin{code}
	./examples/client/client -e|sed 's/:/\n/g'
\end{code}

Como el interés de este trabajo se centra en criptografía post-cuántica, a continuación se muestra un breve listado de las ciphersuites que son de mayor importancia:



\begin{itemize}
	\setlength\itemsep{0em}
	\li NTRU-RC4-SHA
	\li NTRU-DES-CBC3-SHA
	\li NTRU-AES128-SHA
	\li NTRU-AES256-SHA
	\li QSH
\end{itemize}

Al cliente se le puede pasar por argumento la opción \verb|-l| para que utilice una ciphersuite determinada, o varias separándolas por \verb|":"|. El siguiente ejemplo ilustra este el caso de configurar tres ciphersuites desde el lado del cliente.

\begin{code}
./examples/client/client -l ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA
\end{code}

La negociación TLS seleccionó una de las ciphersuites ofrecidas por el cliente, y el resultado puede verse en la sección \verb|SSL-Session| de la salida por pantalla. Además, como se ve a continuación, esta negociación con parámetros predeterminados no hace uso ni de NTRU ni de QSH.

\begin{code}
SSL-Session:
	Protocol  : TLSv1.2
	Cipher    : TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA
\end{code}

\subsection*{Negociación QSH}

Como se vio en la sección anterior, por defecto wolfSSL no hace uso de QSH. A continuación se analizará esta característica en detalle.

Con la intención de entender la implementación de wolfSSL en Cliente y Servidor, y para facilitar la identificación del uso de QSH durante la negociación SSL/TLS, se modificó el código fuente del servidor agregando la siguiente instrucción, aproximadamente en la línea 469 de su código fuente, archivo \verb|examples/server/server.c| (ver Figura \ref{fig:wolf_server_qshcode}), y se recompiló la implementación.

\begin{code}
printf("Quantum-Safe Handshake Ciphersuite?? -> %s\n",
  (wolfSSL_isQSH(ssl))?"Yes":"No");                                  
\end{code}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_qshcode.png}
	\caption{\textbf{Modificación del código del servidor para evaluar QSH}}
	\label{fig:wolf_server_qshcode}
\end{figure}

De esta manera, la salida del servidor para el mismo ejemplo de conexión se muestra la Figura \ref{fig:wolf_server_qshno}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_qshno.png}
	\caption{\textbf{Evaluando QSH en SSL/TLS - Caso negativo}}
	\label{fig:wolf_server_qshno}
\end{figure}

Sin embargo, si el cliente ofrece la ciphersuite QSH, el servidor la utiliza de manera adicional a una ciphersuite tradicional. La siguiente línea de conexión del cliente agrega la ciphersuite \verb|QSH| al final de la cadena.

\begin{code}
./examples/client/client -l ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA:QSH
\end{code}

Esta conexión mostró el mensaje afirmativo agregado al código, como puede apreciarse en la Figura \ref{fig:wolf_server_qshyes}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_server_qshyes.png}
	\caption{\textbf{Evaluando QSH en SSL/TLS - Caso positivo}}
	\label{fig:wolf_server_qshyes}
\end{figure}

Otras opciones interesantes y útiles que pueden utilizarse en el servidor para realizar pruebas son las siguientes:

\begin{itemize}
	\setlength\itemsep{0em}
	\li \textbf{-x}: Permite imprimir los mensajes de error sin cerrar la conexión.
	\li \textbf{-b}: Levanta el servicio en todas las interfaces de red, no sólo localhost.
	\li \textbf{-i}: Permite mantener el servidor activo para realizar múltiples conexiones desde los clientes.
	\li \textbf{-n}: Habilita el cifrado NTRU en SSL/TLS (ver más adelante).
	\li \textbf{-c}: Especifica la ruta al certificado digital a utilizar en la conexión.
	\li \textbf{-k}: Especifica la ruta a la clave privada a utilizar en la conexión.
\end{itemize}

El cliente también acepta otras opciones que se documentan a continuación, aunque serán de utilidad más adelante:

\begin{itemize}
	\setlength\itemsep{0em}
	\li \textbf{-l}: Ya mencionada, permite especificar una ciphersuite determinada.
	\li \textbf{-h}: Permite especificar, por ip o nombre, el host a conectar.
	\li \textbf{-p}: Permite especificar un puerto destino distinto de 11111.
	\li \textbf{-d}: Desactiva la verificación de par, lo que facilita las pruebas con certificados digitales autofirmados.
	\li \textbf{-g}: Envía un mensaje \verb|HTTP GET| al servidor, útil para \verb|https|.
\end{itemize}

\subsection*{Negociación NTRU}

La suite wolfSSL provee, junto con sus archivos de código fuente y configuraciones, una serie de claves y certificados digitales almacenados en el directorio \verb|certs/| de la raíz del repositorio. Dentro del mismo se encuentran una clave y un certificado digital generados utilizando el algoritmo NTRU:

\begin{code}
certs/ntru-cert.pem		# Certificado digital
certs/ntru-key.raw		# Clave privada
\end{code}

El servidor brinda la opción \verb|-n| para activar el algoritmo NTRU, y las opciones \verb|-c| y \verb|-k| para especificar certificado y clave respectivamente, por lo que la llamada al servidor quedó así:

\begin{code}
./examples/server/server -c ./certs/ntru-cert.pem -k ./certs/ntru-key.raw -n
\end{code}

Luego se lanzó el cliente sin especificar ningún parámetro adicional, y de manera predeterminada intentó establecer la conexión NTRU, como puede verse en la Figura \ref{fig:wolf_ntru_cli}.

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_ntru_cli.png}
	\caption{\textbf{Negociación NTRU en TLS v1.2 - Cliente}}
	\label{fig:wolf_ntru_cli}
\end{figure}

El resultado de la negociación TLS en el servidor se ve en la Figura \ref{fig:wolf_ntru_srv}.

\begin{figure}[H]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_ntru_srv.png}
	\caption{\textbf{Negociación NTRU en TLS v1.2 - Servidor}}
	\label{fig:wolf_ntru_srv}
\end{figure}

Como puede verse en la salida del servidor (Figura \ref{fig:wolf_ntru_srv}), al negociar una ciphersuite NTRU en SSL/TLS, no se hace uso de QSH, un algoritmo que se habilitaba anteriormente de manera predeterminada (ver Figuras \ref{fig:wolf_server_simple} y \ref{fig:wolf_client_simple}).

Surgió en este punto la duda de si WolfSSL, al habilitar NTRU, desactiva QSH, o desactiva cualquier otra ciphersuite distinta de NTRU. Se intentó conectar un cliente especificando una ciphersuite estándar no NTRU, y el servidor rechazó la negociación SSL/TLS porque no pudo verificar la ciphersuite, como puede verse en la Figura \ref{fig:wolf_ntru_other}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/wolf_ntru_other.png}
	\caption{\textbf{Servidor en modo NTRU recibiendo otras ciphersuites}}
	\label{fig:wolf_ntru_other}
\end{figure}

Sin embargo, al invocarse al cliente sin utilizar ninguna ciphersuite, negoció NTRU con el servidor. Incluso si se fuerza la negociación de QSH junto con ciphersuites NTRU, QSH no se habilitó.

Por ejemplo si el cliente especifica la ciphersuite \verb|NTRU-AES256-SHA:QSH|, el cifrador resultante de la negociación SSL/TLS será \\
\verb|TLS_NTRU_RSA_WITH_AES_256_CBC_SHA|, que corresponde a la ciphersuite \\
propuesta por el cliente, pero sin habilitar QSH explícitamente. El siguiente fragmento de la salida del servidor ejemplifica el caso:

\begin{code}
SSL version is TLSv1.2
SSL cipher suite is TLS_NTRU_RSA_WITH_AES_256_CBC_SHA
SSL curve name is FFDHE_3072
Server Random : 7148D9E984C5C4C8A6958B701C93608744194A93B37D408DE986...
Quantum-Safe Handshake Ciphersuite?? -> No
\end{code}

Este comportamiento se da porque wolfSSL activa la ciphersuite QSH como complemento resistente a ataques cuánticos en algoritmos tradicionales. NTRU ya dispone de un intercambio post-cuántico integrado en la librería, y se activa automáticamente al establecer la conexión.

La ciphersuite QSH tiene mayor preferencia que otros protocolos comunes durante la selección de algoritmos. Si un usuario integra NTRU dentro de WolfSSL y ambos extremos de conexión soportan NTRU, entonces la ciphersuite NTRU y su intercambio de claves seguro serán seleccionados de manera predeterminada. Sólo en el caso de que el usuario excluya explícitamente a NTRU se usará QSH combinado con ciphersuites tradicionales, como se menciona en el manual de usuario de la suite \citep{wolfsslinc.WolfSSLUserManual2019}.

Dicho manual menciona que NTRU acelera el proceso de conexión SSL/TLS de 20 a 200 veces sobre un intercambio RSA, y la mejora aumenta según aumenta el tamaño de la clave utilizada. Es decir, el proceso de negociación de SSL/TLS será más rápido con claves de 8192 bits que con claves de 1024 bits comparando el rendimiento con RSA.

Finalmente, debido a que NTRU no tiene una adopción masiva como ocurre con RSA, WolfSSL lo implementa en modo híbrido con otras ciphersuites. Los desarrolladores de WolfSSL propusieron la inclusión de NTRU en modo híbrido dentro de QSH, y así lograr que TLS permita utilizar pares de claves post-cuánticas NTRU por única vez, además de los pares de claves de algoritmos tradicionales \citep{whyteQuantumsafeHybridQSH2017, wolfsslQuantumSafetyWolfSSL2019}.


\begin{comment}
qsh hace uso de ntru para dar cifrado ntru al intercambio de claves, lo que agrega resistencia a ataques cuanticos
hay que explicar el qsh de la ietf, los diferentes mecanismos que dan resistencia cuantica, entre los que se encuentran ntru.
\end{comment}


