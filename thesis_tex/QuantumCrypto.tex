\cleardoublepage
\chapter{CRIPTOGRAFÍA CUÁNTICA}
\label{section:quantumcrypto}

En este capítulo se hará una introducción a uno de los mecanismos de intercambio de claves resistentes a ataques cuánticos: la criptografía cuántica, y se introducirán algunos de los algoritmos principales en este campo como lo son BB84, B92 y E91.

Si bien el presente trabajo hace referencia a criptografía post-cuántica, existen una confusión generalizada sobre su diferencia con la criptografía cuántica y el mecanismo de intercambio de claves asociado. Considerando que la criptografía cuántica también representa criptosistemas resistentes a ataques cuánticos, se ha decidido incluir una breve introducción.

\begin{comment}
	La criptografía cuántica permite distribuir claves secretas sobre un canal de comunicación abierto, y aunque se habla de \qq{distribuir}, más precisamente \qq{genera} dicha clave en forma aleatoria en ambos extremos. Las leyes de la física garantizan que un intento de espionaje o escaneo en un canal de comunicación abierto inyectará errores en la clave, y así el atacante o espía será detectado. Ese principio es la base de la criptografía cuántica, o intercambio de claves cuántico QKE (Quantum Key Exchange).

Un punto a tener en cuenta es que la criptografía cuántica no permite darle seguridad a la transmisión de información en la red, sino que solamente permite realizar un intercambio de claves de forma segura. Una vez garantizado este intercambio ambos extremos pueden compartir información cifrada utilizando esta clave en cifradores simétricos conocidos, y realizar la transmisión sobre un medio tradicional como el cobre. Un canal cuántico abierto entre dos nodos permitirá seguir generando claves para continuar transmitiendo datos.

La criptografía cuántica utiliza estados cuánticos, tales como la polarización de fotones individuales, para transmitir bits de información. Tal y como se vio en la Sección \ref{section:quantumcomputing}, no es posible hacer una copia de un estado cuántico desconocido sin modificarlo \citep{wootters_single_1982}, por lo que un supuesto atacante no podría medir el estado de los fotones en el canal.

Esto está intrínsecamente relacionado con el principio de incertidumbre de Heisenberg: si alguien mide la posición precisa de una partícula, toda la información acerca de su momento se perderá. Es decir, el hecho de medir el estado de la partícula cuántica como un fotón, influye sobre ella alterándola. Un ejemplo simple sería la medición de la ubicación de un fotón dentro de una habitación aislada. Heisenberg notó que para poder localizar un fotón en el espacio debían hacerse rebotar electrones en él, pero cuando un electrón rebota en el fotón, altera su momento y se pierde su estado original. Particularmente en el caso de los fotones existen muchas propiedades que no pueden ser medidas simultáneamente, tales como los estados de polarización vertical, horizontal y diagonal.
\end{comment}

La criptografía cuántica permite distribuir claves secretas sobre un canal de comunicación abierto, y aunque se habla de 'distribuir', más precisamente 'genera' dicha clave en forma aleatoria en ambos extremos. Las leyes de la física garantizan que un intento de espionaje o escaneo en un canal de comunicación abierto inyectará errores en la clave, y así el atacante o espía será detectado. Ese principio es la base de la criptografía cuántica, o intercambio/distribución de claves cuánticas QKE/QKD (Quantum Key Exchange/Distribution).

Un punto a tener en cuenta es que la criptografía cuántica no permite darle seguridad a la transmisión de información en la red, sino que solamente posibilita un intercambio de claves de forma segura. Una vez garantizada la seguridad de este intercambio, ambos extremos podrán compartir información cifrada utilizando esta clave en cifradores simétricos conocidos, y realizar la transmisión sobre un medio tradicional como el cobre. Un canal cuántico abierto entre dos nodos permitirá seguir generando claves para continuar transmitiendo datos.

La criptografía cuántica utiliza estados cuánticos, tales como la polarización de fotones individuales, para transmitir bits de información. No es posible hacer una copia de un estado cuántico desconocido sin modificarlo \citep{wootters_single_1982}, por lo que un supuesto atacante no podría medir el estado de los fotones en el canal. Así,  el intercambio de claves cuántico es seguro incondicionalmente dado que no hay posibilidades de que el atacante pueda realizar cálculos matemáticos difíciles para conseguir la clave, y tampoco pueda violar las leyes de la física. Sin embargo, aún con este nivel de seguridad, los protocolos de intercambio de claves cuánticos son susceptibles a ataques de intermediario, o MITM (Man in the Middle - Hombre en el Medio).% En este caso un atacante podría hacer de intermediario entre emisor y receptor, y así realizaría un intercambio cuántico con Alice y otro independiente con Bob, lo que le facilitaría interceptar el tráfico en texto plano.

Esto está intrínsecamente relacionado con el principio de incertidumbre de Heisenberg: si alguien mide la posición precisa de una partícula, toda la información acerca de su momento se perderá. Es decir, el hecho de medir el estado de la partícula cuántica como un fotón, influye sobre ella alterándola. Un ejemplo simple sería la medición de la ubicación de un electrón dentro de una habitación aislada. Heisenberg notó que para poder localizar un electrón en el espacio debían hacerse rebotar fotones en él, pero cuando un fotón rebota en el electrón, si bien permite determinar su ubicación, también altera su momento y se pierde su estado original.

Supóngase el intercambio de claves entre dos extremos, Alice y Bob, la criptografía cuántica se basa en el intercambio de estados cuánticos en un medio físico entre estos dos extremos. Esto puede llevarse a cabo utilizando dos técnicas. Por un lado se puede usar un conjunto predefinido de estados cuánticos no ortogonales de una partícula única. Por otro lado se pueden realizar mediciones predefinidas en partículas entrelazadas, en cuyo caso la clave no existe durante la transmisión. La idea de usar estados cuánticos no ortogonales fue propuesta por Weisner en \citep{wiesner_conjugate_1983}. En general los criptosistemas utilizan dos canales de comunicación entre Alice y Bob: uno cuántico, por ejemplo, una fibra óptica o el vacío, y otro tradicional, como una red cableada o inalámbrica. Por el medio cuántico se llevará a cabo el intercambio de claves, y por el medio tradicional el intercambio de los datos cifrados.

Considérense dos estados normalizados: $|0\rangle$ y $|1\rangle$, de modo que $\langle0|1\rangle \neq 0 $.

Ahora supóngase que existe una máquina de clonación de estados cuánticos que opera como sigue:
\[ |0\rangle|B\rangle|m\rangle \rightarrow |0\rangle|0\rangle|m_0\rangle \]
\[ |1\rangle|B\rangle|m\rangle \rightarrow |1\rangle|1\rangle|m_1\rangle \]

Donde 'B' es el estado inicial de una partícula que será un clon luego de la operación, y todos los estados se definen normalizados. Esta operación debe ser unitaria y debería preservar el producto interno, por lo que se requiere que se cumpla:
\[ \langle0| 1\rangle = \langle0|1\rangle\langle0|1\rangle\langle m_0|m_1\rangle \]

Esto únicamente es posible cuando:
\begin{itemize}
	\setlength\itemsep{0em}
	\item $\langle0|1\rangle = 0$ : cuando los dos estados son ortogonales.
	\item $\langle0|1\rangle = 1$ : cuando los dos estados son indistinguibles. \\
	Este caso no puede utilizarse para codificar dos bits diferentes, por lo que carece de utilidad.
\end{itemize}

Bennet y Brassard propusieron el uso de estados no ortogonales de fotones polarizados para distribuir claves criptográficas \citep{bennettQuantumCryptographyPublic1984}. Esto genera problemas a Eva al momento de realizar cualquier intento de medición que le permita distinguir entre estados no ortogonales $|0\rangle$ y $|1\rangle$ en el canal cuántico. Supóngase que Eva prepara su dispositivo de medición inicialmente en un estado normalizado $|m\rangle$ y quiere diferenciar un valor $|0\rangle$ de un $|1\rangle$ leído en el canal cuántico, y sin alterar dichos estados. Por ejemplo, podría implementar la siguiente operación unitaria:
\[ |0\rangle|m\rangle \rightarrow |0\rangle|m_0\rangle \]
\[ |1\rangle|m\rangle \rightarrow |1\rangle|m_1\rangle \]

La condición de unitaridad implica que $\langle0|1\rangle = \langle0|1\rangle\langle m_0|m_1\rangle$. Por ejemplo, el producto escalar $\langle m_0|m_1\rangle = 1$, el estado final de la medición, es el mismo en ambos casos. Los dos estados no se modifican, pero Eva no gana información acerca del valor del bit codificado. Una medición más general que modifica los estados originales, de modo que $|0\rangle \rightarrow |0'\rangle$ y $|1\rangle \rightarrow |1'\rangle$ tiene la siguiente forma:
\[ |0\rangle|m\rangle \rightarrow |0'\rangle|m_0\rangle \]
\[ |1\rangle|m\rangle \rightarrow |1'\rangle|m_1\rangle \]

La condición de unitaridad da $\langle0|1\rangle = \langle0'|1'\rangle\langle m_0|m_1\rangle$. El valor mínimo del producto escalar $\langle m_0|m_1\rangle$ que corresponde con la situación en la que Eva tiene la mejor oportunidad de distinguir entre los dos estados, es obteniendo $\langle 0'|1'\rangle=1$, y los dos estados en $|0\rangle$ y $|1\rangle$ se volverán el mismo luego de la interacción. 

\section{PROTOCOLO BB84}

Stephen Wiesner en los años '70 propuso utilizar la polarización para codificar información \citep{wiesner_conjugate_1983}. En 1984 Charles Bennett y Gilles Brassard se basaron en estos conceptos para plantear que los estados cuánticos que se propagan a través de un medio óptico pueden ser codificados de manera segura, y crearon el protocolo Bennett-Brassard 1984, más conocido como BB84, el primer protocolo de criptografía cuántica, y que sigue vigente en la actualidad \citep{bennettQuantumCryptographyPublic1984} \citep{bennett1997strengths}. El experimento constó de pulsos de luz en el espacio, en una distancia de 40cm. Si bien no es práctico para realizar el intercambio de claves, representa los primeros pasos en criptografía cuántica.

Para analizar este protocolo de intercambio de claves es necesario disponer de un canal de comunicación cuántico, por ejemplo, una fibra óptica. Considérese pulsos de luz polarizada. Cada pulso contendrá un solo fotón. Se comenzará con polarización vertical u horizontal, respectivamente, $|\leftrightarrow\rangle$ y $|\updownarrow\rangle$ en notación de Dirac de mecánica cuántica.

Para transmitir la información se necesita un sistema de codificación. Supóngase que $|\updownarrow\rangle$ representa un $0$ y $|\leftrightarrow\rangle$ un $1$. Con este sistema, si Alice envía una serie de pulsos que representan el binario $10110$, es decir:
\[ |\leftrightarrow\rangle, |\updownarrow\rangle, |\leftrightarrow\rangle, |\leftrightarrow\rangle, |\updownarrow\rangle \]

Cuando Alice envía solamente los fotones $|\leftrightarrow\rangle$ o $|\updownarrow\rangle$, estará enviando los fotones polarizados en la base $\oplus$. Como la clave debe ser aleatoria, Alice enviará 0 o 1 con igual probabiliadd. Para detectar el mensaje, Bob usa un PBS (Polarization Beamsplitter - Divisor de Hde Polarización) que transmite la polarización vertical y desvía la horizontal. Luego, utilza detectores de fotones en cada caso. Como se ve en la Fig. \ref{fig:pbs}, la detección en el detector D0 (o D1) significará que Alice envió un 0 (o un 1). En este caso, se dice que Bob está midiendo en la base $\oplus$.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth]{img/pbs.png}
	\caption{Esquema de polarización y detección.}
	\source{\citep{ekert2000physics}}
	\label{fig:pbs}
\end{figure}

Los detectores no son perfectos, y se espera que algunos bits sean descartados en el proceso de transmisión, por lo que sólo se utilizará una fracción de los bits originales enviados por Alice. Por esto el sistema no sirve para transmitir mensajes, pero sí para intercambiar claves de cifrado.

Hasta este punto el protocolo es inseguro. Eva, el atacante, podría medir los pulsos con un mecanismo similar al de Bob, y reenviar los mismos pulsos a Bob, pasando desapercibida, y conociendo la totalidad de la clave. Para evitar este problema, Alice añade otra elección aleatoria, además de los bits: una nueva base de polarización: diagonal, o $\otimes$. Ahora, se añade nueva codificación: $|\neswarrow\rangle$ representa un $0$ y $|\nwsearrow\rangle$ un $1$. Alice seleccionará una de las dos bases, $\oplus$ y $\otimes$, aleatoriamente con igual probabilidad. Bob también puede medir estos fotones rotando su configuración de PBS en 45º.

Con este esquema, la seguridad se fundamenta en una propiedad de la mecánica cuántica: la indeterminación. Un fotón preparado en la base $\otimes$ y medido en la base $\oplus$ tendrá una probabilidad de $1/2$ de ser detectado por D0 o D1, y esto es totalmente aleatorio, ya que el fotón no incluye información que permita determinar con qué base medirlo.

Si Alice prepara un fotón en el estado $|\neswarrow\rangle$ y Bob intenta medirlo en la base $\oplus$, será detectado en D0 o D1 con igual probabilidad. Esto no significa que la mitad de los fotones en un haz $|\neswarrow\rangle$ están polarizados en la base vertical y la otra mitad en la horizontal, sino que el sistema se comporta como si eligiera aleatoriamente el camino a seguir, D0 o D1, cuando se mide.

Esto aplica también para Eva. Como Alice usa cada base de manera aleatoria, Eva no sabe qué base usar para medir. Cada vez que Eva mide con una base equivocada, obtendrá un resultado erróneo el 50\% de las veces. Otro punto importante es que Eva no puede saber cuándo un resultado de su medición es erróneo. Cuando recibe un fotón en el detector D0, no sabe si el fotón original estaba preparado en el estado $|\updownarrow\rangle$, o estaba preparado en el estado $|\neswarrow\rangle$ o $|\nwsearrow\rangle$ y simplemente \textit{eligió} ir por el detector D0. Cabe resaltar aquí que es necesario que los fotones polarizados por Alice se envíen de a uno, ya que un haz de más de un fotón enviado en la base equivocada podría pasar por los detectores D0 y D1 a la vez, \textit{diciéndole} a Eva que su base es equivocada, y permitiéndole descartar el fotón evitando considerar un fotón erróneo. Cuando Eva mide el estado del fotón, y éste es único, no tiene otra opción que considerarlo, y reenviarlo a Bob tal como lo dejó, en el estado medido. Esto generará errores en las mediciones de Bob.

Con estos conceptos base, se puede resumir un protocolo de intercambio de claves como sigue:

\begin{enumerate}
	\item Alice genera una secuencia aleatoria de bits, y para cada bit selecciona aleatoriamente una base de polarización, $\otimes$ o $\oplus$, y la utiliza para polarizar cada bit. Envía entonces los fotones codificados a Bob.
	\item Por cada fotón recibido Bob selecciona también aleatoriamente una base de polarización y lo mide. Si para el fotón recibido se dio la coincidencia que el receptor eligió la misma base de polarización que el emisor, entonces el receptor obtendrá el mismo valor de bit que envió el emisor. Si la base elegida no es la misma que la utilizada por el emisor, el valor que obtendrá al decodificar el fotón será aleatorio. Con los bits recibidos genera la clave en bruto.
	\item Bob ahora utiliza el canal público para informarle a Alice qué fotones logró detectar, y qué base utilizó para medirlos, pero no le dice el resultado de dichas mediciones. Alice le responde con las bases que ella utilizó para generar los fotones que Bob pudo medir. Si Alice y Bob usaron la misma base, los bits obtenidos serán iguales. Si usaron bases distintas, o Eva estuvo midiendo el canal, o incluso se perdieron o alteraron bits en el medio de transmisión, esos bits serán considerados erróneos. La clave que ambos arman con los bits correctos se denomina \textbf{clave tamizada}.
	\item Alice y Bob finalmente realizarán algunas tareas de procesamiento para transformar su clave tamizada en una clave secreta. Estos pasos son similares en cualquier protocolo, y constan de:
	\begin{itemize}
		\item Estimar la tasa de error del canal de comunicación.
		\item Estimar la cantidad máxima de información que podría haberse filtrado a Eva.
		\item Corregir los errores, reduciendo la información entregada a Eva.
	\end{itemize}
	Los bits resultantes serán la clave secreta que ahora podrán utilizar Alice y Bob para cifrar información.
\end{enumerate}

El Cuadro \ref{table:bb84_ejemplo} plantea un ejemplo en el que se pueden apreciar los bits seleccionados por Alice, a saber, $0 1 1 0 1 0 0 1$, las bases seleccionadas, y la polarización obtenida al codificar cada bit en cada base. Esos fotones polarizados son los que recibe Bob, que a su vez selecciona una base particular aleatoria para cada uno, lo mide, y obtiene sus propias decodificaciones en bits. Finalmente tanto Alice como Bob descartan aquellos bits para los que no coincidan las bases de polarización.

\begin{table}[htp]
	\centering
	\caption{Ejemplo de Intercambio BB84.}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|}
		\hline
		Bit de Alice & 0 & 1 & 1 & 0 & 1 & 0 & 0 & 1 \\
		\hline
		Base de Alice & $\oplus$ & $\oplus$ & $\otimes$ & $\oplus$ & $\otimes$ & $\otimes$ & $\otimes$ & $\oplus$ \\
		\hline
		Alice envía & $|\updownarrow\rangle$ & $|\leftrightarrow\rangle$ & $|\nwsearrow\rangle$ & $|\updownarrow\rangle$ & $|\nwsearrow\rangle$ & $|\neswarrow\rangle$ & $|\neswarrow\rangle$ & $|\leftrightarrow\rangle$ \\
		\hline
		Base de Bob & $\oplus$ & $\otimes$ & $\otimes$ & $\otimes$ & $\oplus$ & $\otimes$ & $\oplus$ & $\oplus$ \\
		\hline
		Medición de Bob & $|\updownarrow\rangle$ & $|\neswarrow\rangle$ & $|\nwsearrow\rangle$ & $|\neswarrow\rangle$ & $|\leftrightarrow\rangle$ & $|\neswarrow\rangle$ & $|\leftrightarrow\rangle$ & $|\leftrightarrow\rangle$ \\
		\hline
		Clave Secreta & 0 &  & 1 &  &  & 0 &  & 1 \\
		\hline
	\end{tabular}
	\label{table:bb84_ejemplo}
\end{table}

\begin{comment}
	\begin{figure}[htp]
		\centering
		\caption{\textbf{Ejemplo del intercambio BB84}}
		\includegraphics[width=1.0\textwidth]{img/bb84_ejemplo.jpg}
		\label{fig:bb84_ejemplo}
	\end{figure}
\end{comment}

En ausencia de ruido o de cualquier error en la medición, una diferencia en tan solo un bit en esta comprobación indicaría que Eva se encuentra midiendo fotones en el canal cuántico. En efecto, teniendo en cuenta el principio de incertidumbre de Heisenberg, la medición de un fotón por parte de Eva podría alterar sensiblemente su estado, revelando a Alice y Bob que alguien estuvo intentando interceptar la información intercambiada. En este caso Alice y Bob descartarán su clave secreta y comenzarán una nueva negociación. Además, dado que Eva no sabrá qué base se utilizó para codificar cada bit hasta después de que los nodos de la comunicación intercambien la clave tamizada, deberá adivinarla. Si Eva mide con la base incorrecta, por el mismo principio de incertidumbre la información codificada en la otra base se va a perder. Así, cuando el fotón llegue al destino Bob medirá un valor incorrecto el 50\% de las veces, y el 25\% de los bits medidos por Bob diferirán de los que codificó Alice \citep{rieffel_introduction_2000}.

Debe agregarse que los esquemas de polarización son relativamente fáciles de implementar en el espacio, donde la polarización se conserva, pero en una fibra óptica, en la que incurren otros factores propios del medio físico, resulta más complejo. Como alternativa pueden definirse sistemas de criptografía cuántica basados en codificación de fase \citep{ekert2000physics}

\section{PROTOCOLO B92}

En 1992 el mismo Bennett propuso en una investigación denominada \textit{Criptografía cuántica usando cualquier par de estados no ortogonales} lo que es, en esencia, una versión simplificada de su paper sobre el protocolo BB84 \citep{bennett_quantum_1992}. Esta nueva versión se diferencia de la anterior en que son necesarios únicamente dos estados ortogonales que posibilitan cuatro polarizaciones en BB84.

Como se puede ver en la Fig. \ref{fig:b92_polarizacion}, Alice selecciona las bases entre la horizontal y la diagonal a $ 45\degree $. Los bits polarizados horizontalmente serán 0's, los polarizados diagonalmente serán 1's. Alice envía los fotones polarizados a Bob, que los mide con base aleatoria. Las bases elegidas por Bob serán ortogonales a las de Alice, es decir, Bob medirá con base vertical a 90º y diagonal a $-45\degree$. Alice debe avisar a Bob cuándo envía cada fotón. Si Bob detecta el fotón, significa que usó una base no ortogonal a la de Alice para medirlo, y obtiene el mismo valor que Alice, 0 o 1. Por ejemplo, si Bob detecta un fotón midiendo en la base vertical a $90\degree$, significa que Alice envió un fotón polarizado diagonalmente en $45\degree$, ya que si Alice lo hubiera polarizado en la base horizontal, la medición vertical anularía al fotón por la condición de la mecánica cuántica denominada "borrado" (\textit{erasure} en inglés) \citep{bruss_quantum_2007}. Bob asigna entonces 1 a los fotones que puede medir en la base vertical, y 0 a los que puede medir en la base diagonal a $-45\degree$.

%Luego de la transmisión de fotones polarizados, Bob anuncia a Alice qué fotones pudo medir utilizando su base vertical a $90\degree$ o la diagonal a $-45\degree$, y el resto se descartan, ya que ei Bob no detecta al fotón, no sabría con certeza el estado que envió Alice. Así, ambos únicamente mantienen los valores que puede medir Bob. Eva en este contexto no sabría qué fotones pudo medir Bob de los que ella interceptó, por lo que no podría obtener la clave negociada.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.6\textwidth]{img/b92_polarizacion.jpeg}
	\caption{Polarización en B92 - Codificación de 2 estados.}
	\source{\citep{haitjema2007survey}}
	\label{fig:b92_polarizacion}
\end{figure}

\section{PROTOCOLO E91}

Existen algunas variantes del protocolo BB84, pero por razones de extensión únicamente se considerará una de las más estudiadas, una versión simplificada de E91, diseñado por Artur Eckert en 1991 \citep{eckert_quantum_1991}. Este es un sistema en el que hay una sola fuente que emite partículas entrelazadas (por ejemplo, fotones). Esta fuente puede ser externa, o puede ser uno de los dos extremos de la comunicación. La ventaja de E91 es que genera la clave aleatoriamente de manera natural, ya que es imposible determinar la polarización inicial de los fotones.

\begin{comment}
	Podría simplificarse el intercambio en tres pasos. Durante el primero uno de los extremos, o una fuente externa, origina una secuencia de qubits entrelazados para Alice y Bob, separa cada uno, y cada extremo recibe una de las partículas del cada par. En el segundo paso Alice y Bob eligen, de manera independiente y aleatoria, una secuencia de bases para medir la serie de partículas, como se puede ver en el Cuadro \ref{table:e91_p2}. Los valores resultantes servirán a ambos extremos para generar su clave simétrica de cifrado.
	
	
	\begin{table}[htp]
		\centering
		\caption{\textbf{Protocolo E91 - Segundo paso}}
		\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|}
			\hline
			Número de bit & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
			\hline
			Bases de Alice & $\times$ & $\times$ & + & + & $\times$ & + & $\times$ & + & + & $\times$ & + & $\times$ \\
			\hline
			Medición de Alice & $\nearrow$ & $\nwarrow$ & $\rightarrow$ & $\uparrow$ & $\nearrow$ & $\rightarrow$ & $\nwarrow$ & $\rightarrow$ & $\rightarrow$ & $\nearrow$ & $\rightarrow$ & $\nearrow$ \\
			\hline
			Bases de Bob & $\times$ & + & + & $\times$ & $\times$ & + & + & + & + & $\times$ & $\times$ & + \\
			\hline
			Medición de Bob & $\nearrow$ & $\rightarrow$ & $\rightarrow$ & $\nearrow$ & $\nearrow$ & $\rightarrow$ & $\uparrow$ & $\rightarrow$ & $\rightarrow$ & $\nearrow$ & $\nwarrow$ & $\rightarrow$ \\
			\hline
		\end{tabular}
		\label{table:e91_p2}
	\end{table}
	
	Durante el tercer paso, Alice y Bob comparten sus secuencias de bases seleccionadas. Por cada vez que Alice y Bob hayan seleccionado la misma base, debido a las propiedades del entrelazamiento cuántico, deberían obtener el mismo resultado. En este paso Alice y Bob descartan aquellos bits medidos con bases diferentes, como se ve en el Cuadro \ref{table:e91_p3}.
	
	\begin{table}[htp]
		\centering
		\caption{\textbf{Protocolo E91 - Tercer Paso}}
		\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|}
			\hline
			Número de bit & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
			\hline
			Bases de Alice & $\times$ & $\times$ & + & + & $\times$ & + & $\times$ & + & + & $\times$ & + & $\times$ \\
			\hline
			Canal Público  \\
			\hline
			Bases de Bob & $\times$ & + & + & $\times$ & $\times$ & + & + & + & + & $\times$ & $\times$ & + \\
			\hline
			?`Coinciden? & Si & No & Si & No & Si & Si & No & Si & Si & Si & No & No \\
			\hline
		\end{tabular}
		\label{table:e91_p3}
	\end{table}
	
	
	
	\begin{figure}[htp]
		\centering
		\includegraphics[width=1.0\textwidth]{img/e91.png}
		\caption{\textbf{Protocolo de Eckert E91}}
		\label{fig:e91}
	\end{figure}
\end{comment}

Supóngase una fuente que emite pares de fotones entrelazados de este tipo:
\[ |\psi\rangle = \dfrac{1}{\sqrt{2}}(|\updownarrow\rangle|\leftrightarrow\rangle - |\leftrightarrow\rangle|\updownarrow\rangle) \]

Los fotones se separan y son enviados a los dos destinatarios de la comunicación, Alice y Bob, que los miden y registran su resultado en una de tres bases obtenidas de rotar la base $\oplus$ en el eje z en ángulos predefinidos:
\begin{itemize}
	\item Ángulos de rotación de Alice: $ \phi^a_1 = 0 $, $\phi^a_2 = \frac{1}{4}\pi$ y $\phi^a_3 = \frac{1}{8}\pi$
	\item Ángulos de rotación de Bob  : $ \phi^b_1 = 0 $, $\phi^a_2 = -\frac{1}{8}\pi$ y $\phi^a_3 = \frac{1}{8}\pi$
\end{itemize}

Los usuarios eligen una de sus bases de manera aleatoria, de modo que por cada medición pueden obtener dos resultados posibles: $+1$ (el fotón fue medido en el primer estado de la base elegida), y $-1$ (el fotón fue medido en el otro estado posible de la base elegida).

Puede calcularse el \textbf{coeficiente de correlación} de las mediciones realizadas por Alice en la base rotada por $\phi^a_i$ y por bob en la base rotada por $\phi^b_j$ como sigue:
\[ E(\phi^a_i,\phi^b_j) = P_{++}(\phi^a_i,\phi^b_j) + P_{--}(\phi^a_i,\phi^b_j) + P_{+-}(\phi^a_i,\phi^b_j) + P_{-+}(\phi^a_i,\phi^b_j)\]

Donde $P_{\pm\pm}(\phi^a_i,\phi^b_j)$ representa la probabilidad de haber obtenido el resultado $\pm 1$ con la base definida por $\phi^a_i$, y $\pm 1$ con la base definida por $\phi^b_j$. 

Considérese ahora la siguiente regla cuántica:
\[ E(\phi^a_i, \phi^b_j) = -\cos[ 2(\phi^a_i - \phi^b_j)] \]

Para los dos pares de bases con la \textbf{misma orientación} ($\phi^a_1,\phi^b_1$ y $\phi^a_3,\phi^b_3$) la mecánica cuántica predice que la anticorrelación total del resultado obtenido por Alice y Bob será:
\[ E(\phi^a_1, \phi^b_1) = E(\phi^a_3, \phi^b_3) = -1 \]

Se puede definir la cantidad $S$ compuesta por los coeficientes de correlación para los que Alice y Bob usaron \textbf{diferentes orientaciones}:
\[ S =  E(\phi^a_1, \phi^b_3) + E(\phi^a_1, \phi^b_2) + E(\phi^a_2, \phi^b_3) - E(\phi^a_2, \phi^b_2)\]

Este $S$ es el mismo que el propuesto por el teorema generalizado de Bell, y conocido como la desigualdad de CHSH:
\begin{equation}
	\label{"eq:bell"}
	S = -2\sqrt{2} 
\end{equation}


Al finalizar la transmisión, Alice y Bob pueden anunciar públicamente la orientación de sus polarizaciones elegidos para cada partícula, y dividir las medidas en dos grupos:

\begin{enumerate}
	\setlength\itemsep{0em}
	\item Un grupo con las medidas para las que las bases de medición fueron distintas.
	\item Otro grupo con las medidas para las que las bases fueron las mismas.
\end{enumerate}

Luego, Alice y Bob revelan públicamente los resultados de las mediciones del primer grupo. Esto les permite calcular el valor de $S$, de modo que si las partículas no fueron perturbadas por una medición de Eva, deberían reproducir el resultado de la ecuación \ref{"eq:bell"}. Esto asegura que los valores medidos en el segundo grupo estén correlacionados, y que pueden ser convertidos en una clave secreta.

Un espía como Eva no podría obtener nada de información desde las partículas mientras están en tránsito en el canal cuántico porque no hay nada de información codificada aquí. La información será intercambiada luego de Alice y Bob hayan verificado la legitimidad de su clave tal y como se mencionó. Eva, al no conocer las bases utilizadas por Alice y Bob para medir las partículas de su clave, no podrá interactuar sin perturbar los resultados de las polarizaciones, lo que producirá una reducción del valor de $S$, y será detectada por los extremos legítimos de la comunicación. El teorema de Bell puede de esta forma exponer al atacante.

\begin{comment}
	
\section{SEGURIDAD EN INTERCAMBIO DE CLAVES}

\todo{reducir}

Por la forma de trabajo de los protocolos mencionados, podría pensarse en la criptografía cuántica como un cifrador similar AES en modo CTR (Counter, Contador), o un cifrador simétrico de flujo. El prerequisito para la encriptación simétrica es que ambos extremos conozcan una clave secreta impredecible que se incremente según el tiempo que dure la comunicación cifrada. No obstante, un cifrador de flujo y la encriptación cuántica difieren en algunos detalles:

\begin{itemize}
	\setlength\itemsep{0em}
	\li Un cifrador de flujo genera el texto cifrado operando la entrada con una función matemática y una clave. El QKE usa técnicas físicas para que uno de los extremos genere continuamente bits aleatorios secretos y los codifique.
	\li Un cifrador de flujo puede ser usado para proteger la información enviada a través de cualquier número de intermediarios no confiables en una red. El QKE requiere una conexión de fibra óptica directa entre los extremos de la comunicación, con su hardware asociado. El espionaje en el primer caso se evita con el cifrado, mientras que en el segundo falla dado que al intentar leer el canal se modifica su contenido, interrumpiendo la conexión.
	\li Si un cifrador de flujo está implementado correctamente su seguridad depende de que no se encuentre un vector de ataque al algoritmo. Si el QKE está implementado correctamente, su seguridad se basa en las leyes de la física cuántica.
	\li Un cifrador de flujo moderno puede correr en cualquier CPU actual y generar varios Gibibytes de información por segundo. El QKE utiliza un hardware especial, con un altísimo costo, para generar un flujo de solamente kibibytes por segundo.
\end{itemize}

El intercambio de claves cuántico es seguro incondicionalmente dado que no hay posibilidades de que el atacante pueda realizar cálculos matemáticos difíciles para conseguir la clave, y tampoco pueda violar las leyes de la física. Sin embargo, aún con este nivel de seguridad, los protocolos de intercambio de claves cuánticos son susceptibles a ataques de intermediario, o MITM (Man in the Middle, Hombre en el Medio). En este caso Eva podría hacer de intermediario entre Alice y Bob, y así realizaría un intercambio cuántico con Alice y otro independiente con Bob, lo que le facilitaría interceptar el tráfico en texto plano.

Existen varios vectores de ataque a este tipo de protocolos. En la práctica uno de los factores que influyen en la seguridad es el del ruido en el canal. En los sistemas reales, si Alice y Bob descubren que sus mediciones no correlacionan perfectamente, es decir, existe un determinado porcentaje de los bits cuya medición es errónea, los extremos no pueden saber si esas diferencias fueron causadas por equipos imperfectos que introducen ruido en el canal, o por la presencia de algún atacante.

En la sección anterior se plantearon ejemplos ideales del uso de BB84 y de E91, no obstante en la práctica el medio de transmisión en general induce ruido, por lo que utilizando las versiones ideales de los protocolos propuestos Alice y Bob descartarían la mayoría de las claves por no tener una correlación perfecta, ya que esto supondría la presencia de un atacante.

Existen técnicas de amplificación de la privacidad para reducir la información que Eva puede tener sobre la clave. Alice y Bob antes de aplicar estas técnicas pueden utilizar mecanismos de corrección de errores para eliminar las discrepancias en su clave compartida sin entregar información a Eva. Un ejemplo sería que Alice tomara dos bits de su clave y le enviara el resultado de ejecutar un $XOR$ a Bob. Bob verificaría el $XOR$ entre esos mismos bits de su clave, y le informaría a Alice si coincide o no el resultado. Así podrían llegar a la misma clave sin revelar información que puede ser útil a Eva.


%Finalmente Alice y Bob pueden utilizar amplificación de la privacidad para transformar su clave en una nueva clave a la que Eva no puede llegar si no ha obtenido exactamente la misma clave espiando el canal. Una técnica de amplificación interesante es aquella en la que Alice anuncia a Bob pares de bits de su clave original, y ambos reemplazan en cada bit del par el resultado de realizar un XOR entre dichos bits. Eva no podrá conocer el valor del XOR de cada par con certeza a menos que sepa con precisión el valor de los bits originales.

Finalmente debe añadirse que los equipos actuales para intercambio de claves cuánticas no pueden trabajar con fotones individuales, por lo que hacen uso de láser para generar pequeñas cantidades de luz coherente. Esto da lugar a ataques PNS (Photon Number Splitting, División de Número de Fotones), un tipo de ataque basado en la separación de fotones individuales desde un haz de luz láser \citep{lutkenhaus_security_2000}.
\end{comment}