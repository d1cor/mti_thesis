\cleardoublepage
\chapter{CONCLUSIONES}

Los algoritmos criptográficos utilizados en la actualidad tienen ya décadas de estudio, pruebas y criptoanálisis, lo que brinda a los profesionales de la seguridad informática tranquilidad a la hora de utilizarlos para asegurar las comunicaciones en Internet. Los algoritmos post-cuánticos, por su parte, se encuentran recién transitando los primeros pasos en el camino de la estandarización, por lo que al día de hoy la gran mayoría de ellos no cuentan con una verificación exhaustiva que confirme una seguridad comparable a la de los algoritmos tradicionales. Además, las implementaciones de software que permiten llevar a la práctica estos mecanismos post-cuánticos hoy se encuentran mayormente en estado experimental, y muchas de ellas con un desarrollo muy activo.

El presente trabajo realizó un relevamiento de las implementaciones de software más prometedoras y cuyo objetivo es el de proveer a SSL/TLS de algoritmos resistentes al criptoanálisis cuántico. Un requisito para poder analizar las implementaciones de software y su comportamiento fue disponer de su código fuente. Por esta razón que se puso foco en aquellas implementaciones de software que han sido liberadas bajo licencias de software libre y/o código abierto. Se estudió por un lado la librería liboqs y su integración con OpenSSL, y por otro wolfSSL y sus avances en la incorporación de algoritmos post-cuánticos, puesto que se trata de una implementación orientada a dispositivos embebidos e IoT, un campo que presenta grandes perspectivas de crecimiento a corto y mediano plazo \citep{horwitzIoTTrends20212021}.

Se analizó la arquitectura y los algoritmos de encapsulamiento e intercambio de claves post-cuánticos soportados por liboqs. Se estudió OQS-Open-SSL, la integración de liboqs con OpenSSL en sus versiones 1.0.2 y 1.1.1. OQS-OpenSSL v1.0.2 permitió analizar la negociación de cipher suites TLS v1.2, mientras que la v1.1.1 posibilitó el uso de algoritmos de encapsulamiento de claves en TLSv1.3. A su vez se analizó la integración de OpenSSL v1.1.1 con dos de las implementaciones de protocolo HTTP más utilizadas: Apache y Nginx. Finalmente se realizaron pruebas de la integración de ambas versiones de OQS-OpenSSL con OpenVPN utilizando PQCrypto-VPN.

Se analizó también la integración de wolfSSL con la biblioteca libntru, y se montó una prueba de concepto de TLSv1.2 haciendo uso de cipher suites post-cuánticas basadas en QSH y NTRU. Finalmente se integró wolfSSL con Apache para brindar un servicio HTTPS que incorpora algoritmos post-cuánticos. Para ello se modificó el código fuente de Apache adaptándolo a las funciones NTRU implementadas por wolfSSL. Además se estudió la integración entre wolfSSL y OpenVPN. En este punto se montó el servicio de VPN basado en wolfSSL nativo sin mecanismos post-cuánticos, ya que la incorporación de NTRU en OpenVPN se encuentra en etapas muy tempranas de desarrollo al momento de la realización de este trabajo.

Respecto a OQS-OpenSSL y su integración con Apache, Nginx y OpenVPN, los resultados demuestran estabilidad en las ejecuciones y pruebas. Si bien no es recomendable utilizar estos servicios en producción, se aprecia un estado de desarrollo maduro. No ocurre lo mismo con wolfSSL y su integración con NTRU y Apache. Se evidencian comportamientos erráticos e inestabilidad en las ejecuciones, lo que evidencia que, más allá de que el desarrollo de estas implementaciones sea activo, aún se encuentra en una etapa experimental y no se recomienda su uso en producción.

Si bien la mayoría de las pruebas realizadas con OQS-OpenSSL y wolfSSL pudieron llevarse a cabo, la integración de criptografía post-cuántica no resultó sencilla en algunos casos, y hubo necesidad de modificar y adaptar el código fuente de los servicios para lograr su funcionamiento de manera satisfactoria. Por lo dicho anteriormente se concluye que, al momento de realizar este trabajo, las implementaciones analizadas no son aptas para montar soluciones post-cuánticas en servidores de producción.

Finalmente se mencionaron otras implementaciones que dan soporte a algoritmos de encapsulamiento de claves y cifrado post-cuántico, algunas de las cuales resultan prometedoras por su activo desarrollo.

Además de lo expuesto anteriormente los resultados obtenidos permiten apreciar una elevada actividad en el campo de estudio de la criptografía post-cuántica. Las conferencias anuales de criptografía PQCrypto \citep{bernsteinPostquantumCryptographyConferences2020} están dando un gran impulso al avance científico en este campo, y se convierten en el punto de encuentro de los investigadores.

Durante la realización de esta tesis se evidenció un alto grado de actividad en el desarrollo de las principales implementaciones de software analizadas, lo que da cuenta del interés, por parte de la comunidad de desarrolladores de software libre y de código abierto, por lograr avances prácticos en este campo. 

Finalmente debe destacarse que la naturaleza FLOSS \citep{stallmanFLOSSFOSSGNU2016} de las implementaciones de software utilizadas facilitó la realización de las pruebas de concepto. Esta ventaja permite además que gran cantidad de desarrolladores a nivel mundial creen nuevas herramientas y librerías de código, abriendo el abanico de posibilidades para quien desee experimentar.

Es importante destacar que durante el desarrollo de esta tesis se participó en la comunidad científica logrando las siguientes publicaciones:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Criptografía Post Cuántica, presentado en el 19no Edición del Work-shop de Investigadores en Ciencias de la Computación WICC \citep{cordobaCRIPTOGRAFIAPOSTCUANTICA2017}.
	\li Aplicación de criptografía post cuántica en entornos de producción basados en open source, presentado en el 9no Encuentro de Investigadores y Docentes de Ingeniería EnIDI \citep{cordobaAplicacionCriptografiaPost2017}.
	\li Criptografía Post-Cuántica Integrada en SSL/TLS Y HTTPS, presentado en la 21ra Edición del Workshop de Investigadores en Ciencias de la Computación WICC \citep{cordobaCRIPTOGRAFIAPOSTCUANTICAINTEGRADA2019}.
	\li Servicio HTTPS con intercambio de claves resistente a ataques cuánticos, presentado en el 7mo Congreso Nacional de Ingeniería Informática – Sistemas de Información CONAIISI 2019 \citep{cordobaServicioHTTPSCon2019}.
	\li Implementación de criptografía post-cuántica NTRU en servicios \\
	HTTPS, presentado en el 8vo Congreso Nacional de Ingeniería Informática/Sistemas de Información CONAIISI 2020 \citep{cordobaImplementacionCriptografiaPostcuantica2020}.
	\li Computadoras cuánticas y el futuro de la criptografía, conferencia magistral presentada en el XI Encuentro de Investigadores y Docentes de Ingeniería ENIDI \citep{cordobaComputadorasCuanticasFuturo2021}.
	\li Criptografía Post-Cuántica Integrada en HTTPS, conferencia en el Main Track de la Ekoparty Security Conference 2021 \citep{cordobaCriptografiaPostcuanticaIntegrada2021}.
\end{itemizenospace}


\chapter{TRABAJO FUTURO}

Durante la realización del presente trabajo se mantuvo contacto con el equipo de desarrollo y soporte de wolfSSL, quienes brindaron todo el apoyo técnico necesario para poder concretar la integración de su suite SSL/TLS con criptografía post-cuántica en Apache. Además, uno de los programadores y científicos de computación que integran el equipo de wolfSSL, Daniel Stenberg, es también creador del proyecto cURL, una de las herramientas utilizadas en esta tesis. Esto da cuenta del estrecho vínculo que existe entre ambos proyectos, lo cual podría facilitar futuras colaboraciones en la incorporación y prueba de librerías de cifrado post-cuántico en wolfSSL y su integración con cURL.

\begin{comment}
Finalmente, y a modo ofrecer una perspectiva de la evolución de la criptografía post-cuántica en Internet, cabe mencionar un punto importante no tratado en este trabajo: la transición a los nuevos protocolos y algoritmos. 

Como ejemplo de experiencias anteriores considérese el caso del SHA1, que fue declarado vulnerable en el año 2004, y todavía en 2018 podían verse navegadores web y servidores en producción sin dar soporte a su reemplazo: SHA256. Si bien la migración de SHA1 a SHA256 no requirió el mismo esfuerzo que implementar criptografía post-cuántica en Internet, la experiencia marca los pasos generales necesarios para realizar la transición. La siguiente lista ejemplifica una serie de etapas que se estima deberían seguirse para realizar la migración completa a algoritmos cuánticamente resistentes.

\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li El primer paso sería desarrollar, probar y estandarizar algoritmos post-cuánticos para intercambio de claves y firma digital.
	\li Luego los desarrolladores de las suites de seguridad y criptografía deberían implementarlos en multitud de lenguajes de programación, en librerías, en circuitos embebidos y en hardware criptográfico. 
	\li Los nuevos algoritmos deberían ser incorporados a protocolos estándares como TLS, IPSec u OpenVPN, lo que obligaría a una revisión minuciosa de los mismos antes de llevarlos a producción.
	\li Los fabricantes deberían implementar las actualizaciones en sus productos de software y/o hardware.
\end{enumeratenospace}

Desde aquí en adelante pueden transcurrir años hasta que la mayor parte de los sistemas en Internet hayan dejado de utilizar algoritmos obsoletos. Los datos sensibles de gobiernos y corporaciones deberán ser reencriptados según los nuevos estándares, y cualquier copia de seguridad cifrada con algoritmos \textit{pre-cuánticos} deberá ser destruida, tanto las claves como los datos. La infraestructura de clave pública deberá ser actualizada, será necesario crear nuevas claves privadas y certificados digitales, firmarlos por autoridades certificantes válidas, y redistribuirlos.

También hay que tener en cuenta la fabricación actual de dispositivos sin soporte para criptografía post-cuántica, como es el caso de las computadoras de a bordo de muchos vehículos. Estos sistemas, en su gran mayoría, se mantendrán inalterados por los próximos 15 o 20 años. Por estas razones se espera que algunos criptosistemas actuales se mantengan funcionando por décadas, mas allá de que sean considerados obsoletos.

Todo el proceso de migración probablemente tome no menos de 20 años, por lo que es menester acelerar el desarrollo e investigación en criptosistemas post-cuánticos para adelantarse al surgimiento de las primeras computadoras cuánticas escalables \citep{johnson_cryptographic_2017}.
\end{comment}

A continuación se listan algunos tópicos de investigación que se analizarán y desarrollarán en trabajos posteriores a esta tesis:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Analizar y realizar experimentos de integración de wolfSSL con liboqs para lograr servicios de VPN y HTTPS post-cuánticos basado en algoritmos asimétricos candidatos de la 3ra ronda de estandarización del NIST.
	\li Analizar y realizar pruebas de concepto de implementaciones de otros protocolos de uso común, tales como SSH \citep{ylonenSecureShellSSH2006} o IPSec, con librerías de algoritmos resistentes a ataques cuánticos, y determinar la viabilidad de su uso en entornos experimentales.
	\li Realizar pruebas de integración de criptografía de clave secreta con la herramienta de VPN Wireguard para verificar su viabilidad.
	\li Realizar pruebas de integración de algoritmos post-cuánticos en la negociación TLS de la red TOR, y de esta manera analizar su viabilidad y rendimiento.
	\li Desarrollar librerías de intercambio de claves criptográficas basado en la sincronización de redes neuronales artificiales. Cabe mencionar que el autor de esta tesis se encuentra colaborando en un proyecto de investigación al respecto.
	\li Evaluar la posibilidad de integrar bibliotecas de cifrado post-cuántico en implementaciones de otros servicios, tales como SMTP \citep{klensinSimpleMailTransfer2001}, IMAP \citep{crispinINTERNETMESSAGEACCESS2003} o LDAP \citep{sermersheimLightweightDirectoryAccess2006}.
	\li Desarrollar una distribución GNU/Linux que incorpore librerías e implementaciones de criptografía post-cuántica con la intención de facilitar pruebas de validez y rendimiento (\textit{benchmarking}) por parte de terceros interesados.
\end{itemizenospace}
