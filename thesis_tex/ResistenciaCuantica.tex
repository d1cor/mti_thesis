\section{RESISTENCIA CUÁNTICA}

Todos los sistemas criptográficos mencionados se consideran resistentes tanto a ataques ejecutados desde computadoras clásicas como cuánticas. En otras palabras, no se ha encontrado hasta el momento la forma de utilizar los algoritmos de Shor y Grover para vulnerarlos. Si bien algunos de los ataques que se realizan contra algoritmos clásicos resultan contundentes, otros ataques no son tan concluyentes y pueden mitigarse incrementando el tamaño de las claves.

\begin{comment}
Considérese, por ejemplo, estos tres ataques de factorización contra RSA:

\begin{itemize}
\setlength\itemsep{0em}
\li 1987: El paper original de Rivest, Shamir y Adleman mencionó el algoritmo de Schroppel, que permite factorizar cualquier módulo $n$ de RSA y así romper el cifrado usando $2^{(1+o(1))({(\ln n)}^{1/2}{(\lg \lg n)}^{1/2})}$ operaciones simples ($lg = log_2$).
\li 1988: Pollard introduce un nuevo método de factorización, el algoritmo de tamiz de campo numérico. Éste fue generalizado luego por Buhler, Lenstra y Pomerance para factorizar cualquier módulo RSA usando $2^{(1.9 \cdots + o(1))(\lg n)^{1/3}(\lg \lg n)^{2/3}}$ operaciones simples. Más allá de las cifras, es importante notar que hoy, treinta años después, la factorización conocida más rápida para computadoras clásicas sigue usando $2^{(K + o(1))(\lg n)^{1/3} (\lg \lg n)^{2/3}}$ operaciones, con $K$ constante. Han habido algunas mejoras en $K$ y los destalles de la función $O(1)$, pero se podría asumir que se puede hallar un tamaño mínimo de la clave que resulta óptima y resistente a todos los ataques posibles desde computadoras clásicas.
\li 1994: Shor introduce un algoritmo que permite factorizar cualquier módulo $n$ de RSA usando solamente $(\lg n)^{2+o(1)}$ operaciones simples en una computadora cuántica de tamaño $(\lg n)^{1+o(1)}$. Este algoritmo permite romper RSA en un tiempo que relativamente reducido y útil para un atacante.
\end{itemize}
\end{comment}

Considérese un ataque a un algoritmo de clave pública contemporáneo de RSA, el sistema de cifrado de código Goppa oculto McEliece. Sin entrar en detalles de tamaños de claves y operaciones simples necesarias para romper McEliece, puede decirse que es un algoritmo que no parece vulnerable al criptoanálisis cuántico. Sin embargo no se utiliza actualmente como reemplazo de RSA por un factor fundamental: la eficiencia. Las implementaciones criptográficas de McEliece post-cuánticas requieren claves cuyo tamaño ronda el millón de bits, mientras que las claves RSA seguras para computadoras clásicas tienen un tamaño de 2048 bits en la mayoría de los casos.

\begin{comment}
La Figura \ref{fig:prequantumcrypto} resume el proceso de diseño, análisis y optimización de sistemas criptográficos antes de la llegada de las computadoras cuánticas, mientras que la Figura \ref{fig:postquantumcrypto} resume el mismo proceso en un mundo en el que existen computadoras cuánticsas. Ambas figuras tienen la misma estructura:
\end{comment}

El proceso de diseño, análisis y optimización de criptosistemas se podría estructurar en las tres etapas:

\begin{enumerate}
	\setlength\itemsep{0em}
	\li Los criptógrafos diseñan criptosistemas para cifrado y firma digital.
	\li Los criptoanalistas intentan romper esos criptosistemas.
	\li Los diseñadores de algoritmos encuentran los sistemas más rápidos aún no vulnerados.
\end{enumerate}

Este análisis puede llevarse a cabo tanto con la criptografía tradicional como con la post-cuánticas. La computación cuántica introduce nuevas técnicas de criptoanálisis, por lo que la primera etapa, el diseño de criptosistemas, es independiente de si el criptoanálisis incluye computación cuánticas o no.

\begin{comment}
Entre estos algoritmos pueden mencionarse DES, 3DES, AES, RSA, cifrado McEliece, firmas Merkle, cifrado Merkle-Hellman, cifrado Buchmann-Williams, y algoritmos como ECDSA, HFE, NTRU, entre otros.
\end{comment}

\begin{comment}
La etapa de criptoanálisis es la que produce la diferencia. Aquí, si el criptoanálisis se realiza mediante computadoras tradicionales, se consideran seguros algoritmos tradicionales como AES-256, RSA 2048 o ECDSA, y algoritmos post-cuánticos como las firmas McEliece, HFE o NTRU. Si el criptoanálisis se realiza desde computadoras cuánticas, la lista de algoritmos seguros se reduce, eliminando opciones como RSA o ECDSA.

Considerando un modelo simplificado en el que únicamente se tenga en cuenta el parámetro $b$ como el nivel de seguridad requerido por el usuario, el criptoanálisis clásico considera seguros algunos criptosistemas tales como:

\begin{itemize}
\setlength\itemsep{0em}
\li 3DES para $b \le 112$, AES para $b \le 256$
\li RSA con módulo de $b^{3+o(1)}$ bits
\li McEliece con códigos de longitud $b^{1+o(1)}$
\li Firmas Merkle con hash de $b^{2+o(1)}$ bits
\li BW con discriminante de $b^{1+o(1)}$ bits
\li ECDSA con curvas de $b^{1+o(1)}$ bits
\li HFE con $b^{1+o(1)}$ polinomiales
\li NTRU con $b^{1+o(1)}$ bits
\end{itemize}

Entre estos algoritmos clásicos, los esquemas más eficientes y seguros incluyen, por ejemplo, aquellos basados en ECDSA con curvas de $b^{1+o(1)}$.

Por otro lado, el criptoanálisis cuántico permite romper algunos de los algoritmos listados arriba, por lo que en este ejemplo los criptosistemas seguros se reducen a:

\begin{itemize}
\setlength\itemsep{0em}
\li AES para $b \le 256$
\li McEliece con códigos de longitud $b^{1+o(1)}$
\li Firmas Merkle con hash de $b^{2+o(1)}$ bits
\li HFE con $b^{1+o(1)}$ polinomiales
\li NTRU con $b^{1+o(1)}$ bits
\end{itemize}

Y de esta lista los más eficientes para el usuario final serían algoritmos como NTRU o HFE con $b^{1+o(1)}$ polinomiales.
\end{comment}

\begin{comment}
En la Figura \ref{fig:prequantumcrypto} el criptoanálisis utiliza algoritmos de factorización tales como Lenstra-Lenstra-Lovasz para reducciones basadas en enrejado, cálculos basados Gröbner, entre otros. La Figura \ref{fig:postquantumcrypto} hace uso de un varios algoritmos cuánticos entre los que destacan, por supuesto, Shor y Grover. Nótese que todos los sistemas de clave pública no vulnerables de la Figura \ref{fig:prequantumcrypto} sacan ventaja de las estructuras que pueden ser explotadas por el algoritmo de Shor, por lo que estos sistemas desaparecen en la Figura \ref{fig:postquantumcrypto}.

\todo{redo}
\begin{figure}[htp]
\centering
\caption{\textbf{Criptografía pre-cuántica}}
\includegraphics[width=1.0\textwidth]{img/prequantumcrypto.png}
\label{fig:prequantumcrypto}
\end{figure}

\begin{figure}[htp]
\centering
\caption{\textbf{Criptografía post-cuántica}}
\includegraphics[width=0.8\textwidth]{img/postquantumcrypto.png}
\label{fig:postquantumcrypto}
\end{figure}

Se analizarán ahora algunos sistemas que parecen extremadamente difíciles de vulnerar realizando criptoanálisis cuántico, inclusive utilizando computadoras cuánticas relativamente grandes. Dos de los ejemplos representan sistemas de firma digital, y el otro un sistema de cifrado basado en clave asimétrica.

Todos los ejemplos están parametrizados por $b$, el nivel de seguridad requerido por el usuario, y aunque pueden considerarse otros parámetros, inicialmente se tratarán modelos simples.
\end{comment}


\begin{comment}
El enfoque se centra en algoritmos de clave pública dado que, como se mencionó, el criptoanálisis cuántico no genera mayores efectos en sistemas criptográficos simétricos ni en funciones hash/resumen \\
\citep{bernsteinPostQuantumCryptography2009}. 

\end{comment}
El algoritmo de Grover fuerza al uso de claves más largas para cifradores simétricos, pero el efecto es uniforme en la mayoría de los algoritmos, es decir, un algoritmo tradicional considerado rápido hoy en día, con clave de 256 bits, sigue siendo el candidato mas rápido en cifrado post-cuántico incrementando el tamaño de su clave para mantener un nivel de seguridad razonablemente bueno. Si bien existen algunos algoritmos simétricos actuales que pueden ser rotos con el uso de Shor, no representa una amenaza puesto que estos algoritmos no son los más eficientes.

\subsection{Desafíos de la criptografía post-cuántica}

\begin{comment}
Algunos sistemas criptográficos como RSA con claves de 4096 bits son considerados resistentes a ataques producidos desde computadoras clásicas, pero no a aquellos generados desde computadoras cuánticas. Algunas alternativas, tales como el cifrado McEliece con claves de cuatro millones de bits, son considerados resistentes a computadoras clásicas grandes, y también a computadoras cuánticas.
\end{comment}

Actualmente el mundo no está preparado para migrar a cifrado post-cuántico inmediatamente. Tal vez realmente no es necesaria la criptografía post-cuántica, quizás nadie nunca anuncie el lanzamiento de una computadora cuántica comercial suficientemente grande para romper los algoritmos actuales. Sin embargo, de no investigar este campo hoy, si en algunos años la computación cuántica tiene éxito y es viable, será necesaria la criptografía post-cuántica, y se habrán perdido años de desarrollo e investigación crítica que permitiría hacer frente a esa amenaza en un tiempo acotado.

?`Por qué hay que preocuparse ahora por la amenaza de las computadoras cuánticas? ?`Por qué no continuar usando RSA y ECDSA? Y si alguien anuncia la construcción de una computadora cuántica grande dentro de 10 años, ?`por qué no simplemente migrar a McEliece con claves de millones de bits considerado seguro hoy en día? Para responder esta pregunta deben considerarse tres aspectos: eficiencia, confiabilidad, y usabilidad de los algoritmos post-cuánticos.

\subsubsection{Eficiencia}

Los sistemas post-cuánticos de firma como Lamport o Merkle no pueden competir con el rendimiento actual que presentan los sistemas de curva elíptica para un mismo nivel de seguridad \citep{bernsteinPostQuantumCryptography2009}. Si bien la ineficiencia de la criptografía post-cuántica puede llegar a ser una opción para algunos usuarios, no puede ser implementada, por ejemplo, en servidores de Internet que reciban alta decenas de miles de conexiones por segundo. Hasta hace poco tiempo sitios como Google redirigían el tráfico https a http (sin cifrado SSL/TLS) debido a que la gran cantidad de visitas imposibilitaba el procesamiento de la capa de seguridad. Si ese tipo de empresas hasta hace poco tenían dificultades para afrontar SSL/TLS con algoritmos clásicos, es impensable el uso de algoritmos post-cuánticos hoy en día .

Las restricciones en espacio y tiempo siempre han planteado retos críticos para los criptógrafos, y estos desafíos también seguirán surgiendo en la criptografía post-cuántica. Lo positivo de la situación es que estos retos forzaron grandes incrementos en velocidad de procesamiento en algoritmos clásicos, y se espera que ocurra lo mismo con los algoritmos post-cuánticos.


\begin{comment}
Los sistemas de firma digital basados en curva elíptica que proveen $b$ bits de seguridad contra computadoras clásicas requieren tiempos de $b^{2+o(1)}$. 
Los sistemas de firma digital basados en curva elíptica con firmas y claves de $O(b)$ bits proveen $b$ bits de seguridad contra computadoras clásicas. Los algoritmos de firma y verificación toman un tiempo $b^{2+o(1)}$. Algunos criptosistemas post-cuánticos como Lamport-Diffie y Merkle tienen firmas de una longitud de $b^{2+o(1)}$ y $b^{3+o(1)}$ respectivamente. Si bien hay muchas otras propuestas de algoritmos de firma resistentes, no se conoce ninguno que combine firmas de $O(b)$, claves de $O(b)$, y tiempos de firmado y de verificación polinómicos.

\end{comment}

\subsubsection{Confidencialidad}

Los sistemas de firma Merkle y el cifrado de código Goppa oculto surgieron hace más de cuarenta años y continúan siendo inmunes a los esfuerzos de los criptoanalistas. Muchos otros candidatos de criptografía basada en hash y en código son más modernos, y la criptografía de ecuaciones cuadráticas multivariable y la de retículos proveen una mayor variedad de nuevos candidatos en cifrado post-cuántico. Algunas propuestas anteriores han sido vulneradas, lo que permite inducir que quizás los nuevos sistemas puedan ser vulnerables también en la medida en que los criptoanalistas destinen tiempo y recursos a analizarlos.

Para construir la confiabilidad en estos criptosistemas es menester asegurarse que el criptoanálisis lleva ya tiempo prudencial intentando vulnerarlos. Los criptoanalistas, por su parte, necesitan familiarizarse con la criptografía post-cuántica y experimentar nuevas técnicas de ataque.

Debe considerarse también que se podría seguir usando sistemas clásicos que han sobrevivido muchos años de revisiones, pero los últimos descubrimientos en eficiencia criptográfica llevan a desarrolladores y usuarios a volcarse por sistemas más modernos, pequeños y rápidos para mejorar el rendimiento de sus sistemas.

\subsubsection{Usabilidad}

El criptosistema RSA comenzó siendo una simple función de una sola vía. Hoy no es posible utilizar este tipo de funciones como un método de cifrado seguro. Inicialmente RSA calculaba el cubo de un mensaje módulo $n$, pero el algoritmo actualmente utiliza números aleatorios para rellenar el mensaje y mejorar la seguridad. Además, con la intención de manejar mensajes más largos, encripta una cadena aleatoria corta en vez del mensaje, y usa esa cadena como una clave simétrica para cifrar y autenticar el texto plano. Esta infraestructura alrededor de RSA tomó muchos años de investigación y desarrollo, con innumerables inconvenientes en el camino, tales como la vulnerabilidad de Bleichnbacher en 1998 a la variante "\verb|PKCS#1 v1.5|".

Incluso si una función de cifrado seguro se define y estandariza, necesita implementaciones de software que la haga operativa, y quizás desarrollo de hardware que le permita su integración en una gran variedad de aplicaciones. Los programadores y diseñadores necesitan ser muy cuidadosos, no solo para mantener la corrección y velocidad del algoritmo, sino también para evitar fugas y ataques de canal lateral. Unos pocos años atrás varias implementaciones de RSA y AES fueron vulneradas con ataques de cache-timing, con soluciones parciales como el agregado de instrucciones AES a las CPU de Intel \citep{gueronIntelAdvancedEncryption2010}.

\subsection{Comparación con la criptografía cuántica}

La criptografía cuántica o QKE, como se mencionó en la Sección \ref{section:quantumcrypto}, expande una clave compartida corta en un flujo compartido infinito. Como resultado el QKE permite a los extremos de la comunicación disponer de un flujo secreto de bits que se incrementa linealmente durante el tiempo que dure el canal seguro.


\begin{comment}
El prerequisito para la criptografía cuántica es que todos los extremos involucrados en la comunicación conozcan una clave secreta inicial. Como resultado de la criptografía cuántica ambos extremos van a disponer un flujo secreto de bits que se incrementa linealmente según el tiempo que dure el enlace cifrado entre estos extremos.
\end{comment}


Debido a que los mecanismos implementados por la criptografía cuántica y otras técnicas físicas no serán vulnerados por computadoras cuánticas, se podría argumentar que se trata de un subconjunto dentro de la criptografía post-cuántica. No obstante, esta última tiene varias características que la distinguen:

\begin{itemize}
	\setlength\itemsep{0em}
	\li La criptografía en general, incluida la post-cuántica, cubre un amplio rango de tareas tendientes a asegurar las comunicaciones, que van desde operaciones de clave secreta, firmas digitales y cifrado de clave pública, hasta operaciones de alto nivel como el voto electrónico. La criptografía cuántica gestiona sólo una tarea, la expansión de una clave secreta en claves secretas largas.
	\li La criptografía en general, incluida la post-cuántica, añade algunos sistemas que se probaron seguros, pero también incluye criptosistemas de bajo costo que \textit{se creen} seguros. La criptografía cuántica rechaza sistemas que \textit{se piensan} seguros.
	\li La criptografía post-cuántica incluye criptosistemas que pueden ser usados por una porción grande de las comunicaciones en Internet, los extremos de la comunicación necesitan realizar algunas operaciones adicionales y enviar los datos al otro extremo, pero no requieren hardware nuevo. La criptografía cuántica requiere nuevos dispositivos de red que son, al menos por el momento, extremadamente caros para la mayoría de los usuarios de Internet.
\end{itemize}

\begin{comment}
\subsection{Sistema de firma clave pública basado en hash}

Este sistema requiere una función resumen o hash estándar, llamémosle \verb|H|, que produce una salida de 2b bits, siendo b el nivel de seguridad deseado. Para b=128, por ejemplo, podría seleccionarse una función hash SHA-256. Si bien últimamente se están generando muchas preocupaciones acerca de la seguridad que pueden brindar los algoritmos de resumen, y el NIST está buscando reemplazos para funciones como la mencionada, puede concluirse que actualmente romper un SHA-256 es extremadamente costoso \citep{bernsteinPostQuantumCryptography2009}.

La clave pública del que firma tiene $8b^2$ bits, por ejemplo, para b=128 la clave será de 16KiB. La clave consiste en 4b cadenas de 2 bits denominadas $y_1[0]$, $y_1[1]$, $y_2[0]$, $y_2[1]$, $\cdots$, $y_{2b}[0]$, $y_{2b}[1]$.

Una firma de un mensaje $m$ tiene $2b(2b+1)$ bits, por ejemplo, para un $b=128$ la firma tendrá 8 KiB. La firma consiste en cadenas de 2 bits $r, x_1, x_2, \cdots, x_{2b}$ de modo que los bits $(h_1, \cdots, h_{2b})$ de $H(r,m)$ cumplan con $y_1[h_1] = H(x_1)$, $y_2[h_2] = H(x_2)$ hasta $y_{2b}[h_{2b}] = H(x_{2b})$.

?`Cómo encuentra el sistema el $x$ que con $H(x) = y$? El sistema comienza generando un secreto $x$ y entonces calcula $y=H(x)$. Específicamente, el secreto tiene $8b^2$ bits, a saber, $4b$ cadenas aleatorias independientes con distribución uniforme $x_1[0]$, $x_1[1]$, $x_2[0]$, $x_2[1]$, $\cdots$, $x_{2b}[0]$, $x_{2b}[1]$, de modo que cada string tiene $2b$ bits. Ahora el sistema calcula la clave pública $y_1[0]$, $y_1[1]$, $y_2[0]$, $y_2[1]$, $\cdots$, $y_{2b}[0]$, $y_{2b}[1]$ como $H(x_1[0])$, $H(x_1[1])$, $H(x_2[0])$, $H(x_2[1])$, $\cdots$, $H(x_{2b}[0]$, $H(x_{2b}[1]))$.

Para firmar un mensaje $m$ el sistema genera una cadena aleatoria uniforme $r$, calcula los bits $h_1, \cdots, h_{2b})$ de $H(r,m)$ y finalmente halla la firma del mensaje $m$ como ($r$, $x_1[h_1]$, $\cdots$, $x_{2b}[h_{2b}]$). El sistema descarta entonces los valores $x$ restantes y rechaza seguir firmando mensajes.

Este sistema basado en hash es un sistema de firma de única vez denominado Lamport-Diffie, y puede firmar tanto un mensaje individual, como varios mensajes encadenando firmas digitales. EL verificador, en el caso de que sea un solo mensaje, verificará el hash individual, pero en el caso de que sean \verb|n| mensajes, verificará el primer mensaje, y con el hash del primero deberá verificar el segundo, y así sucesivamente, hasta que al final la verificación del mensaje n-ésimo incluirá los n-1 mensajes anteriores. Otros sistemas más avanzados como el sistema de firma de árbol de hash Merkle escalan logarítmicamente con el número de mensajes firmados.

La criptografía basada en hash es uno de los mecanismos más convincentes para firma digital post-cuántica. El algoritmo de Grover es el algoritmo cuántico mas rápido para invertir la mayoría de las funciones hash. La criptografía basada en hash puede convertir cualquier función difícil de invertir en un sistema de firma segura post-cuántica.

\subsection{Sistema de cifrado de clave pública basado en código}

Se asumen que $b$, el nivel de seguridad, es potencia de 2. 

Definiciones
Algoritmos vulnerables y algoritmos resistentes a ataques cuanticos
Quantum Resistant Public Key Cryptography: A Survey
Public Key Cryptography: Theory and Practice
Quantum cryptography: from theory to practice
Post-Quantum Key Exchange for the Internet and the Open Quantum Safe Project
Post-quantum cryptography-dealing with the fallout of physics success.
Quantum-safe hybrid (QSH) key exchange for Transport Layer Security (TLS) version 1.3
WolfSSL Embedded SSL/TLS Library y el link entre ntru y qsh
\end{comment}
