\section{OQS-OPENSSL V1.1.1 Y OPENVPN}
\label{section:oqs111vpn}

SSL/TLS se utiliza para brindar una capa de seguridad a varios protocolos de aplicación además de HTTP, por ejemplo, SMTP, IMAP, POP, FTP, etc. Entre estos protocolos existen algunos que permiten realizar reds privadas virtuales (VPN, por sus siglas en inglés). Uno de los más utilizado es OpenVPN \citep{openvpnproject/openvpninc.OpenVPNOpenSource2018a}, una herramienta de capa de aplicación multiplataforma y de código abierto.

Microsoft ha realizado un fork de OpenVPN y lo ha integrado con liboqs. Microsoft denominó a este proyecto PQCrypto-VPN \citep{microsoftWelcomePQCryptoVPNProject2018a}. En este apartado se analizará dicha implementación, y se llevarán a cabo las pruebas de concepto correspondiente.

Se siguieron las guías de compilación e instalación provistas por Microsoft. Estas guías incluyen scripts automatizados que descargan tanto liboqs como OpenVPN, y realizan las compilaciones correspondientes. Luego de compilar e instalar PQCrypto-VPN se analizaron y probaron las configuraciones necesarias para establecer el túnel de red mediante algoritmos post-cuánticos en la autenticación, intercambio de claves y certificados digitales.

Se descargó desde los repositorios la última versión de PQCrypto-VPN liberada por Microsoft (v1.3), en la que la empresa integró OQS-OpenSSL v1.1.1 con OpenVPN v2.4.9, por lo que se pudieron probar algoritmos KEM y de firma digital post-cuánticos.

A continuación se resumen los experimentos realizados:
\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li Compilación y análisis de PQCrypto-VPN.
	\li Intercambio de claves tradicional y post-cuántico.
	\li Incorporación de algoritmos asimétricos post-cuánticos.
	\li Pruebas de conexión y análisis de tráfico.
\end{enumeratenospace}

\subsection{Consideraciones técnicas}

Las pruebas de conexión se llevaron a cabo entre dos sistemas GNU/Linux. El servidor OpenVPN es el mismo equipo virtualizado que se utilizó para realizar las demás pruebas de concepto de OQS explicadas previamente en el presente trabajo. Aprovechando esta ventaja se utilizó OQS-OpenSSL v1.1.1 para generar certificados digitales basados en algoritmos post-cuánticos. El equipo cliente utilizado fue un Arch Linux, pero este detalle es intrascendente, tal y como se comentó en la Sección \ref{section:oqs_experiment}, ya que OpenVPN actúa tanto de cliente como de servidor dependiendo de las configuraciones que se le suministren por línea de comandos, o mediante archivos de configuración. La ejecución de OpenVPN se llevó a cabo utilizando un módulo de entorno para evitar conflictos con librerías criptográficas del sistema operativo.

Para identificar ambos equipos se asignó la dirección IP \verb|192.168.0.20| al servidor y la IP \verb|192.168.0.10| al cliente. Esta última es despreciable para las pruebas debido a que no se requiere en ninguna configuración, no obstante es de interés en el análisis de capturas de tráfico y registros de conexión. La Fig. \ref{fig:openvpn_red} muestra un diagrama de la conectividad en red.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openvpn_red.jpg}
	\caption{Esquema de red para el experimento de OpenVPN.}
	\label{fig:openvpn_red}
\end{figure}

El primer paso fue clonar el repositorio oficial de PQCrypto-VPN. El repositorio Git contiene un directorio llamado \verb|openvpn/|, que a su vez incluye dos directorios de interés: \verb|build/| y \verb|config/|. El directorio \verb|build/| contiene las herramientas necesarias para compilar y poner en marcha el OpenVPN, mientras que \verb|config/| provee archivos de configuración de ejemplo.

Una vez clonado el repositorio se utilizó el script \verb|openvpn/build/build.py| para realizar la compilación de la aplicación. Los comandos mostrados en el Alg. \ref{alg:oqsopenssl111vpn_clonado} permitieron crear el directorio de trabajo, clonar el repositorio de PQCrypto-VPN, y compilarlo utilizando el mencionado script.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Clonado del repositorio.},
	label={alg:oqsopenssl111vpn_clonado},
	%firstnumber=1,
	numbers=none,
	]
# Clonado del repositorio:
mkdir /opt/openvpn_ms/PQCryptoVPN_1.3/
cd /opt/openvpn_ms/PQCryptoVPN_1.3/
git clone --recurse-submodules https://github.com/microsoft/PQCrypto-VPN.git

# Compilación de PQCryptoVPN:
cd PQCrypto-VPN_1.3/openvpn/build/
./build.py
\end{lstlisting}

Los primeros intentos de compilación generaron errores al momento de enlazar los binarios de test incorporados con liboqs. Dado que el presente trabajo no se centra en ejecutar dichas pruebas, se omitió su compilación y enlace activando la macro \verb|OQS_BUILD_ONLY_LIB| en el archivo \verb|CMakeLists.txt| de liboqs provisto por Microsoft. Este archivo incluye las configuraciones de compilación y enlace que utilizará la utilidad \verb|ninja| para compilar. Para activar la macro mencionada se cambió su valor de \verb|OFF| a \verb|ON|, (Alg. \ref{alg:oqsopenssl111vpn_onlylib}).

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Clonado del repositorio.},
	label={alg:oqsopenssl111vpn_onlylib},
	firstnumber=39,
	%numbers=none,
	]
#Archivo:
# /opt/openvpn_ms/PQCryptoVPN_1.3/openvpn/build/stage/usr/local/openvpn/build/repos/liboqs/CMakeLists.txt

option(OQS_BUILD_ONLY_LIB "Build only liboqs and do not expose build targets for tests, documentation, and pretty-printing available." ON)
\end{lstlisting}

La compilación y enlace, luego de este cambio, se llevaron a cabo satisfactoriamente, y se crearon los siguientes elementos de interés, que serán de utilidad para las pruebas de concepto:
\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Directorio \verb|stage/|: productos compilados (OpenVPN y OQS-OpenSSL).
	\li Archivo \verb|pq-openvpn-linux-staged.tar.gz|: tarball que empaqueta el directorio stage/ para distribuir la aplicación.
	\li Archivo \verb|openvpn-install-2.4.9-I601-Win10.exe|: Compilación de \\
	OpenVPN integrado con OQS-OpenSSL para poder se instalado en sistemas Windows.
	\li Directorio \verb|docker/|: contiene el Dockerfile necesario para realizar la construcción de PQCrypto-VPN en un contenedor Docker.
\end{itemizenospace}

Dentro del directorio \verb|stage/| se encuentra \verb|stage/usr/local/openvpn/|, un subdirectorio que contiene los siguientes elementos: 

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li \verb|lib/|: bibliotecas compiladas de OQS-OpenSSL v1.1.1, a saber, \\
	\verb|libcrypto.so.1.1| y \verb|libssl.so.1.1|, necesarias para OpenVPN con soporte de algoritmos post-cuánticos.
	\li \verb|sbin/|: Contiene el binario ejecutable de OpenVPN.
	\li \verb|etc/|: En este directorio (vacío inicialmente) se cargarán las configuraciones para las pruebas.
\end{itemizenospace}

Con esta información se creó el módulo de entorno que se ve en el Alg. \ref{alg:oqsopenssl111vpn_envmodule}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN v1.3. Módulo de entorno.},
	label={alg:oqsopenssl111vpn_envmodule},
	%firstnumber=1,
	numbers=none,
	]
#%Module1.0########################################
##
## modules modulefile
##
## File: /usr/share/modules/modulefiles/pqcryptovpn/1.3

set prefix "PQCrypto-VPN"
set version "1.3"

set TOPDIR      /opt
set BUILDDIR  /opt/openvpn_ms/PQCryptoVPN_1.3
set BASEDIR $BUILDDIR/openvpn/build/stage/usr/local/openvpn
setenv BUILDDIR $BUILDDIR
setenv KEYSDIR  $TOPDIR/keys
setenv LD_LIBRARY_PATH $BASEDIR/lib
setenv BASEDIR $BASEDIR
setenv CONFDIR $BASEDIR/etc/config

prepend-path  PATH $BASEDIR/sbin
\end{lstlisting}

Se verificó que el módulo estuviera cargue la versión correcta de OpenVPN como se ve en el Alg. \ref{alg:oqsopenssl111vpn_version}. Esta versión de PQCrypto-VPN, al basar su seguridad en OQS-OpenSSL v1.1.1, no incorpora cipher suites post-cuánticas, por lo que los comandos de consulta de cifradores no arrojan diferencias respecto de una compilación tradicional de OpenVPN.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN v1.3. Versión.},
	label={alg:oqsopenssl111vpn_version},
	%firstnumber=1,
	numbers=none,
	]
# Módulo de entorno:
module load pqcryptovpn/1.3

# Verificación de versión:
openvpn --version
>
OpenVPN 2.4.9 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [MH/PKTINFO] [AEAD] built on Mar 20 2022
library versions: OpenSSL 1.1.1g  21 Apr 2020, Open Quantum Safe 2020-07 snapshot, LZO 2.10
Originally developed by James Yonan
[...]
\end{lstlisting}

Para las pruebas se usaron las claves y certificados provistos por la suite, situados en \verb|openvpn/build/repos/openvpn-2.4.9/sample/sample-keys/|. Este directorio se copió dentro de \verb|/opt/keys/|, que ya se disponía de las implementaciones anteriores, con el objetivo de facilitar el acceso en los experimentos y mantener centralizadas las claves y certificados utilizados. Con la misma intención se copió el directorio \verb|openvpn/config| en \verb|etc/|. Estos comandos pueden verse en el Alg. \ref{alg:oqsopenssl111vpn_copykeysconf}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Copiado de archivos.},
	label={alg:oqsopenssl111vpn_copykeysconf},
	%firstnumber=1,
	numbers=none,
	]
#Módulo de entorno:
module load pqcryptovpn/1.3

# Copia de claves:
cp -r $BUILDDIR/openvpn/build/repos/openvpn-2.4.9/sample/sample-keys $KEYSDIR

# Copia de archios de configuración:
cp -r $BUILDDIR/openvpn/config $CONFDIR
\end{lstlisting}

Dado que la implementación es la misma tanto para el servidor como para el cliente, para realizar las pruebas de conexión se replicaron el directorio de la implementación, \verb|openvpn/|, y el de claves y certificados, \verb|/opt/keys/|, desde el equipo servidor al sistema cliente. De esta forma fue posible ejecutar PQCrypto-VPN en modo servidor en uno de los equipos, y en modo cliente en el otro.

\subsection{Intercambio de claves post-cuántico}

En el equipo servidor se cargaron las configuraciones necesarias para hacer uso de claves, certificados y módulos DH predeterminados del proyecto, ahora ubicados en el directorio \verb|$KEYSDIR|. Un trabajo similar se llevó a cabo en el equipo cliente. Ambas configuraciones pueden verse en el Alg. \ref{alg:oqsopenssl111vpn_ovpn}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Configuraciones.},
	label={alg:oqsopenssl111vpn_ovpn},
	%firstnumber=1,
	numbers=none,
	]
# Dentro de (/opt/openvpn_ms/PQCryptoVPN_1.3/openvpn/build/stage/usr/local/openvpn/)

# Archivo ./etc/config/server.ovpn
ca /opt/keys/sample-keys/ca.crt
cert /opt/keys/sample-keys/server.crt
key /opt/keys/sample-keys/server.key
dh /opt/keys/sample-keys/dh2048.pem

# Archivo ./etc/config/client.ovpn
remote 192.168.0.20 1194		 # dirección IP del servidor
ca /opt/keys/sample-keys/ca.crt
cert /opt/keys/sample-keys/client.crt
key /opt/keys/sample-keys/client.key
\end{lstlisting}

Una vez almacenadas las configuraciones en los archivos \verb|server.ovpn| y\\
\verb|client.ovpn| se ejecutaron el servidor y el cliente OpenVPN respectivamente en los sistemas servidor y cliente como se ve en el Alg. \ref{alg:oqsopenssl111vpn_execservcli}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Ejecución.},
	label={alg:oqsopenssl111vpn_execservcli},
	%firstnumber=1,
	numbers=none,
	]
# Ejecución Servidor:
module load pqcryptovpn/1.3
openvpn --config $CONFDIR/server.ovpn 

# Ejecución Cliente:
module load pqcryptovpn/1.3
openvpn --config $CONFDIR/client.ovpn 
\end{lstlisting}

El registro de conexión del cliente no hace mención a mecanismos post-cuánticos, mientras que el servidor muestra explícitamente el uso de algoritmos tradicionales, tal y como se aprecia en la Fig. \ref{fig:msopenvpn1.3_server_nopqx_nopqk}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{img/msopenvpn1.3_server_nopqx_nopqk.png}
	\caption{PQcryptoVPN. Uso de intercambio tradicional.}
	\label{fig:msopenvpn1.3_server_nopqx_nopqk}
\end{figure}

Se habilitó la opción \verb|ecdh-curve sikep434| en las configuraciones de servidor y cliente. Esta opción permite especificar una curva elíptica para ser utilizada en el intercambio DH. Se volvió a establecer la conexión y ahora los nodos utilizaron el intercambio de claves basado en el algoritmo post-cuántico Sike-p434. Esto se llevó a cabo aún utilizando las mismas claves y certificados tradicionales basados en RSA. El resultado del intercambio de claves post-cuántico usando Sike-p434 puede verse en las Figs. \ref{fig:msopenvpn1.3_server_pqc_nopqk} (servidor) y \ref{fig:msopenvpn1.3_client_pqc_nopqk} (cliente). Si bien Sike-p434 es el algoritmo de intercambio de claves configurado en este experimento, puede utilizarse cualquiera de los mencionados al inicio de este capítulo (véase Sección \ref{section:algs111kem}). Por ejemplo, la Fig. \ref{fig:msopenvpn1.3_client_pqc_nopqk_frodo} muestra una captura del cliente cuando ambos nodos negociaron un intercambio basado en Frodo640-AES.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/msopenvpn1.3_server_pqc_nopqk.png}
	\caption{PQcryptoVPN. Intercambio post-cuántico - Servidor.}
	\label{fig:msopenvpn1.3_server_pqc_nopqk}
\end{figure}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/msopenvpn1.3_client_pqc_nopqk.png}
	\caption{PQcryptoVPN. Intercambio post-cuántico - Cliente.}
	\label{fig:msopenvpn1.3_client_pqc_nopqk}
\end{figure}

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/msopenvpn1.3_client_pqc_nopqk_frodo.png}
	\caption{PQcryptoVPN. Intercambio frodo640aes - Cliente.}
	\label{fig:msopenvpn1.3_client_pqc_nopqk_frodo}
\end{figure}

\subsection{Incorporación de criptografía post-cuántica}

En esta sección se analiza la conexión OpenVPN haciendo uso de claves y certificados digitales basados en algoritmos asimétricos post-cuánticos en lugar del tradicional RSA. El repositorio de PQCrypto-VPN no incluye archivos de claves y certificados post-cuánticos como ejemplo, por lo que se debieron generar manualmente para montar las pruebas de concepto. Si bien las claves y certificados podrían haberse generado utilizando la implementación de OQS-OpenSSL v1.1.1 compilada en la sección \ref{section:oqs111_prueba}, se decidió utilizar la implementación de OpenSSL provista por Microsoft en PQCrypto-VPN para facilitar la reproducción de los experimentos sin generar dependencias externas.

El primer paso fue crear un módulo de entorno que permitiera ejecutar el OQS-OpenSSL v1.1.1 incluido por Microsoft. Dicho módulo de entorno puede verse en el Alg. \ref{alg:oqsopenssl111vpnms_envmodule}. Este módulo de entorno, además, facilitó la comparación de la versión de release del OQS-OpenSSL v1.1.1 compilada previamente en este trabajo, con la incluida por Microsoft en PQCryto-VPN. Dicha información puede verse en el Alg. \ref{alg:oqsopenssl111vpnms_compare}. Aquí puede apreciarse que la versión provista por Microsoft data de Abril del 2020, fecha en la que liberó el repositorio de PQCrypto-VPN, y no se ha actualizado desde entonces, mientras que la versión de OQS-OpenSSL compilada para el presente trabajo se encuentra actualizada.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN v1.3. Módulo de entorno de OQS-OpenSSL.},
	label={alg:oqsopenssl111vpnms_envmodule},
	%firstnumber=1,
	numbers=none,
	]
#%Module1.0########################################
##
## modules modulefile
##
## File: /usr/share/modules/modulefiles/openssl/1.1.1_ms

set prefix "OQS-OpenSSL PQCrypto-VPN"
set version "1.1.1"

set TOPDIR      /opt
set BASEDIR  /opt/openvpn_ms/PQCryptoVPN_1.3/openvpn/build/repos/openssl-oqs
setenv KEYSDIR  $TOPDIR/keys
setenv APPSDIR $BASEDIR/apps
setenv LD_LIBRARY_PATH $BASEDIR

prepend-path  PATH $BASEDIR/apps
\end{lstlisting}

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN v1.3. .},
	label={alg:oqsopenssl111vpnms_compare},
	%firstnumber=1,
	numbers=none,
	]
module load openssl/1.1.1
openssl version
>
OpenSSL 1.1.1m  14 Dec 2021, Open Quantum Safe 2022-01

module load openssl/1.1.1_ms
openssl version
>
OpenSSL 1.1.1g  21 Apr 2020, Open Quantum Safe 2020-07
\end{lstlisting}

Para llevar a cabo los experimentos de conexión utilizando algoritmos asimétricos post-cuánticos se generaron los siguientes elementos:
\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Clave privada y certificado de la CA.
	\li Clave privada y certificado digital del servidor firmado por la CA.
	\li Clave privada y certificado digital del cliente firmado por la CA.
\end{itemizenospace}

Estos elementos se generaron y firmaron en el equipo servidor, y luego se replicaron al cliente únicamente aquellos que necesarios para completar su configuración, a saber:
\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Clave privada del cliente.
	\li Certificado digital del cliente.
	\li Certificado digital de la CA para verificar las firmas.
\end{itemizenospace}

En el presente trabajo se realizaron pruebas de conexión utilizando los algoritmos QTeslaP-I (\verb|qteslapi|), QTeslaP-III (\verb|qteslapiii|), Dilithium4 con curva P384 (\verb|p384_dilithium4|) y QTeslaP-III con curva P384 (\verb|p384_qteslapiii|). Con todos se llevó a cabo la autenticación correctamente. A fines de documentación se detallará la conexión utilizando \verb|p384_qteslapiii|, elegido de manera arbitraria para este caso. El experimento consistió en:

\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li Generación de certificado digital y clave privada de la CA basados en \verb|p384_qteslapiii|.
	\li Generación de certificado digital y clave privada del servidor, y firma del certificado mediante la clave privada de la CA.
	\li Generación de certificado digital y clave privada del cliente, y firma del certificado mediante la clave privada de la CA.
	\li Copiado de archivos al equipo cliente: certificado digital y clave del cliente, y certificado digital de la CA.
	\li Actualización de los archivos de configuración de servidor y cliente para incorporar los elementos generados.
	\li Ejecución del servidor y del cliente para establecer la conexión.
\end{enumeratenospace}

\subsubsection*{Generación de certificados y claves}

Primero se generó el certificado digital y la clave privada de la autoridad certificante utilizando \verb|p384_qteslapiii|. Las claves resultantes de crearon en el directorio \verb|/opt/keys/|.

Segundo, se generó la clave privada y la solicitud de firma de certificado para el servidor OpenVPN. Se utilizó el mismo algoritmo en este experimento. Luego se firmó dicho certificado utilizando la clave privada de la CA. El mismo procedimiento se utilizó para generar la clave y certificado del cliente.

El Alg. \ref{alg:oqsopenssl111vpn_keygensign} muestra el detalle de los comandos utilizados.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Generación y firma de claves.},
	label={alg:oqsopenssl111vpn_keygensign},
	%firstnumber=1,
	numbers=none,
	]
# Carga del módulo de entorno:
module load openssl/1.1.1_ms

# Generación de clave y certificado dse la CA:
openssl req -x509 -new -newkey p384_qteslapiii \
    -keyout $KEYSDIR/p384_qteslapiii_CA.key \
    -out $KEYSDIR/p384_qteslapiii_CA.crt \
    -nodes -subj "/CN=CA p384_qteslapiii" -days 365 \
    -config $APPSDIR/openssl.cnf

# Generación de clave y solicitud de firma del servidor:
openssl req -new -newkey p384_qteslapiii \
	-keyout $KEYSDIR/p384_qteslapiii_srv.key \
	-out $KEYSDIR/p384_qteslapiii_srv.csr \
	-nodes -subj "/CN=SRV p384_qteslapiii" -days 365 \
	-config $APPSDIR/openssl.cnf

# Firma del certificado del servidor:
openssl x509 -req -in $KEYSDIR/p384_qteslapiii_srv.csr \
	-out $KEYSDIR/p384_qteslapiii_srv.crt \
	-CA $KEYSDIR/p384_qteslapiii_CA.crt \
	-CAkey $KEYSDIR/p384_qteslapiii_CA.key \
	-CAcreateserial -days 365

# Generación de clave y solicitud de firma cliente:
openssl req -new -newkey p384_qteslapiii \
	-keyout $KEYSDIR/p384_qteslapiii_cli.key \
	-out $KEYSDIR/p384_qteslapiii_cli.csr \
	-nodes -subj "/CN=CLI p384_qteslapiii" -days 365 \
	-config $APPSDIR/openssl.cnf

# Firma del certificado del cliente:
openssl x509 -req -in $KEYSDIR/p384_qteslapiii_cli.csr \
	-out $KEYSDIR/p384_qteslapiii_cli.crt \
	-CA $KEYSDIR/p384_qteslapiii_CA.crt \
	-CAkey $KEYSDIR/p384_qteslapiii_CA.key \
	-CAcreateserial -days 365
\end{lstlisting}

\subsubsection*{Copia de archivos, configuraciones y ejecución.}

Se copiaron los archivos necesarios al equipo cliente: clave y certificado del cliente, y certificado de la autoridad certificante. Para mantener el orden, dado que los elementos mencionados se crearon en el directorio \verb|/opt/keys| (\verb|$KEYSDIR|) del servidor, se copiaron el la misma ubicación en el cliente. Luego se actualizaron los archivos de configuración para referenciar los nuevos elementos generados. El Alg. \ref{alg:oqsopenssl111vpn_ovpnpq} muestra la configuración final de servidor y cliente, y los comandos necesarios para su ejecución.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Configuraciones.},
	label={alg:oqsopenssl111vpn_ovpnpq},
	%firstnumber=1,
	numbers=none,
	]
# Archivo $CONFDIR/server-pq.ovpn
ca /opt/keys/p384_qteslapiii_CA.crt
cert /opt/keys/p384_qteslapiii_srv.crt
key /opt/keys/p384_qteslapiii_srv.key
dh /opt/keys/sample-keys/dh2048.pem

# Archivo $CONFDIR/client-pq.ovpn
remote 192.168.0.20 1194		 # dirección IP del servidor
ca /opt/keys/p384_qteslapiii_CA.crt
cert /opt/keys/p384_qteslapiii_cli.crt
key /opt/keys/p384_qteslapiii_cli.key

# Ejecución del servidor:
module load pqcryptovpn/1.3
openvpn --config $CONFDIR/server-pq.ovpn

# Ejecución del cliente:
module load pqcryptovpn/1.3
openvpn --config $CONFDIR/client-pq.ovpn
\end{lstlisting}

\begin{comment}
	Finalmente se lanzó servidor y cliente en sus respectivos equipos como se muestra en el Alg. \ref{alg:oqsopenssl111vpn_pqexec}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Ejecución.},
	label={alg:oqsopenssl111vpn_pqexec},
	%firstnumber=1,
	numbers=none,
	]
# Ejecución del servidor:
module load pqcryptovpn/1.3
openvpn --config $CONFDIR/server-pq.ovpn

# Ejecución del cliente:
module load pqcryptovpn/1.3
openvpn --config $CONFDIR/client-pq.ovpn
\end{lstlisting}

\end{comment}
Si bien en las salidas estos comandos de conexión del OpenVPN no muestra los algoritmos que dieron origen a certificados digitales y claves, la conexión se llevó a cabo satisfactoriamente, y ambos nodos pudieron verificar a su contraparte, como puede observarse en los extractos de la salida mostrados en el Alg. \ref{alg:oqsopenssl111vpn_output}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Verificación de firmas.},
	label={alg:oqsopenssl111vpn_output},
	%firstnumber=1,
	numbers=none,
	]
# Salida del Servidor verifica cert. de CA y cliente:
[...]
Sun Mar 20 19:23:27 2022 us=22393 192.168.0.10:47803 VERIFY OK: depth=1, CN=CA p384_qteslapiii
Sun Mar 20 19:23:27 2022 us=25295 192.168.0.10:47803 VERIFY OK: depth=0, CN=CLI p384_qteslapiii
[...]

# Salida del Cliente verifica cert. de CA y servidor:
[...]
Sun Mar 20 19:23:26 2022 us=944309 VERIFY OK: depth=1, CN=CA p384_qteslapiii
Sun Mar 20 19:23:26 2022 us=947149 VERIFY OK: depth=0, CN=SRV p384_qteslapiii
[...]
\end{lstlisting}

\subsubsection*{Análisis de captura de tráfico.}

Al establecer la conexión TLS el cliente ofrece los algoritmos de firma digital soportados. Un fragmento del tráfico capturado se ve en la Fig. \ref{fig:openssl111_openvpn_cliserv_sig}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_openvpn_cliserv_sig.png}
	\caption{Algoritmos de firma digital PQCrypto-VPN.}
	\label{fig:openssl111_openvpn_cliserv_sig}
\end{figure}

En este caso particular se muestran menos algoritmos de firma que los utilizados por OQS-OpenSSL v1.1.1 nativo mostrados en la Fig. \ref{fig:openssl111_oqsmaster_cliserv_sig}. Esto se debe a que, si bien las versiones de OQS-OpenSSL (v1.1.1) y de la librería liboqs (v0.3.0) analizadas con anterioridad en esta tesis son las mismas que las utilizadas en PQCrypto-VPN, esta última data de Abril de 2020, mientras que la rama estable analizada en este trabajo es de Diciembre de 2021.

Uno de los cambios más visibles es el número de identificación de cada algoritmo de firma digital. Por ejemplo, el algoritmo Dilithium3 incluido en PQCrypto-VPN se identifica con el código \verb|0xfe06|, y en la versión de OQS-OpenSSL compilada previamente para esta tesis tiene el código \verb|0xfea3|. Además esta última versión incorpora algoritmos no disponibles en la implementación de Microsoft, como Dilithium5 y sus variantes híbridas.

El archivo de cabecera \verb|ssl_local.h|, ubicado dentro del directorio \verb|ssl| de la implementación de OQS-OpenSSL incluye las macros que definen los algoritmos mostrados en la Fig. \ref{fig:openssl111_oqsmaster_cliserv_sig}. Particularmente el archivo es el siguiente:

\begin{lstlisting}[
	language={Bash},
	caption={},
	label={alg:oqsopenssl111vpn_exec},
	%firstnumber=1,
	numbers=none,
	]
$BUILDDIR/openvpn/build/repos/openssl-oqs/ssl/ssl_local.h
\end{lstlisting}

Para analizar las diferencias entre los algoritmos soportados por el OQS-OpenSSL v1.1.1 compilado en el presente trabajo y los disponibles en la release de PQCrypto-VPN, se creó un archivo de diferencias, que puede encontrarse en el repositorio Git que acompaña a esta tesis \citep{cordobaTesisMTIDiego2022}.

\begin{lstlisting}[
	language={Bash},
	caption={PQCryptoVPN. Listar algoritmos.},
	label={alg:oqsopenssl111vpn_listalgos},
	%firstnumber=1,
	numbers=none,
	]
module load openssl/1.1.1_ms
openssl list -public-key-algorithms | grep Name \
	| sort > openssl_1.1.1_publickey_algos_sortnames.txt

module load openssl/1.1.1
openssl list -public-key-algorithms | grep Name \
	|sort > openssl_1.1.1_publickey_algos_sortnames.txt

diff openssl_1.1.1ms_publickey_algos_sortnames.txt \
	openssl_1.1.1_publickey_algos_sortnames.txt \
	> openssl_1.1.1ms_vs_1.1.1_publickey_algos.diff
\end{lstlisting}

Ambas implementaciones coinciden en los algoritmos de clave pública tradicionales, pero difieren en los algoritmos asimétricos post-cuánticos soportados. Mayormente las diferencias se deben a que la última versión de OQS-OpenSSL quitó el soporte a muchos algoritmos que no quedaron finalistas en la 3ra ronda de estandarización del NIST. A continuación se listan algunas diferencias importantes de la versión de OQS-OpenSSL utilizada en el presente trabajo, respecto de la provista por Microsoft.

\begin{itemizenospace}
\setlength\itemsep{0em}
\li Se eliminó el soporte para Dilithium4 y su variante híbrida p384.
\li Se eliminó el soporte para mqdss3148 y su variante híbrida p256.
\li El algoritmo p256\_dilithium3 cambió a la curva p384.
\li Se quitó soporte para el algoritmo Picnic2 puro y variantes híbridas.
\li Se eliminó el soporte para QteslapIII y su variante híbrida p256.
\li Se agregó el algoritmo Dilithium5, y variantes de Dilithium 2, 3 y 5 combinadas con AES.
%\li Se agregaron variantes de Dilithium 2, 3 y 5 combinadas con AES.
\li Se agregó el algoritmo Picnic3 y su variante híbrida con curva p256.
\end{itemizenospace}

%Si se compara esta tabla con la que se obtuvo al analizar el código OQS-OpenSSL v1.1.1, Tabla \ref{table:sig_oqs111_pcap}, se aprecia que la versión utilizada por Microsoft implementa más variantes de algoritmos, entre las que se encuentran las listadas en la Tabla \ref{table:sig_oqs111_pqcryptovpn}.

\begin{comment}
	\begin{table}[htp]
	\centering
	\caption{\textbf{OQS-OpenSSL v1.1.1 - Algoritmos de firma - diferencias PQCrypto-VPN.}}
	\begin{tabular}{|l|l|}
		\hline
		Dilithium3 y sus variantes \verb|p256| y \verb|rsa3072| \\
		\hline
		\verb|falcon512| y sus variantes \verb|p256|, \verb|rsa3072| y \verb|p521| \\
		\hline
		\verb|mqdss3148| y sus variantes \verb|p256| y \verb|rsa3072|\\
		\hline
		\verb|rainbowIcclassic| y sus variantes \verb|p256| y \verb|rsa3072|\\
		\hline
		\verb|rainbowVcclassic| y su variante \verb|p521|\\
		\hline
		\verb|sphincsharaka128frobust| y sus variantes \verb|p256| y \verb|rsa3072|\\
		\hline
	\end{tabular}
	\label{table:sig_oqs111_pqcryptovpn}
\end{table}

\begin{table}[htp]
	\centering
	\caption{Algoritmos de Firma Digital - PQCrypto-VPN.}
	\begin{tabular}{|l|l|}
		\hline
		\thead{Código} & \thead{Algoritmo} \\
		\hline
		\verb|0xfe00| & \verb|TLSEXT_SIGALG_oqs_sig_default| \\
		\hline
		\verb|0xfe01| & \verb|TLSEXT_SIGALG_p256_oqs_sig_default| \\
		\hline
		\verb|0xfe02| & \verb|TLSEXT_SIGALG_rsa3072_oqs_sig_default| \\
		\hline
		\verb|0xfe03| & \verb|TLSEXT_SIGALG_dilithium2| \\
		\hline
		\verb|0xfe04| & \verb|TLSEXT_SIGALG_p256_dilithium2| \\
		\hline
		\verb|0xfe05| & \verb|TLSEXT_SIGALG_rsa3072_dilithium2| \\
		\hline
		\verb|0xfe06| & \verb|TLSEXT_SIGALG_dilithium3| \\
		\hline
		\verb|0xfe07| & \verb|TLSEXT_SIGALG_p256_dilithium3| \\
		\hline
		\verb|0xfe08| & \verb|TLSEXT_SIGALG_rsa3072_dilithium3| \\
		\hline
		\verb|0xfe09| & \verb|TLSEXT_SIGALG_dilithium4| \\
		\hline
		\verb|0xfe0a| & \verb|TLSEXT_SIGALG_p384_dilithium4| \\
		\hline
		\verb|0xfe0b| & \verb|TLSEXT_SIGALG_falcon512| \\
		\hline
		\verb|0xfe0c| & \verb|TLSEXT_SIGALG_p256_falcon512| \\
		\hline
		\verb|0xfe0d| & \verb|TLSEXT_SIGALG_rsa3072_falcon512| \\
		\hline
		\verb|0xfe0e| & \verb|TLSEXT_SIGALG_falcon1024| \\
		\hline
		\verb|0xfe0f| & \verb|TLSEXT_SIGALG_p521_falcon1024| \\
		\hline
		\verb|0xfe10| & \verb|TLSEXT_SIGALG_mqdss3148| \\
		\hline
		\verb|0xfe11| & \verb|TLSEXT_SIGALG_p256_mqdss3148| \\
		\hline
		\verb|0xfe12| & \verb|TLSEXT_SIGALG_rsa3072_mqdss3148| \\
		\hline
		\verb|0xfe15| & \verb|TLSEXT_SIGALG_picnicl1fs| \\
		\hline
		\verb|0xfe16| & \verb|TLSEXT_SIGALG_p256_picnicl1fs| \\
		\hline
		\verb|0xfe17| & \verb|TLSEXT_SIGALG_rsa3072_picnicl1fs| \\
		\hline
		\verb|0xfe1b| & \verb|TLSEXT_SIGALG_picnic2l1fs| \\
		\hline
		\verb|0xfe1c| & \verb|TLSEXT_SIGALG_p256_picnic2l1fs| \\
		\hline
		\verb|0xfe1d| & \verb|TLSEXT_SIGALG_rsa3072_picnic2l1fs| \\
		\hline
		\verb|0xfe22| & \verb|TLSEXT_SIGALG_qteslapi| \\
		\hline
		\verb|0xfe23| & \verb|TLSEXT_SIGALG_p256_qteslapi| \\
		\hline
		\verb|0xfe24| & \verb|TLSEXT_SIGALG_rsa3072_qteslapi| \\
		\hline
		\verb|0xfe25| & \verb|TLSEXT_SIGALG_qteslapiii| \\
		\hline
		\verb|0xfe26| & \verb|TLSEXT_SIGALG_p384_qteslapiii| \\
		\hline
		\verb|0xfe27| & \verb|TLSEXT_SIGALG_rainbowIaclassic| \\
		\hline
		\verb|0xfe28| & \verb|TLSEXT_SIGALG_p256_rainbowIaclassic| \\
		\hline
		\verb|0xfe29| & \verb|TLSEXT_SIGALG_rsa3072_rainbowIaclassic| \\
		\hline
		\verb|0xfe3c| & \verb|TLSEXT_SIGALG_rainbowVcclassic| \\
		\hline
		\verb|0xfe3d| & \verb|TLSEXT_SIGALG_p521_rainbowVcclassic| \\
		\hline
		\verb|0xfe42| & \verb|TLSEXT_SIGALG_sphincsharaka128frobust| \\
		\hline
		\verb|0xfe43| & \verb|TLSEXT_SIGALG_p256_sphincsharaka128frobust| \\
		\hline
		\verb|0xfe44| & \verb|TLSEXT_SIGALG_rsa3072_sphincsharaka128frobust| \\
		\hline

	\end{tabular}
	\label{table:sig_oqs111_vpn}
\end{table}
\end{comment}



\begin{comment}
\clearpage
\todo{anexo?}
\begin{code}
///// OQS_TEMPLATE_FRAGMENT_DEFINE_SIG_CODE_POINTS_START
/* The following are all private use code points */
#define TLSEXT_SIGALG_oqs_sig_default 0xfe00
#define TLSEXT_SIGALG_p256_oqs_sig_default 0xfe01
#define TLSEXT_SIGALG_rsa3072_oqs_sig_default 0xfe02
#define TLSEXT_SIGALG_dilithium2 0xfe03
#define TLSEXT_SIGALG_p256_dilithium2 0xfe04
#define TLSEXT_SIGALG_rsa3072_dilithium2 0xfe05
#define TLSEXT_SIGALG_dilithium3 0xfe06
#define TLSEXT_SIGALG_p256_dilithium3 0xfe07
#define TLSEXT_SIGALG_rsa3072_dilithium3 0xfe08
#define TLSEXT_SIGALG_dilithium4 0xfe09
#define TLSEXT_SIGALG_p384_dilithium4 0xfe0a
#define TLSEXT_SIGALG_falcon512 0xfe0b
#define TLSEXT_SIGALG_p256_falcon512 0xfe0c
#define TLSEXT_SIGALG_rsa3072_falcon512 0xfe0d
#define TLSEXT_SIGALG_falcon1024 0xfe0e
#define TLSEXT_SIGALG_p521_falcon1024 0xfe0f
#define TLSEXT_SIGALG_mqdss3148 0xfe10
#define TLSEXT_SIGALG_p256_mqdss3148 0xfe11
#define TLSEXT_SIGALG_rsa3072_mqdss3148 0xfe12
#define TLSEXT_SIGALG_picnicl1fs 0xfe15
#define TLSEXT_SIGALG_p256_picnicl1fs 0xfe16
#define TLSEXT_SIGALG_rsa3072_picnicl1fs 0xfe17
#define TLSEXT_SIGALG_picnic2l1fs 0xfe1b
#define TLSEXT_SIGALG_p256_picnic2l1fs 0xfe1c
#define TLSEXT_SIGALG_rsa3072_picnic2l1fs 0xfe1d
#define TLSEXT_SIGALG_qteslapi 0xfe22
#define TLSEXT_SIGALG_p256_qteslapi 0xfe23
#define TLSEXT_SIGALG_rsa3072_qteslapi 0xfe24
#define TLSEXT_SIGALG_qteslapiii 0xfe25
#define TLSEXT_SIGALG_p384_qteslapiii 0xfe26
#define TLSEXT_SIGALG_rainbowIaclassic 0xfe27
#define TLSEXT_SIGALG_p256_rainbowIaclassic 0xfe28
#define TLSEXT_SIGALG_rsa3072_rainbowIaclassic 0xfe29
#define TLSEXT_SIGALG_rainbowVcclassic 0xfe3c
#define TLSEXT_SIGALG_p521_rainbowVcclassic 0xfe3d
#define TLSEXT_SIGALG_sphincsharaka128frobust 0xfe42
#define TLSEXT_SIGALG_p256_sphincsharaka128frobust 0xfe43
#define TLSEXT_SIGALG_rsa3072_sphincsharaka128frobust 0xfe44
///// OQS_TEMPLATE_FRAGMENT_DEFINE_SIG_CODE_POINTS_END
\end{code}

\end{comment}

