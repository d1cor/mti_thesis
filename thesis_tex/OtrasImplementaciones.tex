\cleardoublepage
\chapter{OTRAS IMPLEMENTACIONES}
\label{section:otrasimplementaciones}

Durante el transcurso de la investigación llevada a cabo para completar el presente trabajo se produjeron importantes cambios tanto en las bibliotecas de código que proveen criptografía post-cuántica como en las implementaciones de software que las utilizan. Así, hubieron proyectos que al inicio de esta investigación se creían prometedores, que luego dejaron de actualizarse, y otros que en su momento no existían y hoy representan interesantes propuestas.

Entre los proyectos que quedaron obsoletos se cuenta OpenSSL-RingLWE \citep{singhPostQuantumForkOpenSSL2015}, reseñado más adelante en este capítulo, TinySSH \citep{mojzisTinyssh2018} como alternativa post-cuántica a OpenSSH \citep{opensshOpenSSH2021}, o Codecrypt \citep{kratochvilCodecryptPostquantumCryptography2018} como reemplazo de GNUPG \citep{thegnupgprojectGNUPrivacyGuard2021}. Los siguientes apartados mencionarán algunas características de las implementaciones más relevantes.

\begin{comment}
https://tinyssh.org/
https://github.com/exaexa/codecrypt

\end{comment}
\section{OQS-BORINGSSL}

Google originalmente utilizó OpenSSL para proveer de seguridad SSL/TLS a sus plataformas de software. Durante años desarrolló y mantuvo parches para OpenSSL, y los integraba en sus productos. Con el incremento de la cantidad y complejidad de los productos y servicios de Google, el esfuerzo para realizar y mantener estos parches fue cada vez mayor. Así fue que decidieron crear un fork de OpenSSL que se adaptara mejor a sus líneas de desarrollo. Este fork se denominó BoringSSL \citep{googleinc.BoringsslGitGoogle2020a}. Se trata de un proyecto de código abierto optimizado por Google para sus plataformas de software, no es una implementación de SSL/TLS de propósito general como OpenSSL, y no se recomienda su uso por terceros debido a que que su API no es estable y recibe cambios frecuentes.

El proyecto OQS realizó un fork de esta aplicación y le integró liboqs para proveerle algoritmos de autenticación y de intercambio de claves post-cuánticos, dando origen a OQS-BoringSSL \citep{openquantumsafeOQSBoringSSL2020}. Como otras implementaciones de OQS, se trata de un proyecto experimental para prototipado de soluciones resistentes a ataques cuánticos, y no es recomendable para servicios en producción.

Si bien los algoritmos soportados por esta implementación no están aún estandarizados por el NIST, no existen vulnerabilidades reportadas para los mismos. No obstante, al igual que con OQS-OpenSSL, la principal recomendación es utilizar algoritmos híbridos para lograr, como mínimo, una seguridad equivalente a la del cifrado tradicional.

\section{OPENSSL - RINGLWE}

Una implementación que en su momento fue considerada para analizar detalladamente en el presente trabajo es OpenSSL-RingLWE \citep{singhPostQuantumForkOpenSSL2015}. El estudio finalmente fue descartado debido a que el proyecto tuvo su última actualización en mayo de 2016.

OpenSSL-RingLWE es un fork de OpenSSL v1.0.2e, y por ello hoy se considera obsoleto. Provee seguridad post-cuántica utilizando Ring-LWE, una eficiente alternativa a Diffie-Hellman para intercambio de claves que se supone segura contra ataques de computadoras cuánticas y tradicionales. Se basa en problemas de anillos de aprendizaje con errores (RLWE, por sus siglas en inglés) y define dos cipher suites:

\begin{itemizenospace}
	\li RLWE-RSA-AES128-GCM-SHA256
	\li RLWEP-RSA-AES128-GCM-SHA256
\end{itemizenospace}

Ambos brindan seguridad híbrida de 256 bits con RSA, el primero de ellos lo hace utilizando claves públicas de 16384 bits, mientras que el segundo usa claves de 13120 bits. El proyecto quedó suspendido antes de poder implementar las cipher suites basadas en ECDSA. La implementación además era altamente parametrizable usando la biblioteca libcrypt con diferentes niveles de seguridad para RLWE. Si bien el proyecto se encuentra discontinuado, representa una de las pocas alternativas basadas en OpenSSL que no dependen de las librerías liboqs.

\section{VPN WIREGUARD}

Wireguard \citep{donenfeldWireGuardNextGeneration2017} es una aplicación y protocolo de comunicación que permite establecer redes privadas virtuales, liberada bajo licencia de software libre GPLv2. Corre como un módulo del núcleo Linux o BSD, y tiene como objetivo ofrecer mejor rendimiento, mayor tolerancia a fallos y configuración más amigable respecto de aplicaciones ampliamente utilizadas como OpenVPN e IPSec. La primer versión estable para Linux se liberó en Marzo de 2020, y su desarrollo se encuentra activo, por lo que se espera crezca a corto plazo y se extienda su uso.

Wireguard implementa el intercambio o handshake Noise\_IK \citep{perrinNoiseProtocolFramework2018}, y todo el tráfico de red que genera viaja sobre transporte UDP. Utiliza el protocolo ChaCha20 para cifrado simétrico, ECDH con Curve25519 para el intercambio de claves inicial, y hashes basados en BLAKE2. Su intercambio de claves facilita el control de suplantación de identidad, evita los ataques de reenvío (replay attacks) y brinda PFS, entre otras.

El protocolo Wireguard no cuenta con criptografía post-cuántica nativa, ni integra librerías externas como liboqs. Sin embargo, sí añade con una capa de criptografía simétrica opcional para utilizar en combinación con los algoritmos de clave pública, y que puede integrar fácilmente mecanismos post-cuánticos al intercambio de claves \citep{donenfeldProtocolCryptographyWireGuard2020}.

\section{IPSEC}

IPSec \citep{Frankel2011IPS} es uno de los conjuntos de protocolos de redes privadas virtuales en capa de red más importante y confiable en la actualidad. Entre los protocolos que utiliza IPSec se encuentra IKEv2 (Internet Key Exchange, Intercambio de claves en Internet) \citep{Kaufman2005InternetKE} para la negociación inicial del canal seguro. IKE realiza un intercambio de claves basado en ECDH, un algoritmo que, como se mencionó, se considera vulnerable al criptoanálisis cuántico.

En 2017 se presentó un borrador de estándar a la IETF \citep{fluhrer2019postquantum} que luego fue reemplazado por la RFC8784 en junio de 2020 \citep{fluhrerMixingPresharedKeys2020}. El documento plantea la incorporación de una clave pre-compartida post-cuántica (PPK, por sus siglas en inglés) junto con la negociación de DH o ECDH. Así, ambos extremos de la comunicación deberán soportar PPK para establecer el túnel. En el caso de que uno de los extremos no lo soporte, se establecería un túnel IPSec tradicional.

Por su parte, y de forma independiente, StrongSwan, una de las implementaciones de IPSec de código abierto más activas, propuso en 2015 la incorporación de NTRU en la negociación de IKE a través de un plugin, de modo que podría lograr seguridad post-cuántica durante la primera etapa de establecimiento del túnel seguro \citep{strongswanNTRU2015}.

\section{ALTERNATIVAS SEGURAS A SSH}

En esta sección se mencionarán algunas alternativas a OpenSSH que, de una u otra forma, permiten el agregado de algoritmos post-cuánticos en la comunicación segura.

\subsection*{XS: eXperimental Shell}

XS \citep{rlabsRLabsXs} es una alternativa simple a SSH escrita en lenguaje Go, de código abierto (licencia MIT) y cuyo desarrollo es activo. Permite sesiones remotas interactivas y no interactivas, transferencia de archivos, y ofuscación opcional del tráfico de red. Como algoritmos de intercambio de claves soporta tres variantes:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Intercambio basado en HerraduraKEx \citep{herreraHerraduraKEx2019}.
	\li Encapsulamiento de claves KYBER IND-CCA-2 \citep{ducasCRYSTALSKyberVersionSubmission2019}.
	\li Algoritmo de cifrado NewHope \citep{alkim_newhope_2017}.
\end{itemizenospace}

KYBER es un mecanismo de encapsulamiento de claves (KEM) cuya seguridad se basa en la dificultad para resolver problemas de aprendizaje con errores (LWE, por sus siglas en inglés). Es un algoritmo post-cuántico basado en retículos. NewHope es un método de intercambio de claves también basado en Ring-LWE aceptado en la segunda ronda de estandarización del NIST, aunque no fue incluido en la tercera convocatoria.

\subsection*{OQS-OpenSSH}

OQS-OpenSSH \citep{openquantumsafeGitHubOpenquantumsafeOpenssh2018} es un fork de OpenSSH que incorpora criptografía post-cuántica integrando liboqs. Su última versión disponible es la v7.9. Esta implementación es experimental, y no es recomendable su uso en servicios de producción. Tiene las mismas limitaciones que se comentaron para otras integraciones de liboqs tales como OQS-OpenSSL, por lo que, al igual que en ellas, es recomendable el uso de criptografía híbrida en la negociación del túnel cifrado.

\section{CRIPTOGRAFÍA POST-CUÁNTICA EN TOR}

En \citep{zsoltQuantumsafeTOR2019} se presenta un resumen de los mecanismos de cifrado e intercambio de claves resistentes a ataques cuánticos, explica la arquitectura de la red TOR (The Onion Routing - Enrutamiento de Cebolla) \citep{thetorprojectTorProjectAnonymity2021}, y propone la incorporación de intercambio de claves post-cuántico en la conexión. Específicamente plantea el uso de mecanismos de encapsulamiento de claves asimétricas integrando liboqs en el software TOR. Realiza una prueba de concepto utilizando SweetOnions \citep{leonhethefirstSweetOnionsMakingOnion2016} como implementación de TOR. La implementación de SweetOnions utilizada está desarrollada en Python 2.7, mientras que liboqs está escrita en C, y posee un wrapper en Python 3, por lo que debió utilizar el paquete \textit{future} de Python para realizar la conversión de versiones, y de esta manera poder llevar a cabo la integración. Cabe mencionar que hoy se encuentra disponible un fork de SweetOnions reescrito en Python 3 \citep{maitrerenardSweetOnionsMakingOnion2020}, no presente al momento de la publicación de \citep{zsoltQuantumsafeTOR2019}.

TOR logra su seguridad utilizando una combinación de criptografía simétrica AES y mecanismos asimétricos RSA. Usa RSA para cifrar la clave secreta AES que se utiliza para dar seguridad al tráfico de red dentro del túnel TOR. \citep{zsoltQuantumsafeTOR2019} plantea dos soluciones prácticas, una post-cuántica pura y otra híbrida. La solución post-cuántica hace uso de algoritmos resistentes para remplazar a RSA en la negociación del canal, mientras que la solución híbrida cifra la clave AES con un algoritmo post-cuántico previo al cifrado RSA, lo que le brinda, como mínimo, seguridad equivalente a la de RSA. Como algoritmos de encapsulamiento incorpora Frodo-640-AES, Frodo-640-SHAKE, Kyber512, NewHope-512-CCA, NTRU-HPS-2048-509 y Sike-p503.

\section{CRIPTOGRAFÍA POST-CUÁNTICA EN CURL}

Si bien ya se ha mencionado en la Sección \ref{section:curl_liboqs} una integración entre el cliente cURL y la biblioteca liboqs del proyecto OQS como parte de una serie de imágenes Docker provistas por OQS \citep{stebilaOqsdemos2022}, cabe mencionar también un trabajo del equipo de desarrollo de wolfSSL que tiene la intención de integrar su implementación de TLS con cURL y la incorporación de algoritmos KEM resistentes a ataques cuánticos \citep{wolfsslPostQuantumCURL}.

\section{INTEGRACIÓN WOLFSSL + LIBOQS}

Como se mencionó en la Sección \ref{section:wolfssl_pruebas}, desde la versión v5.0.0 de wolfSSL, liberada en Noviembre de 2021, el proyecto discontinuó su integración tanto con NTRU como con QSH (véase el ChangeLog de \citep{wolfsslinc.GitHubWolfSSLWolfssl2020}).

Desde entonces el equipo de wolfSSL ha continuado trabajando en soporte de algoritmos post-cuánticos en TLS 1.3, pero ahora utilizando como base liboqs, la biblioteca post-cuántica del proyecto OQS, haciendo uso de los algoritmos finalistas de la 3ra ronda de estandarización del NIST.

\section{CRIPTOGRAFÍA NEURONAL}

La criptografía neuronal \citep{kinzel2002neural} es una rama de la criptografía que se basa en el estudio de algoritmos estocásticos basados en redes neuronales artificiales para su uso en el cifrado y el criptoanálisis.

Las redes neuronales artificiales se identifican por su capacidad para explorar soluciones de manera selectiva para un problema determinado. Por esto es que tiene un amplio uso en el campo del criptoanálisis. Una clase particular de red neuronal utilizada en este ámbito es la Máquina de Paridad de Árbol (TPM, por sus siglas en inglés), un tipo especial de red neuronal multicapa de propagación hacia adelante (feed-forward) \citep{Michie1994MachineLN, kinzel2002neural}.

Los conceptos de aprendizaje mutuo, autoaprendizaje y comportamiento estocástico de las redes neuronales artificiales pueden usarse para diversos campos de la criptografía, como el cifrado de clave pública, las funciones hash, la generación pseudo-aleatoria de números, o la distribución de claves mediante el uso de la sincronización mutua de redes neuronales.

Este último punto es de especial importancia en el presente trabajo. Como se ha estudiado hasta ahora, la mayor parte de los mecanismos de intercambio de claves están basados en el protocolo Diffie-Hellman. El intercambio basado en la sincronización de redes TPM no se fundamenta en complejidad computacional, por lo que podría considerarse, en primera instancia, un protocolo potencialmente seguro contra criptoanálisis cuántico \citep{javurekUseArtificialNeural2013,stypinskiSynchronizationTreeParity2021}. Igualmente este protocolo tiene otras vulnerabilidades conocidas, explotables desde el criptoanálisis clásico.
