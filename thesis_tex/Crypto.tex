\cleardoublepage
\chapter{CRIPTOGRAFÍA APLICADA}
\label{section:criptografiaaplicada}

El término \textbf{criptografía} proviene del griego \textit{kryptós}, que significa \textit{oculto o secreto}, y \textit{graphé}, que significa \textit{grafo o escritura}: \textit{escritura oculta o secreta}. Es decir, es el estudio de las técnicas destinadas a asegurar las comunicaciones en presencia de terceros denominados adversarios o atacantes. La criptografía permite diseñar y analizar mecanismos que brinden la comunicación secreta a dos partes interesadas, utilizando un canal público considerado hostil.

Pueden distinguirse dos eras en la criptografía: la clásica y la moderna. La clásica hace referencia a las técnicas de transposición y sustitución de caracteres para ocultar mensajes. La criptografía moderna es la que surge con la era de la computación, y hace uso de conocimientos de álgebra, teoría de números, estadística, combinatoria, y hasta física cuántica para lograr su objetivo. En el presente trabajo resulta de interés ésta última.

Se define al criptoanálisis como la disciplina que se encarga de estudiar de los sistemas criptográficos con la intención de encontrar vulnerabilidades sin conocer ninguna información secreta. Ambos, criptografía y criptoanálisis, forman parte de una ciencia llamada criptología.

La seguridad de los algoritmos criptográficos modernos se basa en supuestos de complejidad computacional no demostrados, tales como operaciones matemáticas unidireccionales. De esta forma las claves secretas necesarias para el cifrado se generan mediante operaciones matemáticas muy simples de calcular pero extremadamente difíciles de revertir. Por ejemplo, una clave RSA se genera a partir de la multiplicación de dos números primos grandes. Esta operación es muy simple de realizar con cualquier computadora clásica. No obstante, teniendo el valor de este producto, un supuesto atacante debería invertir mucho tiempo de cómputo para encontrar los dos números primos que le dieron origen. Este tiempo resulta una limitante para el atacante.

Por otro lado, los avances tecnológicos han permitido un incremento en la potencia de cálculo con técnicas como el aumento de la frecuencia de procesamiento o el uso de paralelismo. Esto le brinda a la computadora maneras de ejecutar más operaciones simples por segundo, reduciendo el tiempo de ejecución que requiere un algoritmo. El incremento de potencia beneficia a aquellas aplicaciones que necesitan realizar cálculos complejos, como es el caso del criptoanálisis. En la medida en que los procesadores se hacen más rápidos, las tareas de criptoanálisis se vuelven más eficientes, y permiten romper algoritmos criptográficos simples o cuyas claves sean relativamente cortas. Por este motivo, el incremento de la potencia de procesamiento acompaña al desarrollo e implementación de algoritmos criptográficos más complejos y/o al incremento del tamaño de las claves de cifrado sin que esto ocasione en una pérdida de rendimiento.

\section{CRIPTOGRAFÍA SIMÉTRICA Y ASIMÉTRICA}

Para entender los conceptos subyacentes a la criptografía simétrica y asimétrica primero es necesario introducir las nociones de criptosistema. Un criptosistema es un conjunto de algoritmos necesarios para implementar servicios de seguridad tales como la confidencialidad o privacidad, la autenticidad y la integridad. El esquema general de un criptosistema puede apreciarse en la Fig. \ref{fig:cryptosystem}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.9\textwidth]{img/cryptosystem.jpeg}
	\caption{Criptosistema general.}
	\source{\citep{ramioaguirreCursoCriptografiaAplicada2018}}
	\label{fig:cryptosystem}
\end{figure}

Los componentes principales del mismo son:
\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Emisor: entidad encargada de emitir un mensaje en texto plano.
	\li Sistema de cifrado: algoritmo criptográfico que generará un mensaje cifrado a partir del mensaje original y una clave.
	\li Sistema de descifrado: algoritmo criptográfico que generará un mensaje en texto plano a partir del mensaje cifrado y una clave.
	\li Receptor: entidad destinataria del mensaje original.
	\li Mensaje: datos que el emisor envía al receptor. Según la etapa del criptosistema en la que se encuentre, puede tratarse de un mensaje en texto plano o un mensaje cifrado.
	\li Clave: clave o claves utilizadas para cifrar y/o descifrar mensajes.
\end{itemizenospace}

Ahora bien, en un criptosistema puede que el algoritmo de cifrado y el de descifrado sean el mismo o no, y a su vez puede ocurrir también que las claves de cifrado y de descifrado coincidan o sean diferentes.

Cuando la claves de cifrado y de descifrado es la misma se dice que el criptosistema es simétrico o de clave secreta, y cuando la clave usada para cifrar el mensaje difiere de la que se usa para descifrarlo, se dice que el criptosistema es asimétrico o de clave pública.

\subsection{Criptografía simétrica}

Los algoritmos criptográficos simétricos a su vez pueden ser de flujo o de bloques, dependiendo de si el procedimiento de cifrado y de descifrado se lleva a cabo un caracter a la vez o en bloques de información de tamaño fijo. Un cifrador que genere un texto cifrado procesando muy pequeñas porciones del texto original, por ejemplo, un caracter a la vez, es un cifrador de flujo o \textit{stream}. Un algoritmo simétrico de flujo común es RC4. Por su parte, si el cifrado y descifrado se lleva a cabo por bloques de tamaño fijo, por ejemplo 128 bits, se trata de un cifrador simétrico de bloques. Uno de los algoritmos más utilizados en la actualidad en esta categoría es AES.

Los algoritmos simétricos de bloques a su vez pueden funcionar utilizando diferentes esquemas de encadenamiento de bloques para lograr el cifrado del conjunto completo de datos. Esos esquemas se denominan \textit{modos de operación}. Si bien los detalles de cada uno de los modos de operación de cifradores simétricos exceden los alcances del presente trabajo, se ilustrará uno de los más comunes a modo de facilitar el concepto.

Los cifradores de bloques dividen el texto plano en bloques de igual tamaño, y encriptan cada uno para obtener un bloque cifrado. El algoritmo puede encadenar el cifrado de estos bloques de texto plano de modo que, por ejemplo, el texto cifrado de un bloque se use en el proceso de cifrado del bloque siguiente, mejorando la privacidad de los datos cifrados.

El modo de operación del cifrador define la forma en la que se encadenan estos bloques durante el proceso de cifrado. Un ejemplo común es el modo de Encadenamiento de bloques cifrados (CBS por sus siglas en inglés). La Fig. \ref{fig:cbc} ilustra el proceso de cifrado. Los mensajes de texto plano se identifican con la letra \verb|M|, y los cifrados con la letra \verb|C|. \verb|E| y \verb|D| representan, respectivamente, algoritmo de encriptación y desencriptación.

El bloque cifrado inicial, \verb|C1|, se genera realizando una operación \verb|XOR| entre el primer bloque de texto plano, \verb|M1|, con un bloque aleatorio inicial denominado \verb|IV| (Initialization Vector - Vector de inicialización). Al resultado de dicha operación se lo encripta utilizando el algoritmo \verb|E| y la clave, generando el primer bloque cifrado, \verb|C1|. Este bloque se utiliza como entrada al \verb|XOR| correspondiente al segundo bloque de texto plano \verb|M2|. 

El proceso de desencriptación realiza el proceso inverso. Primero se desencripta el bloque \verb|C1|, y el resultado se opera mediante \verb|XOR| con el mismo \verb|IV| generado para encriptar, obteniendo el bloque \verb|M1|. El proceso se repite para los siguientes bloques cifrados, utilizando como entrada de cada \verb|XOR| el bloque cifrado anterior.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/cbc.jpg}
	\caption{Modo CBC de cifrado simétrico de flujo.}
	\source{\citep{tanenbaumComputerNetworks2010}.}
	\label{fig:cbc}
\end{figure}

El resto de los modos de operación cambian la manera en la que se encadenan los diferentes mecanismos de encriptación/desencriptación de bloques. Si bien no se realizará una explicación detallada de cada modo de operación, es importante mencionarlos para comprender la conformación de las cipher suites de SSL/TLS más adelante. Los modos más comunes son:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li ECB: Electronic Code Book
	\li CBC: Cipher Block Chaining
	\li PCBC: Propagating CBC
	\li CFB: Cihper FeedBack
	\li OFB: Output FeedBack
	\li CTR: Counter
	\li GCM: Galois Counter Mode
\end{itemizenospace}

Uno de los principales inconvenientes de los criptosistemas simétricos es el intercambio de la clave secreta entre las partes involucradas sin que ningún adversario pueda interceptarla. Un protocolo para intercambio de claves muy utilizado en la actualidad es Diffie-Hellman, mecanismo que sentó las bases de la criptografía asimétrica (véase la Sección \ref{subsection:intercambio}).

\subsection{Criptografía asimétrica}

Los algoritmos asimétricos hacen uso de dos claves: una pública y una privada. Esto tiene una ventaja sobre los simétricos: sólo requiere que las partes involucradas intercambien sus claves públicas. Las claves públicas suelen distribuirse en un formato encapsulado denominado certificado digital. El estándar más utilizado en este caso es el x509 \citep{itu-t509InformationTechnology2019}. Este certificado incorpora, además de una clave pública, datos del propietario de dicha clave, información de validez temporal y firma digital emitida por alguna autoridad certificante. El conjunto de claves privadas, certificados digitales x509 y entidades de firma de dichos certificados da origen a lo que se conoce como Infraestructura de Clave Pública, o PKI (Public Key Infrastructure, Infraestructura de Clave Pública). Una desventaja de los algoritmos asimétricos respecto de los simétricos radica en los cálculos que realizan: las tareas de cifrado y descifrado asimétricas requieren más operaciones matemáticas, por lo que pueden perjudicar el rendimiento de los sistemas donde se implementen.

\section{CRIPTOGRAFÍA Y SEGURIDAD INFORMÁTICA}

La criptografía representa la base de las técnicas de seguridad informática en Internet. Tareas como la navegación en sitios web, la autenticación de usuario en el home banking, o en una API que accede a servicios en los que prima la privacidad, requieren de mecanismos criptográficos que garanticen la seguridad en la comunicación. Éstos requisitos deben proveerse en un amplio abanico de protocolos de red tales como HTTP, protocolos de transferencia de archivos, o de comunicaciones en túnel como el caso de las VPN (Virtual Private Network, Redes Privadas Virtuales).

Ahora bien: ?`cómo se logra la seguridad en este contexto? Para que una comunicación en Internet sea segura deben verificarse tres requisitos: privacidad, autenticidad, e integridad de los mensajes intercambiados. El receptor de los mensajes es quien verifica estos requisitos, y puede hacerlo utilizando criptografía simétrica, asimétrica, o una combinación de ambas.

\subsection{Privacidad o confidencialidad}

Este punto se refiere a implementar mecanismos criptográficos que cifren los mensajes enviados de tal modo que únicamente pueda descifrarlos el destinatario de los mismos. Así, si existiera un atacante en el canal, éste no podrá ser capaz interpretar el contenido de los mismos. La privacidad puede lograrse mediante criptografía simétrica o asimétrica.

En el caso de usar criptografía simétrica ambos nodos deberán compartir previamente una clave secreta. Esta clave permitirá a un extremo cifrar un mensaje haciendo uso de un algoritmo de cifrado simétrico, y el otro extremo podrá descifrarlo utilizando la misma clave y el algoritmo correspondiente. Un atacante no podrá acceder al contenido ya que no poseerá la clave secreta compartida por emisor y receptor. Es importante evitar que la clave secreta se vea comprometida en el medio.

Usando criptografía asimétrica ambos extremos deberán tener un juego de claves pública y privada. Un mensaje cifrado con una de las claves podrá ser descifrado únicamente con la otra clave del par. Así, el emisor podrá encriptar un mensaje usando la clave pública del receptor, y éste podrá desencriptarlo utilizando su clave privada, par de la pública usada por el emisor. Un atacante no podría descifrar el mensaje ya que no tiene acceso a la clave privada del receptor.

\subsection{Autenticación, Integridad y No Repudio}

La autenticidad y la integridad pueden verificarse usando los mismos mecanismos, ya sea aplicando criptografía simétrica o asimétrica. En ambos casos se hacen uso de las funciones resumen, o hash \citep{lucenalopezCriptografiaSeguridadComputadores2011}.

La autenticidad del mensaje le permite al receptor verificar que el mismo haya sido generado por quien dice ser, y que no se trata de un mensaje enviado por un tercero. . Por su parte, la integridad permite verificar que los mensajes recibidos no hayan sido adulterados desde el momento en que se enviaron, independientemente de si la modificación en los mensajes se debió a errores propios del canal de comunicación, o a cambios intencionales introducidos por un atacante.

En criptografía simétrica la autenticidad se logra mediante un HMAC (Hash-based Message Authentication Code, Código de Autenticación Basado en Hash). El HMAC es el resultado de una función hash aplicada al mensaje original combinado con una clave simétrica de autenticación. En otras palabras, se toma el mensaje original, se le combina una clave simétrica, y sobre el conjunto se calcula una función hash. Luego el emisor envía el mensaje original y el hash calculado. El receptor deberá contar con la misma clave simétrica para verificar el hash del mensaje recibido. El receptor, al poder verificar el HMAC, podrá corroborar que el emisor es quien dice ser, y también estará constatando que el mensaje no ha sido modificado desde que se emitió, por lo que la integridad también quedará validada.

En criptografía asimétrica la autenticación de los mensajes se lleva a cabo mediante firmas digitales. La firma digital se calcula cifrando, con la clave privada del emisor, un hash calculado sobre el mensaje. El destinatario podría verificar la firma descifrando el hash con la clave pública del emisor, y comparando el hash obtenido con el que puede calcular del mensaje recibido. Al igual que en el caso de la autenticación por HMAC, el receptor que pueda verificar una firma digital de un mensaje estará constatando también que el mismo no haya sido adulterado durante la transmisión, por lo que validará también su integridad.

Un servicio relacionado con la autenticidad es el de no repudio. Si el destinatario puede verificar la autenticidad de un mensaje recibido, implica que el emisor del mismo no puede negar su envío.

\subsection{Intercambio de claves}
\label{subsection:intercambio}

Cuando la seguridad de las comunicaciones en red se basa en claves simétricas de sesión, ambos nodos deben inicialmente compartir dicha clave a través de algún mecanismo que le impida a un atacante interceptar la conversación. Los nodos utilizarán dicha clave durante un tiempo configurado, y luego la descartarán y negociarán una nueva clave para la siguiente sesión. Uno de los métodos más utilizados para lograr este cometido es el protocolo de Diffie-Hellman (DH) \citep{rescorlaRFC2631DiffieHellman1999}. Este método permite a dos partes establecer una clave secreta compartida sobre un canal inseguro. Esta clave permitirá el cifrado y autenticación simétrica de los siguientes mensajes enviados entre los nodos. DH se usa en gran variedad de mecanismos de seguridad, entre ellos SSL/TLS, objeto de estudio de esta tesis.

Además, las claves de sesión generadas por DH son totalmente independientes unas de otras, por lo que un atacante que logre comprometer una de ellas no podrá usar dicha información para obtener la de la siguiente sesión. Esta característica se denomina PFS (Perfect Forward Secrecy - Secreto Perfecto Hacia Adelante) \citep{menezes_handbook_2018}.

DH permite a dos extremos de una comunicación obtener un número secreto común, que será utilizado como clave, sin ningún intercambio previo. Supóngase que dos extremos de una comunicación, llamados Alice y Bob, necesitan encontrar una clave simétrica común teniendo en cuenta que un atacante, Eva, espía el canal público. Para ello, y como se ve en la Fig. \ref{fig:diffie-hellman}:
\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li Alice y Bob eligen un número privado cada uno: $a$ y $b$ respectivamente.
	\li Alice selecciona dos números públicos, $g$ y $p$, primos.
	\li Alice calcula un nuevo valor público: $ A = g^a\ mod\ p $.
	\li Alice envía a Bob los valores $ A $, $ g $ y $ p $.
	\li Bob con esos datos puede calcular otro número público: $ B = g^b\ mod\ p $.
	\li Bob envia a Alice su resultado $ B $.
	\li Ambos nodos ahora pueden calcular la clave secreta $ K $:
	\begin{itemizenospace}
		\setlength\itemsep{0em}
		\li Alice calcula la clave con: $ K = B^a\ mod\ p $
		\li Bob calcula la clave con  : $ K = A^b\ mod\ p $
	\end{itemizenospace}
\end{enumeratenospace}

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.7\textwidth]{img/diffie-hellman.jpeg}
	\caption{Intercambio Diffie-Hellman.}
	\source{\citep{lucenalopezCriptografiaSeguridadComputadores2011}}
	\label{fig:diffie-hellman}
\end{figure}

De esta forma un atacante que haya estado observando el canal público desde el inicio de la comunicación conocerá los valores $g$, $p$ $A$ y $B$, y aún así no podrá calcular $K$ en un tiempo relativamente corto. Aquí:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li $g$ se denomina base pública, es conocida por Alice, Bob y Eva.
	\li $p$ se denomina módulo, es conocido por Alice, Bob y Eva.
	\li $a$ es la clave privada de Alice, conocida únicamente por Alice.
	\li $b$ es la clave privada de Bob, conocida únicamente por Bob.
	\li $A$ es la clave pública de Alice, conocida por Alice, Bob y Eva.
	\li $B$ es la clave pública de Bob, conocida por Alice, Bob y Eva.
\end{itemizenospace}

Existen algunas variantes de DH como DH de curva elíptica (ECDH, por sus siglas en inglés), que combina el protocolo original con Criptografía de curva elíptica (ECC, por sus siglas en inglés) \citep{koblitz1987elliptic}, una variante de la criptografía asimétrica que hace uso de matemáticas de curva elíptica para proveer a los algoritmos una manera de reducir el tamaño de las claves, y con ello tambén el tiempo de procesamiento, sin perder el nivel de seguridad provisto por el protocolo original.

\subsection{KEM: Key Encapsulation Mechanism}

Este apartado introduce los conceptos básicos de mecanismos de encapsulamiento de claves dado que su entendimiento es necesario para comprender el lugar que ocupan los algoritmos post-cuánticos en algunas implementaciones analizadas en la parte DESARROLLO de esta tesis.

Los cifradores asimétricos como RSA suelen ser lentos y consumir más recursos que alternativas simétricas como AES \citep{donta2007performance}, y es por esta razón que no suelen utilizarse para encriptar contenido de tráfico de red, como es el caso de TLS. Los cifradores asimétrico en TLS se utilizan para proteger el intercambio de claves simétricas que luego se usarán para encriptar el contenido del tráfico. Algoritmos como RSA encriptan bloques de tamaño equivalente al de la clave utilizada, es decir, si se usa RSA con claves de 2048 bits, los bloques de texto plano a cifrar serán de ese tamaño, 256 bytes. Ahora bien, el tamaño de clave más común en AES es de 256 bits (32 bytes), por lo que para encriptar una clave AES se requiere incorporar un relleno, o \textit{padding}.

Hacer uso de rellenos en mecanismos criptográficos se considera inseguro ya que incrementa los vectores de ataque sobre la protección criptográfica \citep{bleichenbacherChosenCiphertextAttacks1998,217494,sullivanPaddingOraclesDecline2016,ronen20199,mangerChosenCiphertextAttack2001}. Para evitar el relleno generalmente se hace uso de mecanismos de encapsulamiento de claves (KEM, por sus siglas en inglés). Estos mecanismos facilitan la generación de claves simétricas aleatorias a partir de un sistema de clave pública y funciones hash.

En lugar de generar una clave secreta simétrica y luego encriptarla con un algoritmo asimétrico añadiendo el relleno, se define una API a la que se llama utilizando la clave pública del destinatario. La API retorna una clave simétrica AES encriptada lista para utilizar. Suponiendo que Alice es el emisor y Bob el receptor, quien dispone de su clave pública, la secuencia para enviar un mensaje cifrado utilizando un mecanismo KEM es la siguiente:

\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\item Bob le envía su clave pública RSA a Alice. Dicha clave pública incluye un módulo propio del cifrador RSA.
	\item Alice genera un número aleatorio entre 2 y el módulo de la clave pública de Bob menos uno.
	\item Alice encripta dicho número con la clave pública de Bob. Este número aleatorio tiene el mismo tamaño que el módulo de la clave pública de Bob, por lo que no se añade relleno. A este valor cifrado se lo denomina clave encapsulada.
	\item Alice genera una clave AES calculando un hash del número aleatorio.
	\item Alice encripta el mensaje utilizando dicha clave AES.
	\item Alice envía a Bob el mensaje cifrado y la clave encapsulada cifrada.
	\item Bob desencripta la clave encapsulada utilizando su clave privada, y obtiene el número aleatorio generado por Alice.
	\item Bob calcula el mismo hash que calculó Alice sobre este número aleatorio, obteniendo la clave AES.
	\item Bob desencripta el mensaje utilizando esta clave AES.
\end{enumeratenospace}

El Alg. \ref{alg:kem_pseudocode}, adaptado de \citep{maddenHybridEncryptionKEM2021}, muestra un ejemplo de las funciones necesarias para lograr el cifrado basado en KEM. Aquí:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li \verb|rsa_encapsulate|: genera una clave AES de único uso y la retorna en texto plano y cifrada con una clave pública RSA.
	\li \verb|rsa_decapsulate|: retorna la clave AES de único uso en texto plano descifrándola  con la clave privada RSA.
	\li \verb|encrypt|: encripta un mensaje usando la clave AES de único uso.
	\li \verb|decrypt|: descifra un mensaje cifrado con la clave AES de único uso.
\end{itemizenospace}


%\verb|rsa_encapsulate| recibe por argumento la clave pública del destinatario (\verb|publicKey|), y retorna una clave AES de un único uso en texto plano (\verb|aesKey|) y cifrada usando dicha clave pública (\verb|encryptedKey|). \verb|rsa_decapsulate| recibe la clave privada del destinatario (\verb|privateKey|), y la clave AES encriptada, y retorna la clave simétrica AES. Las funciones \verb|encrypt| y \verb|decrypt| hacen uso del encapsulamiento para cifrar y descifrar un mensaje respectivamente.

Este esquema permite la transmisión de mensajes cifrados utilizando un algoritmo simétrico como AES, cuya clave se comparte de manera segura mediante cifrado asimétrico (RSA en este ejemplo). Además el mecanismo permite cifrar cada bloque utilizando claves aleatorias únicas.

Otra ventaja que brinda un esquema KEM es la posibilidad de utilizar otros mecanismos asimétricos para realizar el encapsulamiento, no solamente RSA. \citep{maddenHybridEncryptionKEM2021} menciona un ejemplo basado en ECDH.


\begin{lstlisting}[
	language={Python},
	caption={Ejemplo de funciones necesarias para KEM.},
	label={alg:kem_pseudocode},
	%firstnumber=1,
	numbers=none,
	]
def rsa_encapsulate(publicKey):
	# random number in range(2,publicKey.modulus-1)
	random = generate_secure_random(publicKey);
	encryptedKey = rsa_encrypt(random, publicKey);
	aesKey = sha256(random);
	return (aesKey, encryptedKey)

def rsa_decapsulate(encryptedKey, privateKey):
	random = rsa_decrypt(encryptedKey, privateKey);
	return sha256(random)

def encrypt(message, publicKey):
	(aesKey, encryptedKey) = rsa_encapsulate(publicKey);
	ciphertext = aes_encrypt(aesKey, message);
	return (encryptedKey, ciphertext)

def decrypt(ciphertext, encryptedKey, privateKey):
	aesKey = rsa_decapsulate(encryptedKey, privateKey);
	message = aes_decrypt(aesKey, ciphertext);
	return message;
\end{lstlisting}
