%\cleardoublepage
\section{OQS-OPENSSL v1.1.1 Y HTTPS (APACHE)}
\label{section:oqs111_https_apache}

El proyecto OQS provee una serie de imágenes Docker \citep{dockerinc.EmpoweringAppDevelopment2013} con versiones pre-compiladas de algunos servicios post-cuánticos \citep{stebilaOqsdemos2022}. Las imágenes Docker permiten crear entornos virtuales aislados, denominados contenedores. Estos contenedores le ofrecen al usuario entornos repetibles de construcción y prueba de aplicaciones. Particularmente en esta tesis los contenedores Docker facilitaron la realización de los experimentos con las implementaciones tal y como los desarrolladores las ofrecían, sin necesidad de compilaciones y configuraciones que dependan de detalles propios de cada sistema operativo.

Los experimentos llevados a cabo en esta sección son los siguientes:

\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li Construcción el contenedor Docker de Apache.
	\li Análisis de sus configuraciones predeterminadas, y adaptaciones.
	\li Análisis de conexión con parámetros predeterminados.
	\li Extracción y análisis de claves y certificados digitales del contenedor.
	\li Análisis del binario de OpenSSL integrado en la imagen Docker.
	\li Incorporación de algoritmos KEM puros e híbridos en TLS.
	\li Incorporación de un cliente cURL post-cuántico.
	\li Análisis de conexión TLS post-cuántica (autenticación y KEM).
\end{enumeratenospace}

\subsubsection*{Contenedor Docker de Apache.}

Una de las demos Docker provistas por OQS corresponde con una implementación de Apache integrado con OQS-OpenSSL v1.1.1. Uno de los parámetros importantes de la imagen Docker es el algoritmo de firma y autenticación utilizado de manera predeterminada a la hora de crear el contenedor. Puede cambiarse el algoritmo de autenticación predeterminado utilizando el modificador \verb|--build-alg SIG_ALG=<SIG>| en Docker, donde \verb|<SIG>| representa un algoritmo válido (véase Sección \ref{section:oqsopenssl111_pubkey}). El Alg. \ref{alg:oqsopenssl111apache_clonado} muestra el comando necesario para clonar el repositorio y crear el contenedor, ya sea utilizando el algoritmo de firma predeterminado como haciendo uso de \verb|--build-alg|. Se muestra como último comando el necesario para ejecutar el contenedor previamente creado.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Ejecución del contenedor.},
	label={alg:oqsopenssl111apache_clonado},
	%firstnumber=1,
	numbers=none,
	]
# Clonado del repositorio oficial (recomendado):
mkdir /opt/oqs-demos && cd /opt/oqs-demos/
git clone \
	https://github.com/open-quantum-safe/oqs-demos.git

# Clonado del repositorio (fork modificado):
mkdir /opt/oqs-demos && cd /opt/oqs-demos/
git clone git@github.com:d1cor/oqs-demos.git
# Cambio de branch:
cd oqs-demos/
git checkout httpd-dilithium3

# Creación del contenedor usando SIG_ALG predeterminado:
docker build -t openquantumsafe/httpd .

# Creación del contenedor cambiando el algoritmo de firma:
docker build --build-arg SIG_ALG=<SIG> \
	-t openquantumsafe/httpd .

# Ejecución del contenedor creado: (TCP 4433)
docker run -p 4433:4433 openquantumsafe/httpd
\end{lstlisting}

Según la documentación esta demo hace uso de Dilithium3 como algoritmo de autenticación predeterminado, sin embargo el experimento inicial dio cuenta que el algoritmo utilizado para la creación de las claves fue Dilithium2. Esta inconsistencia se debía a que el \verb|Dockerfile| redefinía por error la variable \verb|SIG_ALG|, variable que permite configurar el algoritmo asimétrico predeterminado al momento de crear el contenedor. Por este motivo se realizó un fork del repositorio oficial, y se corrigió y reportó el error. El Alg. \ref{alg:oqsopenssl111apache_clonado} muestra el clonado del repositorio oficial para reproducir los experimentos, y el clonado del repositorio modificado a modo de información. Para reproducir los experimentos es recomendable utilizar el repositorio oficial ya que el cambio reportado fue integrado a la rama principal del proyecto.

\begin{comment}
	Se pudo verificar que el puerto se encontraba atendiendo conexiones sin inconvenientes con el comando \verb|ss| del sistema operativo (Alg. \ref{alg:oqsopenssl111apache_ss}).

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSL v1.1.1 con Apache - Verificación de servicio.},
	label={alg:oqsopenssl111apache_ss},
	%firstnumber=1,
	numbers=none,
	]
# ss -nplt|grep 4433 | awk '{print $4"\t"$6}'
*:4433  users:(("docker-proxy",pid=7998,fd=4))
\end{lstlisting}
\end{comment}

\subsubsection*{Conexión del cliente OpenSSL.}

Las pruebas de conexión se realizaron con la v1.1.1 de OQS-OpenSSL previamente compilada en la Sección \ref{section:oqs111_prueba}. Para realizar el siguiente experimento se cargó el módulo de entorno correspondiente, y lanzó el cliente compilado previamente, tal y como se muestra en el Alg. \ref{alg:oqsopenssl111apache_execclient}.

\begin{comment}
	agregar el uso del comando openssl ecparam -list_curves (o groups) para listar las curvas o algoritmos KEX
\end{comment}

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Ejecución del cliente.},
	label={alg:oqsopenssl111apache_execclient},
	%firstnumber=1,
	numbers=none,
	]
module load openssl/1.1.1
openssl s_client -connect localhost:4433
\end{lstlisting}

El resultado de la ejecución puede verse en la Fig. \ref{fig:openssl111_oqsmaster_https_cliserv}. Aquí se muestra parte de la salida del comando, donde se ve que se estableció la conexión satisfactoriamente, y marca el uso de Dilithium3 como algoritmo de autenticación y firma digital, el algoritmo que se compiló de manera predeterminada al crear la imagen Docker. Esto implica que ambas implementaciones soportan el algoritmo Dilithium3. Como dato adicional, dicha figura muestra el uso del algoritmo tradicional KEM ECDH con curva Curve25519.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_https_cliserv.png}
	\caption{Conexión OQS-OpenSSL v1.1.1 predeterminada - httpd.}
	\label{fig:openssl111_oqsmaster_https_cliserv}
\end{figure}

La misma Fig. \ref{fig:openssl111_oqsmaster_https_cliserv} muetra el error de verificación de certificado digital del servidor. Esto se debe a que el cliente no dispone de la clave pública utilizada para firmar el certificado recibido desde el servidor Apache.

\subsubsection*{Verificación de certificado digital.}
\label{section:verificacioncacrt}

El certificado digital de la CA se encuentra en el directorio \verb|cacert/| y se denomina \verb|CA.crt| \citep{stebilaOqsdemos2022}. Dicho certificado se extrajo utilizando el comando \verb|cat| dentro del contenedor tal y como se muestra en el Alg. \ref{alg:oqsopenssl111apache_certextract}. Para mantener el orden y consistencia con el resto de los experimentos, se creó un directorio particular dentro de \verb|KEYSDIR| llamado \verb|oqs_httpd/|, y dentro del mismo se extrajo el certificado de la CA y su clave privada por si fuera a ser necesaria en futuros experimentos. Se cargó el módulo de entorno para poder acceder al contenido de la variable \verb|KEYSDIR|.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Extracción del certificado.},
	label={alg:oqsopenssl111apache_certextract},
	%firstnumber=1,
	numbers=none,
	]
module load openssl/1.1.1
mkdir $KEYSDIR/oqs_httpd/

#Extracción de certificado digital de la CA:
docker run -p 4433:4433 openquantumsafe/httpd \
	cat cacert/CA.crt > $KEYSDIR/oqs_httpd/CA.crt

#Extracción de clave privada de la CA:
docker run -p 4433:4433 openquantumsafe/httpd \
	cat cacert/CA.key > $KEYSDIR/oqs_httpd/CA.key
\end{lstlisting}

El Alg. \ref{alg:oqsopenssl111apache_certdatacmd} muestra el comando utilizado para extraer la información del certificado, y un fragmento de interés de su salida (la salida completa se encuentra en el repositorio Git de la tesis \citep{cordobaTesisMTIDiego2022}). Se utilizó la v1.1.1 de OQS-OpenSSL, previamente compilada en este trabajo para extraer dicha información. El certificado digital y su firma son de tipo Dilithium3, algoritmo asimétrico utilizado por defecto esta imagen de Docker.

El comando utilizado para conectar el cliente y verificar dicha firma digital se ve al final del mismo Alg. \ref{alg:oqsopenssl111apache_certdatacmd}. La Fig. \ref{fig:openssl111_oqsmaster_https_cliservverify} muestra el resultado de la negociación, y puede apreciarse que el certificado ahora pasa el proceso de verificación.
\clearpage
\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Extracción de datos.},
	label={alg:oqsopenssl111apache_certdatacmd},
	%firstnumber=1,
	numbers=none,
	]
# Extracción de datos:
module load openssl/1.1.1
openssl x509 -in $KEYSDIR/oqs_httpd/CA.crt -noout -text

# Salida (resumida para resaltar información de interés):
Certificate:
	Data:
		Version: 3 (0x2)
		Serial Number: [...]
		Signature Algorithm: dilithium3
		Issuer: CN = oqstest CA
		Validity
            Not Before: Mar 11 14:38:48 2022 GMT
			Not After : Mar 11 14:38:48 2023 GMT
		Subject: CN = oqstest CA
		Subject Public Key Info:
		Public Key Algorithm: dilithium3
			dilithium3 Public-Key:
			pub: [...]
	Signature Algorithm: dilithium3
		[...]

# Verificación de datos del certificado:
openssl s_client -connect localhost:4433 \
	-CAfile $KEYSDIR/oqs_httpd/CA.crt
\end{lstlisting}

\begin{comment}
	\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Verificación de certificado.},
	label={alg:oqsopenssl111apache_execverif},
	%firstnumber=1,
	numbers=none,
	]
module load openssl/1.1.1
openssl s_client -connect localhost:4433 \
	-CAfile $KEYSDIR/oqs_httpd/CA.crt
\end{lstlisting}

\end{comment}
\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_https_cliservverify.png}
	\caption{OQS-OpenSSL v1.1.1 + Apache. Verificación OK.}
	\label{fig:openssl111_oqsmaster_https_cliservverify}
\end{figure}

Este experimento da cuenta de que ambas implementaciones de OpenSSL, la integrada en la imagen Docker de Apache y la compilada para el presente trabajo, soportan algoritmos de clave pública post-cuánticos como Dilithium3. Se agrega como dato adicional que ambas implementaciones de OpenSSL, si bien comparten la versión, v1.1.1, difieren en la letra de patch: \verb|l| para la release integrada en imagen Docker, y \verb|m| para la compilada para esta tesis. Así, ambas releases ofrecen las mismas características, pero la compilada para el presente trabajo hace correcciones menores respecto de la incluída en la imagen Docker. Para determinar la versión y release del OpenSSL compilado para esta tesis se utilizó el comando \verb|version|. El contenedor Docker no incluye los binarios de OpenSSL, de modo que se extrajo la información analizando el binario de la librería \verb|libssl.so| integrada en el servicio httpd. Ambos procedimientos pueden verse en el Alg. \ref{alg:oqsopenssl111_versionvsdocker}.

\begin{lstlisting}[
	language={Bash},
	caption={Versiones de OQS-OpenSSL utilizadas.},
	label={alg:oqsopenssl111_versionvsdocker},
	%firstnumber=1,
	%numbers=none,
	]
# Versión y release de OQS-OpenSSL incluída en Docker
docker run -p 4433:4433 openquantumsafe/httpd strings /usr/lib/libssl.so.1.1| grep "OpenSSL"
> OpenSSL 1.1.1l  24 Aug 2021

# Versión y release de OQS-OpenSSL compilada manualmente
module load openssl/1.1.1 && openssl version
> OpenSSL 1.1.1m  14 Dec 2021, Open Quantum Safe 2022-01
\end{lstlisting}

Como paso adicional se analizó el certificado del servidor Apache almacenado en el contenedor Docker. El algoritmo utilizado para su generación y firma es Dilithium2, tal y como muestra el Alg. \ref{alg:oqsopenssl111apache_certdataserver} (la salida completa se encuentra en el repositorio Git que acompaña a la tesis \citep{cordobaTesisMTIDiego2022}).

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Certificado servidor.},
	label={alg:oqsopenssl111apache_certdataserver},
	%firstnumber=1,
	numbers=none,
	]
# Extracción de certificado digital del servidor:
docker run -p 4433:4433 openquantumsafe/httpd \
	cat /opt/httpd/pki/server.key \
	> $KEYSDIR/oqs_httpd/server.crt

# Extracción de datos:
openssl x509 -in $KEYSDIR/oqs_httpd/server.crt \
	-noout -text
	
# Salida (resumida para resaltar información de interés):
Certificate: [...]
	Signature Algorithm: dilithium2
	Issuer: CN = oqstest CA
	Validity
		Not Before: Mar  9 14:52:06 2022 GMT
		Not After : Mar  9 14:52:06 2023 GMT
		Public Key Algorithm: dilithium2
		dilithium2 Public-Key: [...]
\end{lstlisting}

\subsubsection*{Intercambio de claves post-cuántico.}

El siguiente experimento realizó la incorporación de algoritmos de encapsulamiento de claves KEM. Para ello se llevó a cabo la prueba de conexión utilizando uno de los algoritmos KEM post-cuánticos soportados por OQS-OpenSSL v1.1.1: Frodo640AES. El modificador \verb|-curves| al ejecutar el cliente permite indicar un algoritmo de intercambio de claves (Alg. \ref{alg:oqsopenssl111_clientfrodo}).

\begin{lstlisting}[
	language={},
	caption={OQS-OpenSSLv1.1.1. Uso de Frodo640AES.},
	label={alg:oqsopenssl111_clientfrodo},
	%firstnumber=1,
	%numbers=none,
	]
module load openssl/1.1.1
openssl s_client -CAfile $KEYSDIR/oqs_httpd/CA.crt \
	-curves frodo640aes -crlf -connect localhost:4433
\end{lstlisting}

El resultado de la negociación puede verse en la Fig. \ref{fig:openssl111_oqsmaster_https_cliservfrodo} e indica que ambas implementaciones pudieron interpretar dicho algoritmo. Esta salida difiere de la salida mostrada en la Fig. \ref{fig:openssl111_oqsmaster_https_cliserv}, en la que se utilizó, de manera predeterminada, el algoritmo de curva elíptica X25519.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_https_cliservfrodo.png}
	\caption{OQS-OpenSSL v1.1.1 + Apache. Algoritmo KEM.}
	\label{fig:openssl111_oqsmaster_https_cliservfrodo}
\end{figure}

Frodo640AES es un algoritmo puramente post-cuántico, como se mencionó en la Sección \ref{section:algs111kem}. En dicha sección también se comentó la integración de algoritmos híbridos junto con los post-cuánticos en el intercambio de claves. Frodo640AES es un algoritmo de nivel de seguridad L1 del NIST \citep{openquantumsafeLiboqs2020} (véase la Sección \ref{section:estandarizacion}), por lo que estas implementaciones (servidor y cliente) soportan también el algoritmo híbrido \verb|p256_frodo640aes|, como puede apreciarse en la Fig. \ref{fig:openssl111_oqsmaster_https_cliservfrodohybrid}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_https_cliservfrodohybrid.png}
	\caption{OQS-OpenSSL v1.1.1 + Apache. Algoritmo KEM híbrido.}
	\label{fig:openssl111_oqsmaster_https_cliservfrodohybrid}
\end{figure}

\subsection{Cliente cURL y liboqs}
\label{section:curl_liboqs}

Durante la realización de esta tesis el proyecto OQS publicó una versión de cURL integrada con liboqs. Esta implementación no estaba disponible inicialmente pero resulta de interés comentar un experimento realizado: conectar un contenedor Apache con uno cURL usando cifrado post-cuántico.

Para comunicar el contenedor de \verb|oqs-httpd| y el de \verb|curl| se decidió crear una red Docker. Si bien existen otras alternativas, tales como el uso de la red host provista por Docker, el \textit{mapeo} de puertos del contenedor en puertos del host, o el uso de enlaces (\verb|link|) para que los contenedores puedan ubicarse por nombre, se ha decidió por el uso de una red Docker. Esta red permite comunicar contenedores de manera aislada, sin que se genere tráfico fuera de dicha red virtual, ya sea con otros contenedores externos, o con el propio host que los alberga. La red virtual se creó con el nombre \verb|httpd-test| y luego se lanzó el contenedor Apache, tal y como se ve en el Alg. \ref{alg:oqsopenssl111apache_dockernetapache}. Este algoritmo muestra también el comando docker que permitió ejecutar el cliente cURL y conectar con el servidor Apache.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Red virtual Docker Apache.},
	label={alg:oqsopenssl111apache_dockernetapache},
	%firstnumber=1,
	numbers=none,
	]
# Creación de la red docker httpd-test:
docker network create httpd-test

# Ejecución del contenedor Apache:
docker run --network httpd-test --name oqs-httpd -p 4433:4433 openquantumsafe/httpd

# Ejecución del contenedor cliente cURL (otra terminal):
docker run --network httpd-test -it \
	openquantumsafe/curl curl -k https://oqs-httpd:4433 \
	--curves frodo976aes
\end{lstlisting}

La Fig. \ref{fig:red_docker_httpd-test} muestra un esquema de conectividad de la red virtual Docker. Se aprecian aquí los dos contenedores en ejecución, por un lado el servidor Apache (oqs-httpd), y por otro, el contenedor curl. Ambos contenedores y la red virtual fueron definidos dentro de la máquina virtual Lubuntu utilizada para las pruebas.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/red_docker_httpd-test.jpg}
	\caption{Esquema de la red virtual Docker httpd-test.}
	\label{fig:red_docker_httpd-test}
\end{figure}

En este caso particular se utilizó un intercambio de claves Frodo640-AES, pero podría omitirse el argumento \verb|--curves| y para utilizar el algoritmo predeterminado \verb|oqs_kem_default| explicado en la Sección \ref{section:algs111kem}.

La negociación TLS requiere que el cliente cURL utilice por defecto el certificado x509 de la CA para verificar la firma del certificado del servidor. Aquí existen dos alternativas: omitir dicha verificación realizando la prueba de conexión con modificador \verb|-k| (o \verb|--insecure|) en \verb|curl|, o descargar el certificado de la CA de Apache y utilizarlo. Dado que en la Sección \ref{section:verificacioncacrt} se extrajo dicho certificado, se optó por esta última opción. El Alg. \ref{alg:oqsopenssl111apache_curlcert} utiliza la opción \verb|-v| del comando \verb|docker| para crear un volumen dentro del contenedor cURL, mapeando el directorio local donde se encuentra el certificado de la CA, con el directorio \verb|/opt/cacert| de dicho contenedor. Mediante el modificador \verb|--cacert| se le indicó a \verb|curl| la ruta interna donde se encuentra el certificado de la CA. Las opciones \verb|-vvI| se usaron para ampliar la información mostrada por pantalla.

\begin{comment}
	en el comando \verb|curl|. Más adelante en esta sección se realizará la mencionada verificación, y se analizará el certificado digital de la CA. La Fig. \ref{fig:openssl111_oqsmaster_https_certfail} muestra la salida del comando de conexión, donde se aprecia la advertencia relativa al fallo en la verificación del certificado del servidor.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_https_certfail.png}
	\caption{Fallo en la verificación de certificado digital.}
	\label{fig:openssl111_oqsmaster_https_certfail}
\end{figure}

Para salvar esta advertencia y realizar la verificación correctamente se extrajo de la imagen Docker el certificado digital de la autoridad certificante, y se utilizó en la llamada \verb|curl| para verificar la firma del certificado digital enviado por el servidor. Se utilizó el comando \verb|cat| dentro del contenedor para leer el contenido del certificado digital, situado en el directorio \verb|/opt/httpd/cacert/|, y una redirección de salida en el intérprete de comandos para escribir su contenido en un nuevo archivo de texto que se ha denominado \verb|CA.crt| (Alg. \ref{alg:oqsopenssl111apache_certextract}).

Con el certificado digital en el directorio local (\verb|pwd|), puede invocarse el contenedor Docker de cURL para verificar la conexión (Alg. \ref{alg:oqsopenssl111apache_curlcert}).
\end{comment}

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. cURL con claves.},
	label={alg:oqsopenssl111apache_curlcert},
	%firstnumber=1,
	numbers=none,
	]
docker run --network httpd-test \
	-v $KEYSDIR/oqs_httpd:/opt/cacert -it \
	openquantumsafe/curl curl --cacert /opt/cacert/CA.crt \
	https://oqs-httpd:4433 --curves frodo640aes -vvI
\end{lstlisting}

Este comando ejecutó el contenedor de cURL, \textit{mapeó} el directorio local al directorio \verb|/opt/cacert| interno de dicho contenedor, y corrió la utilidad \verb|curl|. Se le pasó por argumento el certificado digital situado en el directorio mapeado, y su verificación correcta puede verse en la Fig. \ref{fig:openssl111_oqsmaster_https_certok}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/openssl111_oqsmaster_https_certok.png}
	\caption{Verificación correcta de certificado digital.}
	\label{fig:openssl111_oqsmaster_https_certok}
\end{figure}

\begin{comment}
	Se realizó un procedimiento similar para extraer el certificado propio del servidor, y analizar su contenido. Para encontrar dicho certificado digital y la clave se analizó el contenido del archivo de configuración \verb|httpd-ssl.conf| del servidor Apache dentro la imagen Docker. El Alg. \ref{alg:oqsopenssl111apache_confcertkey} muestra el comando utilizado para encontrar la configuración, y para extraer el certificado digital de la imagen docker. Este Algoritmo también muestra un fragmento de los datos internos del certificado, donde puede comprobarse que es de tipo Dilithium2.

\begin{lstlisting}[
	language={Bash},
	caption={OQS-OpenSSLv1.1.1 y Apache. Certificado y clave.},
	label={alg:oqsopenssl111apache_confcertkey},
	%firstnumber=1,
	numbers=none,
	]
# Configuración de certificado y clave (https-ssl.conf):
docker run --network httpd-test --name oqs-httpd -p \
	4433:4433 openquantumsafe/httpd cat \
	/opt/httpd/httpd-conf/httpd-ssl.conf\
	|grep "^SSLCertificate"

# Salida:
SSLCertificateFile "/opt/httpd/pki/server.crt"
SSLCertificateKeyFile "/opt/httpd/pki/server.key"

# Descarga de certificado digital:
docker run --network httpd-test --name oqs-httpd \
	-p 4433:4433  openquantumsafe/httpd \
	cat /opt/httpd/pki/server.crt > server.crt

# Extracción de datos:
module load openssl/1.1.1
openssl x509 -in server.crt -noout -text

# Salida (resumida para resaltar información de interés):
[...]
Subject: CN = oqs-httpd
Subject Public Key Info:
Public Key Algorithm: dilithium2
dilithium2 Public-Key:
[...]
\end{lstlisting}

Cabe mencionar finalmente que, así como se ha mapeado el directorio local, también pueden cargarse configuraciones particulares de usuario en un archivo \verb|httpd.conf| en el sistema local, y hacer uso el directorio compartido para que el contenedor Docker pueda hacer uso de dichas configuraciones.

\end{comment}

\begin{comment}
\textit{Probar compilar con otro algo por default a ver si genera cetrificados nuevos distintos de Dil2}
\todo{do}
\end{comment}

