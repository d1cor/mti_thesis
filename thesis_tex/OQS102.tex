\cleardoublepage
\chapter{ANÁLISIS DE OQS-OPENSSL}
\label{section:oqs_pruebas}

El proyecto OQS integró la biblioteca liboqs con un fork de OpenSSL la que denominó OQS-OpenSSL, para proveer un prototipo que soporte intercambio de claves, autenticación y cipher suites post-cuánticos. El objetivo de esta integración es brindar un prototipado sencillo y experimental de criptografía post-cuántica, y no debería ser considerado una suite de seguridad estable para su uso en entornos de producción. OQS realizó la integración de liboqs en dos versiones de OpenSSL: 1.0.2 y 1.1.1. En el presente trabajo se analizaron ambas versiones, sus algoritmos soportados y consideraciones de seguridad. Se estudió su integración con OpenVPN, Apache y Nginx. En todos los casos se utilizó la útlima versión estable de liboqs al momento de realizar las pruebas, la v0.3.0.

\subsubsection{Nota de obsolescencia }
\label{section:obsolescencia_oqs-openssl_102}

OpenSSL suspendió el soporte para la versión 1.0.2 el 1 de Enero de 2020, como resultado, el proyecto OQS decidió discontinuar el soporte de esta versión de OQS-OpenSSL, por lo que ya no se actualizará su repositorio Git. Debido a esto los nuevos proyectos que requieran implementar criptografía post-cuántica en TLS deberían utilizar OQS-OpenSSL v1.1.1. Igualmente, dado que la versión v1.1.1 de OQS-OpenSSL no soporta negociación de cipher suites post-cuánticas en TLS 1.3, el presente trabajo estudió la versión 1.0.2 con la intención de analizar dicha negociación en TLS v1.2.


\section{ANÁLISIS DE OQS-OPENSSL v1.0.2}
\label{section:oqs102}

OQS-OpenSSL 1.0.2 es un fork de OpenSSL v1.0.2 integrado con la librería liboqs que facilita el armado de prototipos de criptografía post-cuántica en TLS v1.2. Este fork soporta los siguientes mecanismos criptográficos:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li { Intercambio de claves post-cuántico.}
	\li { Intercambio de claves híbrido (post-cuántico y curva elíptica).}
	\li { Primitivas post-cuánticas de liboqs en el comando speed de OpenSSL}
\end{itemizenospace}

OQS-OpenSSL v1.0.2 sólo permite comunicaciones en TLS1.2 y brinda mecanismos de intercambio de claves post-cuánticos e híbridos, a los que añade negociación de cipher suites resistentes a ataques cuánticos. En el presente trabajo se realizaron pruebas de concepto del intercambio de claves híbrido y de negociación de cipher suites post-cuánticas en TLS.

OQS-OpenSSL v1.0.2 no debería ser utilizado en producción debido a que su línea de desarrollo es experimental y, además, esta versión se considera obsoleta, tal y como se mencionó anteriormente. Este proyecto desarrolla un prototipo de criptografía post-cuántica previo a la estandarización de nuevos algoritmos por parte del NIST, por lo que es también recomendable remitirse a criptografía híbrida para montar servicios que sean, como mínimo, tan seguros como los algoritmos criptográficos tradicionales.

\begin{comment}
Para llevar a cabo pruebas de seguridad TLS como las planteadas por \citep{jagerSecurityTLSDHEStandard2012} o \citep{krawczykSecurityTLSProtocol2013} se requiere un mecanismo de intercambio de claves que permita seguridad activa tal como lo hacen las funciones pseudorandom Oracle-Diffie-Hellman (PRF-ODH) \citep{brendelPRFODHRelationsInstantiations2017} o un algoritmo de intercambio de claves basado en indistinguibilidad bajo un ataque de texto plano seleccionado (IND-CPA) \citep{jiangINDCCASecureKeyEncapsulation2018}.
\end{comment}

\subsection{Cipher suites disponibles}

Los algoritmos de intercambio de claves y cipher suites disponibles en esta versión de OQS-OpenSSL pueden listarse utilizando el comando \verb|ciphers| del OpenSSL compilado. La Sección \ref{section:oqs102_prueba} amplía estos detalles y analiza capturas de tráfico de red. En general estas cipher suites post-cuánticas incluyen el prefijo \verb|OQSKEM-DEFAULT| en su definición. OQS-OpenSSL v1.0.2 soporta cipher suites tradicionales y post-cuánticas.

\subsection{Algoritmos de Intercambio de claves}
\label{section:algs102}

Los algoritmos de intercambio de claves soportados por este fork son:
\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li { \verb|DEFAULT|}
	\li { \verb|DEFAULT-ECDHE|: \verb|DEFAULT| en modo híbrido con ECDH.}
\end{itemizenospace}

\begin{comment}
Algo similar ocurre en OQS-OpenSSL v1.1.1 con el contenido de la variable \verb|oqs_kem_default| (ver sección \ref{section:algs111kem}).
\end{comment}

\verb|DEFAULT| indica que, durante la negociación KEM, se haga uso del algoritmo configurado por defecto durante la compilación de liboqs. \verb|DEFAULT-ECDHE| es el mecanismo KEX predeterminado en modo híbrido con ECDHE. El Alg. \ref{alg:oqsopenssl102_kemmethod} muestra un fragmento del archivo \verb|src/kem/kem.c| de código fuente de liboqs. En dicho fragmento se ve el modo en el que se selecciona del algoritmo KEM predeterminado. La función \verb|OQS_KEM_alg_is_enabled| retornará \verb|OQS_KEM_DEFAULT| si el método de intercambio de claves por defecto es \verb|OQS_KEM_alg_default|. En caso contrario se verificará, contra una lista de algoritmos válidos, el algoritmo que fue pasado por argumento, \verb|method_name|, y retornará 1 si dicho algoritmo se encuentra habilitado.

\begin{comment}
	\begin{code}
if (0 == strcasecmp(method_name, OQS_KEM_alg_default)) {
	return OQS_KEM_alg_is_enabled(OQS_KEM_DEFAULT);                                             
\end{code}
\end{comment}

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_kemmethod.jpg}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Selección de algoritmo KEM.}}
	\label{fig:oqsopenssl102_kemmethod}
\end{figure}
\end{comment}

\begin{lstlisting}[
	language=C, caption={OQS-OpenSSLv1.0.2. Selección de algoritmo KEM.}, label={alg:oqsopenssl102_kemmethod}, firstnumber=36]
OQS_API int OQS_KEM_alg_is_enabled(const char *method_name) {
	if (method_name == NULL) {
		return 0;
	}
	if (0 == strcasecmp(method_name, OQS_KEM_alg_default)) {
		return OQS_KEM_alg_is_enabled(OQS_KEM_DEFAULT);
		
	} else if (0 == strcasecmp(method_name, OQS_KEM_alg_bike1_l1)) {
#ifdef OQS_ENABLE_KEM_bike1_l1
		return 1;
#else
		return 0;
#endif
\end{lstlisting}

La lógica de selección del algoritmo KEM predeterminado se encuentra en el script de configuración de liboqs que debe ejecutarse previo a la compilación. Esto puede apreciarse en el Alg. \ref{alg:oqsopenssl102_kemmethodconfig}. La variable \verb|$KEM_DEFAULT| recibirá, de manera predeterminada, el algoritmo \verb|OQS_KEM_alg_sike_p503|, salvo que se configure otro diferente durante la compilación.

\begin{lstlisting}[
	language=C,
	caption={OQS-OpenSSLv1.0.2. Configuración de algoritmo KEM.},
	label={alg:oqsopenssl102_kemmethodconfig},
	firstnumber=21634]
case "$KEM_DEFAULT" in
#(
"")

$as_echo "#define OQS_KEM_DEFAULT OQS_KEM_alg_sike_p503" >>confdefs.h

;;
#(
*)

cat >>confdefs.h <<_ACEOF
#define OQS_KEM_DEFAULT $KEM_DEFAULT
_ACEOF

;;
esac
\end{lstlisting}

\begin{comment}
\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_kemmethodconfig.jpg}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Configuración de algoritmo KEM.}}
	\label{fig:oqsopenssl102_kemmethodconfig}
\end{figure}
\end{comment}

Una vez que el algoritmo fue seleccionado, la instalación de liboqs cargará dicho algoritmo en la cabecera \verb|openssl/oqs/include/oqs/oqsconfig.h|, utilizada por OQS-OpenSSL. El Alg. \ref{alg:oqsopenssl102_kemmethoddefault} muestra un fragmento de este archivo, la línea 93 especifica el algoritmo predeterminado en este caso.

\begin{lstlisting}[
	language=C,
	caption={OQS-OpenSSLv1.0.2. Algoritmo KEM predeterminado.},
	label={alg:oqsopenssl102_kemmethoddefault},
	firstnumber=93]
#define OQS_KEM_DEFAULT OQS_KEM_alg_sike_p503
\end{lstlisting}
\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_kemmethoddefault.jpg}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Algoritmo KEM predeterminado.}}
	\label{fig:oqsopenssl102_kemmethoddefault}
\end{figure}

\end{comment}
\begin{comment}
	\begin{code}
case "$KEM_DEFAULT" in
	"")
		$as_echo "#define \textsc{OQS_KEM_DEFAULT} OQS_KEM_alg_sike_p503" >>confdefs.h
		;;
	*)
		cat >>confdefs.h <<_ACEOF
		#define OQS_KEM_DEFAULT $KEM_DEFAULT
		_ACEOF
		;;
esac
\end{code}
\end{comment}

Si se quisiera utilizar otro algoritmo KEM se deberá añadir un parámetro adicional en la llamada a \verb|./configure| especificando el contenido de la variable \verb|OQS_KEM_DEFAULT|. Por ejemplo, para configurar \verb|FrodoKEM-640-AES| debe utilizarse la siguiente macro en la llamada a \verb|./configure|:

\begin{lstlisting}[
	language=C,
%	caption={Configuración de FrodoKEM-640-AES en liboqs.},
	label={alg:oqsopenssl102_kemfrodo},
	numbers=none
	]
-DOQS_KEM_DEFAULT="OQS_KEM_alg_frodokem_640_aes"
\end{lstlisting}

Los valores de macros válidos se encuentran definidos en el archivo de cabecera \verb|src/kem/kem.h|, y puede ser extendida para añadir nuevas implementaciones. El Alg. \ref{alg:oqsopenssl102_kemmethodheader} muestra un fragmento de este archivo, en el que se ven las variantes del algoritmo FrodoKEM.
\clearpage
\begin{lstlisting}[
	language=C,
	caption={OQS-OpenSSLv1.0.2. Definición de macros para KEM.},
	label={alg:oqsopenssl102_kemmethodheader},
	firstnumber=78]
/** Algorithm identifier for FrodoKEM-640-AES KEM. */
#define OQS_KEM_alg_frodokem_640_aes "FrodoKEM-640-AES"
/** Algorithm identifier for FrodoKEM-640-SHAKE KEM. */
#define OQS_KEM_alg_frodokem_640_shake "FrodoKEM-640-SHAKE"
/** Algorithm identifier for FrodoKEM-976-AES KEM. */
#define OQS_KEM_alg_frodokem_976_aes "FrodoKEM-976-AES"
/** Algorithm identifier for FrodoKEM-976-SHAKE KEM. */
#define OQS_KEM_alg_frodokem_976_shake "FrodoKEM-976-SHAKE"
/** Algorithm identifier for FrodoKEM-1344-AES KEM. */
#define OQS_KEM_alg_frodokem_1344_aes "FrodoKEM-1344-AES"
/** Algorithm identifier for FrodoKEM-1344-SHAKE KEM. */
#define OQS_KEM_alg_frodokem_1344_shake "FrodoKEM-1344-SHAKE"

\end{lstlisting}

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_kemmethodheader}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Definiciones de macros, algoritmos KEM.}}
	\label{fig:oqsopenssl102_kemmethodheader}
\end{figure}

\end{comment}
La siguiente lista muestra las macros válidas en este caso.

\begin{itemizenospace}
\setlength\itemsep{0em}
\li { \verb|OQS_KEM_alg_default|}

\li { \verb|OQS_KEM_alg_<B>_<L>|, donde \verb|<B>| puede ser \verb|bike1|, \verb|bike2| o \verb|bike3|, y \verb|<L>| puede ser L1, L3 o L5.}

\li { \verb|OQS_KEM_alg_kyber_<S>|, donde \verb|<S>| puede ser 512, 768 o 1024.}

\li { \verb|OQS_KEM_alg_newhope_512cca|, \verb|OQS_KEM_alg_newhope_1024cca|.}

\li { \verb|OQS_KEM_alg_ntru_<V>|, donde \verb|<V>| puede ser \verb|hps2048509|, \verb|hps2048677|, \verb|hps4096821| o \verb|hrss701|.}

\li { \verb|OQS_KEM_alg_saber_<V>|, donde \verb|<V>| puede ser \verb|lightsaber|, \verb|saber| o \verb|firesaber|.}

\li { \verb|OQS_KEM_alg_frodokem_<L>_<C>|, donde \verb|<L>| puede ser 640, 976 o 1344, y \verb|<C>| puede ser \verb|aes| o \verb|shake|.}

\li { \verb|OQS_KEM_alg_<V>_<L>|, donde \verb|<V>| puede ser \verb|sidh| o \verb|sike|, y \verb|<L>| puede ser \verb|p434|, \verb|p503|, \verb|p610| o \verb|p751|.}

\end{itemizenospace}

\begin{comment}
	\begin{table}[htp]
	\centering
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Algoritmos KEM}}
	\begin{tabular}{|l|l|}
		\hline
		\thead{Macro} & \thead{Valor} \\
		\hline
		\verb|OQS_KEM_alg_default| & \verb|"DEFAULT"| \\
		\hline
		\verb|OQS_KEM_alg_bike1_l1| & \verb|"BIKE1-L1"| \\
		\hline
		\verb|OQS_KEM_alg_bike1_l3| & \verb|"BIKE1-L3"| \\
		\hline
		\verb|OQS_KEM_alg_bike1_l5| & \verb|"BIKE1-L5"| \\
		\hline
		\verb|OQS_KEM_alg_bike2_l1| & \verb|"BIKE2-L1"| \\
		\hline
		\verb|OQS_KEM_alg_bike2_l3| & \verb|"BIKE2-L3"| \\
		\hline
		\verb|OQS_KEM_alg_bike2_l5| & \verb|"BIKE2-L5"| \\
		\hline
		\verb|OQS_KEM_alg_bike3_l1| & \verb|"BIKE3-L1"| \\
		\hline
		\verb|OQS_KEM_alg_bike3_l3| & \verb|"BIKE3-L3"| \\
		\hline
		\verb|OQS_KEM_alg_bike3_l5| & \verb|"BIKE3-L5"| \\
		\hline
		\verb|OQS_KEM_alg_kyber_512| & \verb|"Kyber512"| \\
		\hline
		\verb|OQS_KEM_alg_kyber_768| & \verb|"Kyber768"| \\
		\hline
		\verb|OQS_KEM_alg_kyber_1024| & \verb|"Kyber1024"| \\
		\hline
		\verb|OQS_KEM_alg_newhope_512cca| & \verb|"NewHope-512-CCA"| \\
		\hline
		\verb|OQS_KEM_alg_newhope_1024cca| & \verb|"NewHope-1024-CCA"| \\
		\hline
		\verb|OQS_KEM_alg_ntru_hps2048509| & \verb|"NTRU-HPS-2048-509"| \\
		\hline
		\verb|OQS_KEM_alg_ntru_hps2048677| & \verb|"NTRU-HPS-2048-677"| \\
		\hline
		\verb|OQS_KEM_alg_ntru_hps4096821| & \verb|"NTRU-HPS-4096-821"| \\
		\hline
		\verb|OQS_KEM_alg_ntru_hrss701| & \verb|"NTRU-HRSS-701"| \\
		\hline
		\verb|OQS_KEM_alg_saber_lightsaber| & \verb|"LightSaber-KEM"| \\
		\hline
		\verb|OQS_KEM_alg_saber_saber| & \verb|"Saber-KEM"| \\
		\hline
		\verb|OQS_KEM_alg_saber_firesaber| & \verb|"FireSaber-KEM"| \\
		\hline
		\verb|OQS_KEM_alg_frodokem_640_aes| & \verb|"FrodoKEM-640-AES"| \\
		\hline
		\verb|OQS_KEM_alg_frodokem_640_shake| & \verb|"FrodoKEM-640-SHAKE"| \\
		\hline
		\verb|OQS_KEM_alg_frodokem_976_aes| & \verb|"FrodoKEM-976-AES"| \\
		\hline
		\verb|OQS_KEM_alg_frodokem_976_shake| & \verb|"FrodoKEM-976-SHAKE"| \\
		\hline
		\verb|OQS_KEM_alg_frodokem_1344_aes| & \verb|"FrodoKEM-1344-AES"| \\
		\hline
		\verb|OQS_KEM_alg_frodokem_1344_shake| & \verb|"FrodoKEM-1344-SHAKE"| \\
		\hline
		\verb|OQS_KEM_alg_sidh_p434| & \verb|"Sidh-p434"| \\
		\hline
		\verb|OQS_KEM_alg_sidh_p503| & \verb|"Sidh-p503"| \\
		\hline
		\verb|OQS_KEM_alg_sidh_p610| & \verb|"Sidh-p610"| \\
		\hline
		\verb|OQS_KEM_alg_sidh_p751| & \verb|"Sidh-p751"| \\
		\hline
		\verb|OQS_KEM_alg_sike_p434| & \verb|"Sike-p434"| \\
		\hline
		\verb|OQS_KEM_alg_sike_p503| & \verb|"Sike-p503"| \\
		\hline
		\verb|OQS_KEM_alg_sike_p610| & \verb|"Sike-p610"| \\
		\hline
		\verb|OQS_KEM_alg_sike_p751| & \verb|"Sike-p751"| \\
		\hline
	\end{tabular}
	\label{table:kem_oqs102}
\end{table}
\end{comment}

Una vez elegido el mecanismo KEM y compilados liboqs y OQS-OpenSSL, la suite dispondrá de las siguientes cipher suites:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li { OQSKEM-\textbf{<KEX>}-RSA-AES128-GCM-SHA256}
	\li { OQSKEM-\textbf{<KEX>}-ECDSA-AES128-GCM-SHA256}
	\li { OQSKEM-\textbf{<KEX>}-RSA-AES256-GCM-SHA384}
	\li { OQSKEM-\textbf{<KEX>}-ECDSA-AES256-GCM-SHA384}
\end{itemizenospace}

Aquí, \textbf{<KEX>} puede tener dos valores: \verb|DEFAULT| o \verb|DEFAULT-ECDHE|.

\subsection{Algoritmos de Firma Digital}

El algoritmo de firma digital utilizado por defecto es Dilithium2. Al igual que en el caso anterior, puede configurarse este algoritmo modificando el código fuente de la implementación al configurar liboqs. El Alg. \ref{alg:oqsopenssl102_sigmethod} muestra un fragmento del código fuente \verb|src/sig/sig.h| de liboqs, donde se ve la lógica de decisión para la selección del algoritmo de firma predeterminado. De manera similar a como ocurría con los algoritmos KEM, la función \verb|OQS_SIG_alg_is_enabled| retornará \verb|OQS_SIG_DEFAULT| si el método de firma por defecto es \verb|OQS_SIG_alg_default|. En caso contrario se verificará, contra una lista de algoritmos válidos, el algoritmo que fue pasado por argumento, \verb|method_name|, y retornará 1 si dicho algoritmo se encuentra habilitado.

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_sigmethod.jpg}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Selección de algoritmo de firma digital.}}
	\label{fig:oqsopenssl102_sigmethod}
\end{figure}

\end{comment}
\begin{comment}
	\begin{code}
if (0 == strcasecmp(method_name, OQS_SIG_alg_default)) {
	return OQS_SIG_new(OQS_SIG_DEFAULT);                                                            
\end{code}
\end{comment}




\begin{lstlisting}[
	language=C,
	caption={OQS-OpenSSLv1.0.2. Selección de algoritmo de firma.},
	label=alg:oqsopenssl102_sigmethod,
	firstnumber=33,
	%numbers=none,
	]
OQS_API int OQS_SIG_alg_is_enabled(const char *method_name) {
	if (method_name == NULL) {
		return 0;
	}
	if (0 == strcasecmp(method_name, OQS_SIG_alg_default)) {
		return OQS_SIG_alg_is_enabled(OQS_SIG_DEFAULT);
		///// OQS_COPY_FROM_PQCLEAN_FRAGMENT_ENABLED_CASE_START
	} else if (0 == strcasecmp(method_name, OQS_SIG_alg_dilithium_2)) {
#ifdef OQS_ENABLE_SIG_dilithium_2
		return 1;
#else
		return 0;
#endif
[...]
\end{lstlisting}

El Alg. \ref{alg:oqsopenssl102_sigmethodconfig} muestra las líneas del archivo \verb|liboqs/configure| que cargan el algoritmo previo a la compilación. La variable \verb|$SIG_DEFAULT| recibirá, de manera predeterminada, el algoritmo de firma \verb|OQS_SIG_alg_dilithium_2|, salvo que se configure alguna alternativa antes de compilar de liboqs. Si se quisiera cambiar el algoritmo predeterminado debería especificarse un valor para la macro \verb|OQS_SIG_DEFAULT| durante la llamada a \verb|./configure|.

\begin{lstlisting}[
	language=C,
	caption={OQS-OpenSSLv1.0.2. Configuración de algoritmo de firma.},
	label={alg:oqsopenssl102_sigmethodconfig},
	firstnumber=21652,
	%numbers=none,
	]
case "$SIG_DEFAULT" in
#(
"")

	$as_echo "#define OQS_SIG_DEFAULT OQS_SIG_alg_dilithium_2" >>confdefs.h

;;
#(
*)

	cat >>confdefs.h <<_ACEOF
	#define OQS_SIG_DEFAULT $SIG_DEFAULT
	_ACEOF

;;
esac
\end{lstlisting}
\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_sigmethodconfig.jpg}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Configuración de algoritmo de firma.}}
	\label{fig:oqsopenssl102_sigmethodconfig}
\end{figure}

\end{comment}

El script \verb|./configure| cargará el algoritmo especificado en \verb|OQS_SIG_DEFAULT| dentro del archivo \verb|openssl/oqs/include/oqs/oqsconfig.h|. El Alg. \ref{alg:oqsopenssl102_sigmethoddefault} muestra un fragmento de dicho archivo, donde puede verse el algoritmo utilizado de manera predeterminada.

\begin{lstlisting}[
	language={C},
	caption={OQS-OpenSSLv1.0.2. Algoritmo SIG predeterminado.},
	label={alg:oqsopenssl102_sigmethoddefault},
	firstnumber=95,
	%numbers=none,
	]
#define OQS_SIG_DEFAULT OQS_SIG_alg_dilithium_2	
\end{lstlisting}

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_sigmethoddefault.jpg}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Algoritmo de firma predeterminado.}}
	\label{fig:oqsopenssl102_sigmethoddefault}
\end{figure}
\end{comment}

Para cambiar el algoritmo predeterminado puede modificarse el valor de la macro \verb|OQS_SIG_DEFAULT| en la llamada a \verb|./configure|. Por ejemplo, a continuación se muestra la definición de macro que configura qTESLA\_I como algoritmo de firma:

\begin{lstlisting}[
	language=C,
	caption={},
	label={alg:oqsopenssl102_sigqtesla},
	numbers=none,
	]
-DOQS_SIG_DEFAULT="OQS_SIG_alg_qTESLA_I"
\end{lstlisting}

Los valores de macros válidos están almacenados en el archivo de cabecera \verb|src/sig/sig.h|. El Alg. \ref{alg:oqsopenssl102_sigmethodheader} muestra un fragmento del mismo en el que se definen las tres versiones de qTESLA soportadas.

\begin{lstlisting}[
	language={C},
	caption={OQS-OpenSSLv1.0.2. Macros de firma digital.},
	label={alg:oqsopenssl102_sigmethodheader},
	firstnumber=52,
	%numbers=none,
	]
/** Algorithm identifier for qTESLA_I */
#define OQS_SIG_alg_qTESLA_I "qTESLA_I"
/** Algorithm identifier for qTESLA_III_size */
#define OQS_SIG_alg_qTESLA_III_size "qTESLA_III_size"
/** Algorithm identifier for qTESLA_III_speed */
#define OQS_SIG_alg_qTESLA_III_speed "qTESLA_III_speed"
\end{lstlisting}

\begin{comment}
	\begin{figure}[htp]
	\centering
	\includegraphics[width=1.0\textwidth]{img/oqsopenssl102_sigmethodheader}
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Definiciones de macros, algoritmos de firma.}}
	\label{fig:oqsopenssl102_sigmethodheader}
\end{figure}
\end{comment}

Los nombres de macros válidas se listan a continuación.

\begin{itemizenospace}
	\setlength\itemsep{0em}
\li {\verb|OQS_SIG_alg_default|}

\li {\verb|OQS_SIG_alg_picnic_<L>_<S>| \\
Donde \verb|<L>| puede ser \verb|L1|, \verb|L3| o \verb|L5|, y \verb|<S>| puede ser \verb|FS| o \verb|UR|}.

\li {\verb|OQS_SIG_alg_picnic2_L1_FS|, \verb|OQS_SIG_alg_picnic2_L3_FS|, \\ \verb|OQS_SIG_alg_picnic2_L5_FS|}

\li {\verb|OQS_SIG_alg_qTESLA_I|, \verb|OQS_SIG_alg_qTESLA_III_size|, \\ \verb|OQS_SIG_alg_qTESLA_III_speed|}

\li {\verb|OQS_SIG_alg_dilithium_2|, \verb|OQS_SIG_alg_dilithium_3|,\\ \verb|OQS_SIG_alg_dilithium_4|}

\li {\verb|OQS_SIG_alg_mqdss_31_48|, \verb|OQS_SIG_alg_mqdss_31_64|}

\li {\verb|OQS_SIG_alg_sphincs_<H>_<L>_<S>| \\
%Donde \verb|<H>| puede ser \verb|haraka|, \verb|sha256| o \verb|shake256|, \\
\verb|<L>| puede ser 128f, 128s, 192f, 192s, 256f y 256s, \\
y \verb|<S>| puede ser \verb|robust| o \verb|simple|.}
\end{itemizenospace}


\begin{comment}
	\verb|OQS_SIG_alg_sphincs_<H>_128s_robust|, \\ \verb|OQS_SIG_alg_sphincs_<H>_128s_simple|, \\ \verb|OQS_SIG_alg_sphincs_<H>_192f_robust|, \\ \verb|OQS_SIG_alg_sphincs_<H>_192f_simple|, \\ \verb|OQS_SIG_alg_sphincs_<H>_192s_robust|, \\ \verb|OQS_SIG_alg_sphincs_<H>_192s_simple|, \\ \verb|OQS_SIG_alg_sphincs_<H>_256f_robust|, \\ \verb|OQS_SIG_alg_sphincs_<H>_256f_simple|, \\ \verb|OQS_SIG_alg_sphincs_<H>_256s_robust|, \\ \verb|OQS_SIG_alg_sphincs_<H>_256s_simple|} \\
\end{comment}


\begin{comment}
	\begin{table}[htp]
%	\footnotesize
	\centering
	\caption{\textbf{OQS-OpenSSL v1.0.2 - Algoritmos de firma digital}}
	\begin{tabular}{|l|l|}
		\hline
		\verb|OQS_SIG_alg_default| \\
		\hline
		\verb|OQS_SIG_alg_picnic_L1_FS|, \verb|OQS_SIG_alg_picnic_L1_UR|, \\
		\verb|OQS_SIG_alg_picnic_L3_FS|, \verb|OQS_SIG_alg_picnic_L3_UR|, \\
		\verb|OQS_SIG_alg_picnic_L5_FS|, \verb|OQS_SIG_alg_picnic_L5_UR|, \\
		\verb|OQS_SIG_alg_picnic2_L1_FS|, \verb|OQS_SIG_alg_picnic2_L3_FS| \\
		\verb|OQS_SIG_alg_picnic2_L5_FS| \\
		\hline
		\verb|OQS_SIG_alg_qTESLA_I|, \verb|OQS_SIG_alg_qTESLA_III_size|, \\
		\verb|OQS_SIG_alg_qTESLA_III_speed| \\
		\hline
		\verb|OQS_SIG_alg_dilithium_2|,\\ \verb|OQS_SIG_alg_dilithium_3|, \verb|OQS_SIG_alg_dilithium_4| \\
		\hline
		\verb|OQS_SIG_alg_mqdss_31_48|, \verb|OQS_SIG_alg_mqdss_31_64| \\
		\hline
		\verb|OQS_SIG_alg_sphincs_<H>_128f_robust|,\\ \verb|OQS_SIG_alg_sphincs_<H>_128f_simple|,  \\
		\verb|OQS_SIG_alg_sphincs_<H>_128s_robust|,\\ \verb|OQS_SIG_alg_sphincs_<H>_128s_simple|,  \\
		\verb|OQS_SIG_alg_sphincs_<H>_192f_robust|,\\ \verb|OQS_SIG_alg_sphincs_<H>_192f_simple|,  \\
		\verb|OQS_SIG_alg_sphincs_<H>_192s_robust|,\\ \verb|OQS_SIG_alg_sphincs_<H>_192s_simple|,  \\
		\verb|OQS_SIG_alg_sphincs_<H>_256f_robust|,\\ \verb|OQS_SIG_alg_sphincs_<H>_256f_simple|,  \\
		\verb|OQS_SIG_alg_sphincs_<H>_256s_robust|,\\ \verb|OQS_SIG_alg_sphincs_<H>_256s_simple|, \\
		Donde \verb|<H>| es \verb|haraka|, \verb|sha256| o \verb|shake256|\\
		\hline
		
	\end{tabular}
	\label{table:sig_oqs102}
\end{table}

\end{comment}
