\cleardoublepage
\chapter{COMPUTACIÓN CUÁNTICA Y SEGURIDAD INFORMÁTICA}
\label{section:quantumcomputing}

En este capítulo se realizará una breve introducción a los conceptos fundamentales de la computación y la información cuánticas, como así también a los principales algoritmos cuánticos, importante motivación para el desarrollo de los temas tratados en este trabajo. Se introducirá la transformada cuántica de Fourier, los algoritmos de Shor y de Grover, y cómo éstos afectan a los mecanismos criptográficos actuales.

Las computadoras actuales funcionan convirtiendo información en series de dígitos binarios, o bits, y operando con estos bits mediante el uso de circuitos integrados que contienen millones de transistores. Cada bit tiene sólo dos valores posibles, 0 y 1. Mediante la manipulación de estos bits las computadoras pueden realizar todo su procesamiento.

Una computadora cuántica también representa información en bits, llamados bits cuánticos, o \textit{qubits}. Al igual que un bit clásico, un qubit puede tener un valor de 0 o 1, pero a diferencia de ellos, que solamente pueden estar en uno de estos dos estados, un qubit también puede estar en un estado especial denominado \textit{superposición}, en el que vale 0 y 1 al mismo tiempo. Cuando se extiende el sistema a una gran cantidad de qubits, la superposición de estados le da a la computadora cuántica un enorme potencial.

\begin{comment}
Por otro lado, las reglas que rigen los sistemas cuánticos también tienen su dificultad para aprovechar esta ventaja, por ejemplo, por la misma naturaleza de la mecánica cuántica, la mejor solución a un problema no es trivial.
\end{comment}

\section{FUNDAMENTOS DE LA FÍSICA CUÁNTICA}

A principios del siglo XX la mecánica cuántica fue uno de los modelos mejor conocidos para explicar el mundo de la física. Esta teoría describe el comportamiento de las partículas en escalas de energía y distancia muy pequeñas, sentando las bases para entender las propiedades de la materia.

Un objeto cuántico no existe en un estado determinado y visible. Cada vez que se observa un objeto cuántico se ve como una partícula, pero cuando no está siendo observado se comporta como una onda. Es por esto que los fenómenos físicos en este sentido tienen una dualidad onda-partícula. Mientras que la evolución del sistema tiene un comportamiento ondulatorio, cualquier medición del mismo obtendrá un valor que podría demostrar que se trata de una partícula. Un ejemplo clásico de este comportamiento es el experimento de la doble ranura (Fig. \ref{fig:dobleranura}).

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.6\textwidth]{img/dobleranura.jpeg}
	\caption{Experimento de la doble ranura.}
	\source{\citep{young_fisica_2013}}
	\label{fig:dobleranura}
\end{figure}

Un objeto cuántico puede existir en múltiples estados a la vez, con cada uno de ellos coexistiendo e interfiriéndose como si fueran ondas. El estado de cualquier sistema cuántico se describe en términos de funciones de onda. En muchos casos el estado de un sistema puede ser expresado matemáticamente como la suma de los posibles sub-estados que lo constituyen, escalados por un número complejo que representa el peso relativo de cada uno. Este valor complejo permite expresar, mediante parte real e imaginaria, la amplitud y la fase de la onda. Por ejemplo, $Ae^{i\theta}$ hace referencia a una función de onda con amplitud $A$ y fase $\theta$. Se dice que los estados son \textit{coherentes} porque cada uno puede interferir con los otros en forma constructiva y destructiva, tal y como ocurre con las ondas.

Cuando se intenta observar un sistema cuántico se ve sólo uno de los estados componentes, y esto ocurre con una probabilidad proporcional al cuadrado del valor absoluto de su coeficiente. Para un observador el sistema siempre parecerá clásico cuando se lo mide. La observación de un objeto cuántico o de un sistema cuántico (conjunto de objetos cuánticos) ocurre cuando el objeto interactúa con un sistema físico mayor que extrae información de él. Así, el hecho de medir el objeto cuántico tiene un efecto disruptivo sobre su estado, el aspecto de la onda que fue medida representa sólo uno de los estados observables, con la consecuente pérdida de información. Luego de la medición, la función de onda del objeto cuántico medido será el resultado de la medición, y perderá su estado previo.

Para visualizar esta situación considérese el ejemplo de una moneda sobre una mesa. En el mundo de la física clásica el estado de la moneda será \textit{cara} o \textit{cruz}. Una versión cuántica de la moneda podría existir en una combinación o superposición de ambos estados. La función de onda de la moneda cuántica podría ser escrita como la suma ponderada de ambos estados, escalada por los coeficientes $C_C$ y $C_Z$, donde $C$ y $Z$ representan el cara y cruz de la moneda respectivamente. Un intento por observar el estado de la moneda cuántica conducirá a encontrarla en uno de sus dos estados, cara o cruz, con una probabilidad proporcional al cuadrado de su coeficiente. A esta medición, que deriva en la configuración de un estado particular del objeto cuántico, se la suele llamar \qq{colapso de la función de onda}.

\begin{comment}
Un objeto cuántico puede entonces estar en varios estados superpuestos, y esta superposición puede ser descrita como una combinación lineal de las contribuciones de cada estado individual, con coeficientes complejos. Cuando el estado de un sistema puede describirse como un conjunto de números complejos, uno para cada estado base, se dice que el estado completo del sistema es \textbf{coherente}. La coherencia es necesaria para los fenómenos cuánticos, como la interferencia cuántica, la superposición y el entrelazamiento. Pequeñas interacciones con el medio ambiente provocan que los sistemas cuánticos se descifren lentamente. Las interacciones ambientales hacen que incluso los coeficientes complejos para cada estado sean probabilísticos.


Debido a que un par de monedas convencionales tienen cuatro estados posibles (AA, AR, RA, RR), un par de monedas cuánticas podrían existir en una \textit{superposición de esos cuatro estados}, cada uno ponderado por su coeficiente, a saber, $C_{AA'}$, $C_{AR'}$, $C_{RA'}$, $C_{RR'}$.

Luego de la medición el par de monedas cuánticas parecerá un par de monedas clásicas, tendrán una de las cuatro configuraciones mencionadas. En forma generalizada, un conjunto de $n$ monedas tendrá $2^n$ estados posibles. 
\end{comment}

Además, bajo algunas circunstancias, dos o mas objetos cuánticos en un sistema pueden estar intrínsecamente enlazados, de modo que la medición de uno de ellos revelará también el estado del otro, haciéndo colapsar su función de onda, independientemente de la distancia que los separe. Esta propiedad se conoce como entrelazamiento cuántico, y es la clave principal del gran potencial de la computación cuántica. Esto ocurre cuando las funciones de onda de ambas partículas no son separables matemáticamente, es decir, cuando la función de onda del sistema completo no puede escribirse como un producto de las funciones de onda de cada partícula individual. Para este fenómeno no existe un equivalente en la física clásica.

\section{INFORMACIÓN CUÁNTICA}

La ciencia de la información cuántica explora la manera en la que se puede codificar la información en un sistema cuántico. En este campo de estudio son de interés tres ramas importantes: las comunicaciones cuánticas, la detección cuántica y la computación cuántica.

Las investigación y desarrollo en \textbf{comunicaciones cuánticas} se centran en el transporte o intercambio de información codificándola en un sistema cuántico. Es probable que se necesiten nuevos protocolos de comunicación para que las computadoras cuánticas puedan transportar información. Un campo importante en este punto es el de la criptografía cuántica, que diseña sistemas de comunicaciones basados en principios de física cuántica.

Un uso interesante en este campo es el de QKD (Quantum Key Distribution - Distribución de claves cuánticas), un método de distribución de claves criptográficas llevado a cabo por medio de mecanismos cuánticos. El protocolo mejor conocido en este entorno es el BB84, desarrollado por Charlie Bennet y Gilles Brasard en 1984 \citep{bennettQuantumCryptographyBell1992}. Este protocolo fue desarrollado experimentalmente  en cables de fibra óptica y comunicaciones satelitales, y ha sido la guía de varios desarrollos comerciales.% Se analizarán algunos detalles adicionales de QKD en el capítulo \ref{section:quantumcrypto}.

Por su parte la \textbf{detección cuántica} involucra el estudio y desarrollo de sistemas cuánticos que son extremadamente sensibles al entorno, y pueden ser explotados para medir propiedades físicas tales como campos magnéticos, eléctricos, gravitacionales, o incluso temperatura, con una precisión mayor que la que ofrecen los las tecnologías actuales.

Finalmente, la \textbf{computación cuántica} implica el estudio de propiedades cuánticas para ejecutar cálculos computacionales. Una computadora cuántica es un sistema físico compuesto por una colección de qubits apareados. Estos qubits pueden ser manipulados para que la medición del estado final del sistema pueda resolver un problema con una muy alta probabilidad de éxito. Los qubits de una computadora cuántica deben estar lo suficientemente aislados del entorno para que su estado se mantenga coherente durante el tiempo que lleve realizar los cálculos. El campo de la computación está entrando en la era NISQ (Noisy Intermediate Scale Quantum - Dispositivos Cuánticos de Escala Intermedia) \citep{preskill2018quantum}. Esto es, la construcción de computadoras cuánticas lo suficientemente grandes (decenas de cientos de qubits) que no pueden ser simulados eficientemente en computadoras clásicas. Aún no se logra construir un procesador cuántico que gestione correctamente el ruido, por lo que los algoritmos cuánticos no pueden implementarse directamente.

\section{ALGORITMOS CUÁNTICOS Y SEGURIDAD}
\label{section:algoritmoscuanticosyseguridad}

En computación clásica se verifica que el número total de operaciones requeridas para resolver un problema es independiente del diseño de la computadora que las ejecute. A este principio se lo denomina tesis de Church-Turing extendida, e implica que, para resolver un problema computacional de manera más rápida, se deben realizar algunas de estas tareas:

\begin{enumeratenospace}
	\setlength\itemsep{0em}
	\li Reducir el tiempo en implementar cada operación simple.
	\li Ejecutar muchas operaciones en paralelo.
	\li Reducir el total de operaciones para completar el cálculo.
\end{enumeratenospace}

La computación cuántica viola la tesis extendida pudiendo resolver ciertos cálculos con menos operaciones que una computadora clásica que use el mejor algoritmo conocido para dicha tarea. Se abre así una nueva posibilidad para solucionar problemas computacionales. Esto genera gran preocupación en la comunidad de expertos en seguridad informática ya a que la dificultad de resolver ciertos cálculos con una computadora clásica es la base de muchos sistemas criptográficos actuales.

Durante las últimas décadas se han estudiado intensamente los problemas de factorización de números grandes. En el caso de la factorización de un número $N$ de $n$ bits ($N = 2^n$), los posibles divisores primos de $N$ incluyen todos los números primos menores que $N$, y la cantidad de cifras a evaluar es $\exp(n) = e^n$. Esta función implica que el tiempo de ejecución del algoritmo crece exponencialmente respecto del crecimiento del tamaño del número a factorizar. Los algoritmos que se ejecutan en tiempo exponencial no son eficientes en procesadores clásicos, por lo que no resultan prácticos para factorizar números grandes.

Existen algoritmos de factorización como GNFS (Criba general del cuerpo de números, por sus siglas en inglés) \citep{briggs_introduction_1998} cuyo tiempo de ejecución es sub-exponencial, es decir, son más eficientes que un algoritmo de tiempo exponencial, pero no lo suficiente como un algoritmo de tiempo polinómico. Así, GNFS, el mejor algoritmo de factorización de números grandes conocido, tampoco es práctico ejecutándose en un procesador clásico.

No se conocen algoritmos de factorización de tiempo polinómico para procesadores clásicos, pero sí dentro de la computación cuántica. El algoritmo cuántico de Shor (descrito en la Sección \ref{section:shor}) puede factorizar números grandes en un tiempo polinómico $O(n^3)$. Esto implica que, sobre una computadora cuántica, el tiempo de ejecución del algoritmo crece polinómicamente respecto del crecimiento del número, por lo que el algoritmo de Shor podría factorizar eficientemente números grandes como los utilizados en algoritmos de cifrado asimétrico actuales.

%Entre los objetivos de la teoría de algoritmos se encuentra resolver problemas de complejidad P, tareas computacionales en un número de pasos que escale polinómicamente según el tamaño de $N$. En computación cuántica la clase de complejidad que corresponde con estos problemas, y que contiene todas las tareas que una computadora cuántica puede resolver en tiempo polinomial cuántico de error acotado (BQP, por sus siglas en inglés). (Bounded-error Quantum Polynomial - Tiempo Polinomial Cuántico de Error Acotado). %Por el contrario, los algoritmos cuyo tiempo de ejecución escala exponencialmente según el tamaño de la entrada se vuelven extremadamente costosos en la medida en que la entrada aumenta su tamaño.

Es importante notar que las computadoras cuánticas no aceleran uniformemente todos los problemas computacionales. Existen problemas como los NP, que no pueden resolverse en tiempo polinómico. Esta categoría incluye problemas aún más complejos, los NP-completos \citep{karp1975computational}. El algoritmo de Shor prueba que los algoritmos cuánticos requieren un tiempo exponencial para resolver los problemas NP-completos \citep{bennett1997strengths}.

\begin{comment}
Así que, si se considera a $N$ como el conjunto de soluciones, Bennett muestra que un algoritmo cuántico puede resolver el problema como mínimo en $N^{1/2}$ pasos.


El diseño de algoritmos cuánticos sigue principios diferentes de los que se utilizan en algoritmos clásicos. Los algoritmos que logran aprovechar un procesador cuántico utilizan paradigmas o técnicas de construcción que no tienen una contraparte en computación clásica.


Grover, por su parte, requiere un tiempo de $O(N^{1/2})$ pasos \citep{grover1996fast}. 

Una computadora clásica puede verificar el grado de corrección de una solución de algoritmo NP en un tiempo polinómico, sin importar la dificultad para encontrar dicha solución. 

La potencia de un algoritmo cuántico se deriva exponencialmente de la complejidad del propio sistema cuántico. Dado el estado de un sistema como $N=2^n$. Además las aplicaciones de cada compuerta elemental, por ejemplo, dos qubits, actualiza los $2^n$ números complejos que describen el estado, lo que parece demostrar que se pueden ejecutar $2^n$ cálculos en un simple paso. Por otro lado, al terminar los cálculos, cuando se miden los qubits se obtiene un resultado de $n$ bits clásicos, y el diseño de algoritmos tiene como objetivo tomar las ventajas de estos dos fenómenos, es decir, encontrar algoritmos que aprovechen el paralelismo que brindan los procesadores cuánticos, y produzca un estado cuántico final que tenga grandes probabilidades de retornar información valiosa luego de la medición. Las mejores aproximaciones hasta el momento toman ventajas del fenómeno de interferencia cuántica para generar resultados útiles. A continuación se resumen brevemente algunas características de estas soluciones.

\end{comment}
\subsection{Transformada cuántica de Fourier}

La transformada de Fourier es una operación que convierte la representación de una señal en otra forma de representación  diferente. La transformada clásica convierte una función del dominio del tiempo al de la frecuencia. La función inversa, denominada anti-transformada de Fourier, permite convertir una función del dominio de la frecuencia al del tiempo sin pérdida de información. Esta propiedad es clave para cualquier operación en una computadora cuántica. La transformada cuántica de Fourier, o QFT (Quantum Fourier Transform) es el equivalente cuántico de la transformada clásica, y constituye un bloque básico de construcción de algoritmos cuánticos. 

\begin{comment}
Concretamente, la entrada es un vector N-dimensional con entradas complejas $(a_1, a_2, \cdots, a_n)$, y las salidas son un vector N-dimensional $(b_1, b_2, \cdots, b_n)$.
\end{comment}

Debido a la utilidad de la transformada de Fourier muchos algoritmos la utilizan en computación clásica. Una de las mejores implementaciones es la Transformada Rápida de Fourier (FFT, por sus siglas en inglés), que se evalúa en un tiempo $O(N\log N)$ para un número $N$ de $n$ bits. Si bien la FFT es eficiente, existen implementaciones de la QFT que, haciendo uso de compuertas de uno o dos qubits, se evalúan en un tiempo $O(\log^2 N)$, que representa una aceleración exponencial respecto de la FFT \citep{napQuantumComputingProgress2019}. Cabe mencionar que esta eficiencia solamente se da cuando las entradas de la QFT son pre-codificadas en qubits, y no leídas directamente. De esta forma la QFT permite aprovechar las características de las computadoras cuánticas, y por ello es útil para crear una gran cantidad de algoritmos cuánticos, entre los que se encuentran la factorización, o la búsqueda de estructuras ocultas y estimación de fase cuántica.

%Considerando nuevamente a $N$ un número de $n$ bits, es decir, $N=2^n$, una de las mejores implementaciones es la FFT (Fast Fourier Transform - Transformada Rápida de Fourier), que se evalúa en un tiempo $O(N\log N)$. 

\begin{comment}
, donde $N=2^n$ en su formulación original, luego mejorada para ejecutarse en un tiempo $O(n\log n)$ \citep{hales2000improved}.
\end{comment}

\begin{comment}
Si el tiempo requerido para leer los datos de entrada es $O(N)$, el algoritmo cuántico puede ser completado en sólo $O(\log^2 n)$, lo que ya incrementa el rendimiento comparado con su equivalente clásico si los datos de entrada son pre-codificados en $\log N$ qubits y no son leídos directamente desde la entrada. Se considera que estos qubits están en superposición de $N$ estados cuánticos, y el coeficiente en cada estado representa la secuencia de datos a ser transformada por la QFT \citep{nationalacademiesofsciencesengineeringandmedicineQuantumComputingProgress2019}.
\end{comment}



\subsection{Factorización cuántica y el algoritmo de Shor}
\label{section:shor}

Peter Shor descubrió en 1994 algoritmos cuánticos de tiempo polinómico para la factorización y el calculo de logaritmos discretos \citep{shor1994algorithms}. Esto representó un enorme avance en el campo de los algoritmos cuánticos por la aparente aceleración en comparación con las variantes clásicas. Este tipo de algoritmos se consideran una buena forma de explotar la aceleración exponencial de la QFT, incluso teniendo en cuenta las limitaciones de entrada y salida del muestreo de Fourier.

Para poder aprovechar el poder de la QFT, Shor primero convierte el problema de encontrar los factores de un número en otro problema, hallar un patrón repetido. Shor fue capaz de mostrar que la factorización de un problema es equivalente a encontrar el período de una secuencia de números, aunque ésta sea exponencialmente más larga que la cantidad de bits del número a factorizar. En un ordenador clásico esta operación no mejoraría el tiempo de cálculo, ya que la computadora debería generar esta secuencia de $2^n$ números para factorizar un número de $n$ bits, lo que llevaría a una cantidad de tiempo exponencial. Sin embargo, en un ordenador cuántico una secuencia exponencialmente larga puede codificarse sólo en $n$ qubits, y generarse en un tiempo polinómico. Una vez que se ha generado esa secuencia, se puede usar la QFT para encontrar su período.

Debe tenerse en cuenta que el resultado devuelto será solamente una muestra de las amplitudes de la transformada de Fourier, aunque esto no es una limitante ya que, con una alta probabilidad, la información deseada será la que se encuentre en dicha muestra. De esta manera, si se desplegara el algoritmo de Shor en una computadora cuántica perfecta, sería posible calcular la clave secreta de la mayoría de los criptosistemas de clave pública tales como RSA o DSA, o algoritmos de intercambio Diffie-Hellman.

\begin{comment}
	El descubrimiento de los algoritmos de tiempo polinómico para la factorización y el calculo de logaritmos discretos realizado por Peter Shor \citep{shor1994algorithms} fue un enorme avance en el campo de los algoritmos cuánticos por la aparente aceleración en comparación con las variantes clásicas. Este tipo de algoritmos pueden considerarse una interesante forma de explotar la aceleración exponencial de la QFT, incluso teniendo en cuenta las limitaciones de entrada y salida del muestreo de Fourier.
\end{comment}


\begin{comment}
El algoritmo cuántico de Shor para factorizar y calcular logaritmos discretos puede ser considerado un ejemplo que permite encontrar estructuras algebraicas ocultas, relacionadas con problemas matemáticos bien conocidos como el \qq{el problema del subgrupo oculto}. Actualmente existen aproximaciones cuánticas para resolver algunos casos de estos problemas eficientemente, tales como las variedades Abelianas. Por otro lado se encuentran los problemas del grupo de simetría diédrica, que se espera que sean difíciles de resolver para computadoras cuánticas (y clásicas también). Este tipo de problemas está relacionado con otro llamados problemas de vector más corto, y son la base de los criptosistemas de aprendizaje con errores, o LWE (Learning With Errors), uno de los esquemas post-cuánticos más prometedores.
\end{comment}

\subsection{Algoritmo de Grover}

\begin{comment}
Así como la QFT es la base de muchos algoritmos cuánticos, otra clase de algoritmos aprovecha la ventaja de un método diferente denominado \qq{caminata cuántica aleatoria}. Este método es análogo al de caminada aleatoria clásica que describe un camino consistente en una sucesión de pasos aleatorios en un espacio matemático donde el estado del caminante está descrito por una distribución probabilística de posiciones.
\end{comment}

El algoritmo de Grover aborda el problema de encontrar las entradas únicas de una función que producirán una determinada salida \citep{grover1996fast}. En computación clásica representa un problema clasificado como NP-duro, es decir, no tiene soluciones de tiempo polinómico conocidas. A falta de información sobre la naturaleza de la función, el algoritmo clásico más rápido que se conoce para este problema es el de búsqueda exhaustiva, o la exploración de todas las entradas posibles hasta dar con la solución, proceso que toma $O(N) = O(2^n)$ pasos, donde $n$ es el número de bits necesarios para representar la entrada. El algoritmo de Grover resuelve este problema en $O(N^{1/2})$ pasos. Aunque esto es sólo una aceleración polinómica sobre el mejor enfoque clásico, podría ser significativo en la práctica, y sería capaz comprometer algunas operaciones criptográficas. 

\begin{comment}
Además, éste es el algoritmo cuántico óptimo para resolver este tipo de problemas según \citep{bennett1997strengths}, ya que el puede tomar al menos $\sqrt{N}$ pasos para resolver el problema en el modelo de caja negra.
\end{comment}

\section{ALGORITMOS Y RESISTENCIA CUÁNTICA}

En esta sección se analizarán las implicaciones de la computación cuántica en los mecanismos criptográficos utilizados en las comunicaciones: intercambio de claves, cifrado asimétrico, cifrado simétrico, firmas y certificados digitales, funciones resumen y contraseñas.

\subsection{Intercambio de claves y cifrado asimétrico}

Un protocolo de intercambio de claves basado en Diffie-Hellman asume que ciertos problemas algebraicos son intratables, es decir, hasta la fecha no se ha encontrado un algoritmo que pueda resolverlo eficientemente. Para romper ECDH se necesita resolver un problema de logaritmos discretos de curva elíptica. Una computadora clásica puede resolver un problema de este tipo en un tiempo exponencial $2^{n/2}$, siendo $n$ el tamaño de la clave. Así, por ejemplo, para una clave de $256$ bits el mejor algoritmo conocido puede vulnerarla en un tiempo $2^{128}$. Este tiempo es equivalente al requerido para atacar un cifrados simétrico AES-GCM, lo que nos da una idea de que el nivel de seguridad del intercambio de claves ECDH. Los algoritmos asimétricos como RSA o DSA también están basados en logaritmos discretos.

Ahora bien, los problemas de este tipo, si bien se consideran difíciles de resolver con computadoras clásicas, pueden ser vulnerados usando el algoritmo de Shor. De esta forma una computadora cuántica podría romper la mayoría de los algoritmos asimétricos y de intercambio de claves actuales. Por ejemplo, una computadora cuántica de 2300 qubits lógicos podría usar el algoritmo de Shor para romper RSA de 1024 bits en aproximadamente un día \citep{napQuantumComputingProgress2019}.%Debido a ésto es que el NIST (National Institute of Standards and Technology - Instituto Nacional de Estándares y Tecnología) en 2016 inició un proceso de selección y estandarización de protocolos que reemplacen este tipo de algoritmos vulnerables, proceso que se estima duraría de seis a ocho años. El proceso estandarización del NIST se trata en la Sección \ref{section:estandarizacion}.

\subsection{Cifrado simétrico}
\label{section:cifradosimetrico}

Una vez que ambos extremos establecieron su clave secreta en TLS, implementan cifrado simétrico para asegurar la privacidad de la comunicación. Uno de los algoritmos criptográficos más utilizados es el Estándar de Cifrado Avanzado (AES, por sus siglas en inglés) \citep{daemenAESProposalRijndael1999}, y en general se hace uso del modo GCM. AES-GCM soporta claves de 128, 192 y 256 bits. Considerando que en los protocolos estándares en Internet los primeros bytes de los mensajes cifrados suelen ser constantes, un atacante que intercepte un mensaje cifrado con AES-GCM de $n$ bits debería probar, por medio de fuerza bruta, las $2^n$ claves posibles para descifrar el mensaje. La búsqueda terminará cuando el atacante logre visualizar los primeros bytes conocidos del mensaje, y con ello ya podrá acceder al resto del texto plano original. Para el caso de claves de 192 o 256 bits se han descubierto ataques que permiten romper el algoritmo en $2^{176}$ y $2^{119}$ ciclos respectivamente \citep{biryukovKeyRecoveryAttacks2010}. Aunque estos ataques resultan más rápidos que una búsqueda exhaustiva, igualmente son impracticables y no presentan una amenaza real al algoritmo.

\begin{comment}

Una computadora promedio procesando AES logra realizar $10^{18}$ intentos por segundo, por lo que el cálculo le tomaría aproximadamente $10^{13}$ años de procesamiento. Por esta razón frecuentemente se utiliza AES-GCM de 128 bits, un algoritmo que se considera seguro en la actualidad. El uso de claves mayores de 192 o 256 bits se emplean en aplicaciones que requieren alta seguridad o en las que el usuario desconfía de si el atacante conoce alguna debilidad del algoritmo que le permitiría procesar el ataque en menor tiempo.
\end{comment}

El algoritmo de Grover puede identificar la clave secreta de AES-GCM de 128 bits en un tiempo proporcional a $2^{128/2} = 2^{64}$ corriendo en una computadora cuántica de alrededor de 3000 qubits lógicos \citep{napQuantumComputingProgress2019}. Resulta difícil calcular el tiempo real que le tomaría a una computadora cuántica ya que cada paso del algoritmo de Grover debe descomponerse en primitivas simples. Teniendo en cuenta esto, \citep{napQuantumComputingProgress2019} estima que un cluster de computadoras cuánticas podría romper AES-GCM de 128b en un mes. Incluso en el momento en que una computadora cuántica pueda romper AES-GCM en un tiempo relativamente corto, incrementar el tamaño de la clave haría muy difícil encontrar una solución usando Grover. De esta forma, AES-GCM puede ser muy fácilmente asegurado contra el criptoanálisis cuántico.

No obstante, AES-GCM está diseñado para resistir ataques sofisticados ejecutados por computadoras clásicas, como criptoanálisis lineal y diferencial, pero no está diseñado para ataques cuánticos sofisticados. Es decir, es posible que exista un algoritmo desconocido que permita encontrar una solución a AES-GCM en una computadora cuántica de manera más eficiente que Grover. En este escenario, incrementar el tamaño de la clave no asegura que AES-GCM se mantenga seguro.


\begin{comment}
Aquí surge una pregunta: ?`cuánto tiempo le toma a una computadora cuántica correr $2^{64}$ pasos del algoritmo de Grover para romper AES-GCM? 

Esto es muy difícil de calcular ya que depende de cuánto tiempo le lleve a una computadora cuántica ejecutar un paso del algoritmo de Grover. Cada paso debe ser descompuesto en un número de operaciones primitivas a ser implementadas de manera reversible. La construcción real de un circuito cuántico para cada paso de Grover puede incrementar sustancialmente el número de qubits y tiempos de coherencia requeridos para una implementación física.

Usando el hardware clásico, se podría construir un circuito de propósito especial que pruebe $10^9$ claves por segundo. Asumiendo que una computadora cuántica puede operar a la misma velocidad, necesitaría cerca de 600 años para correr el algoritmo para la cantidad necesaria de $2^{64}$ pasos de Grover. Entonces se requeriría un enorme cluster de computadoras cuánticas para romper una clave de 128 bits en un mes. De hecho, esto es una medición estimada muy optimista porque este tipo de computadoras cuánticas requieren qubits lógicos que necesitan más operaciones de qubits físicos para completarse.

\end{comment}

\subsection{Firma digital y Certificados}

Las firmas digitales son un mecanismo criptográfico utilizados para verificar la integridad y autenticidad de datos. Una CA (Certification Authority - Autoridad Certificante) firma un certificado digital para un individuo u organización, de modo que quien necesite verificar la identidad de dicha entidad pueda hacerlo en la medida en que confíe en la CA. Este es el uso que le da TLS a las firmas digitales. Los dos esquemas de firma digital más utilizados son RSA y ECDSA. La seguridad de RSA se basa en problemas de factorización, y la de ECDSA en logaritmos discretos, ambas técnicas vulnerables al algoritmo de Shor. Un atacante que tenga una computadora cuántica podría utilizar el algoritmo de Shor para romper las firmas RSA y ECDSA. El atacante podría falsificar certificados digitales, y con ello suplantar la identidad de una de las partes en una comunicación.

\subsection{Funciones resumen o hash}

Algunas primitivas adicionales a tener en cuenta son las funciones hash o resumen. La función hash genera cadenas de tamaño fijo utilizando como entrada un contenido arbitrario. La seguridad de una función hash se basa en cálculos matemáticos unidireccionales, imposible de revertir. Además, una función hash debe ser resistente a las colisiones, es decir, debería ser muy difícil (si no imposible) encontrar dos mensajes de entrada que generen la misma cadena resumen de salida.

En principio es esperable que una función hash SHA256 no sea vulnerable al criptoanálisis cuántico. Se supone que es imposible romper un hash de ese tamaño en una computadora cuántica de aproximadamente 2400 qubits lógicos corriendo el algoritmo de Grover \citep{napQuantumComputingProgress2019}.

\citep{napQuantumComputingProgress2019} menciona también que el algoritmo de Grover también puede utilizarse para realizar ataques de fuerza bruta a cadenas hash que representan contraseñas. Si bien este tipo de ataques no explotan una vulnerabilidad del algoritmo hash en si mismo, sí podrían romper la seguridad de un sistema al encontrar, mediante búsqueda exhaustiva, una cadena de texto que de como resultado un hash determinado. No obstante, los mecanismos de corrección de errores necesarios para implementar Grover extenderían el tiempo del ataque a cifras poco prácticas.

\begin{comment}
Por otro lado, las funciones hash se suelen utilizar para almacenar contraseñas en bases de datos. La debilidad de este mecanismo es que, en promedio, las contraseñas de usuario suelen ser cortas, por lo que son susceptibles a un ataque de fuerza bruta. A un cluster de computadoras clásicas le resultaría difícil, aunque no imposible, realizar un ataque de fuerza bruta sobre un espacio de contraseñas de 10 caracteres. Una computadora cuántica corriendo el algoritmo de Grover podría reducir este tiempo a unos pocos segundos. No obstante, los mecanismos de corrección de errores necesarios para implementar Grover extenderían este tiempo a valores impracticables \citep{napQuantumComputingProgress2019}.

Mejorando esta corrección de errores el algoritmo de Grover sería una gran amenaza a los sistemas de contraseña, por lo que se requeriría encontrar alternativas para este tipo de autenticación. Una buena opción sería incorporar mecanismos biométricos, valores criptográficos de un solo uso, tockens de autenticación de hardware, etc.	

Otro uso común de los hashes son las llamadas pruebas de trabajo, muy comunes en el procesamiento de criptomonedas, y el algoritmo de Grover podría ser adecuado para resolver los desafíos matemáticos realizados por los mineros. No obstante, la sobrecarga por corrección de errores requerida por Grover para resolver esta prueba de trabajo se estima que demoraría más tiempo que el utilizado, por ejemplo, por Bitcoin, para validar los bloques de transacciones, lo que imposibilitaría a Grover romper el ecosistema de esta criptomoneda. Si este tiempo de procesamiento se reduce, o si se logra construir una computadora cuántica tolerante a fallos, Grover podría ser una amenaza para las cadenas de bloques, por lo que Bitcoin quizás necesitaría migrar a un sistema de firma digital post cuántico.


	Definicion
	Entropia, cantidad de informacion
	curvas elipticas
	numeros aleatorios
	algoritmos criptograficos (nociones)
	funciones hash
	Criptografia simetrica, cifrado por bloques y de flujo
	Criptografia asimetrica
	KEM/DEM technique for hybrid - encryption http://cryptowiki.net/index.php?title=KEM/DEM_technique_for_hybrid_encryption
	Lucena
	
\end{comment}