\cleardoublepage

\chapter*{RESUMEN}
\addcontentsline{toc}{chapter}{RESUMEN}

Actualmente la mayoría de los protocolos de aplicación en Internet delegan su seguridad a la capa TLS por medio de implementaciones de software tales como OpenSSL y wolfSSL. Esta capa genera un canal seguro entre cliente y servidor para ofrecer privacidad, autenticidad e integridad a los datos que transitan la red. TLS utiliza criptografía asimétrica para lograr la autenticación e intercambio de las claves. Estos métodos se basan en supuestos de complejidad computacional, por lo que hacen uso de funciones matemáticas muy simples de calcular, pero extremadamente difíciles de revertir haciendo uso de los procesadores actuales. No obstante, estos mecanismos se consideran vulnerables al criptoanálisis realizado desde computadoras cuánticas. Aún así, existen algoritmos asimétricos que se consideran resistentes a este tipo de ataque: los algoritmos post-cuánticos.

Este trabajo estudió el estado del arte en el campo de la criptografía post-cuántica, mencionó los avance en el proceso de estandarización y los algoritmos candidatos más prometedores en la actualidad. Además, analizó las implementaciones de software y librerías de código abierto que han alcanzado mayor nivel de madurez, y que a su vez cuentan con un desarrollo activo. Se detallaron dos implementaciones de TLS: wolfSSL y las variantes de OpenSSL provistas por el proyecto Open Quantum Safe, OQS-OpenSSL; y se realizaron experimentos de integración de estas implementaciones con HTTP y OpenVPN como protocolos de capa de Aplicación.

A pesar de los avances que este campo ha tenido en los últimos años, esta tesis da cuenta del nivel experimental de las implementaciones de software disponibles y de cómo, tanto éstas como el desarrollo de nuevos estándares, se encuentran aún en etapas tempranas de su evolución.


\chapter{INTRODUCCIÓN}
\label{section:intro}

\section{PLANTEAMIENTO DEL PROBLEMA}

Las comunicaciones en Internet se han vuelto indispensables tanto para la vida de las personas como para el funcionamiento de las sociedades y organizaciones. Su uso se ha incrementado sustancialmente la última década debido a las posibilidades que brindan los avances tecnológicos en computadoras, teléfonos inteligentes, y más aún con el surgimiento de paradigmas como el Internet de las cosas o la computación de nube. Esta gran interconexión habilita a usuarios, empresas y gobiernos a hacer uso de la red de redes para acceder y brindar servicios en línea a través de aplicaciones móviles, sistemas web, entre otros \citep{owidinternet}. Esta tendencia se vio incrementada desde el 2020 por la pandemia del coronavirus COVID-19 \citep{clement2020coronavirus}.

Consecuentemente la seguridad en las transacciones en Internet está tomando especial relevancia, particularmente en operaciones comerciales, el acceso a la banca electrónica, el uso de billeteras virtuales, el uso de sistemas de mensajería y correo electrónico, y servicios que requieran de autenticación. La seguridad en estas comunicaciones permite garantizar la confidencialidad, integridad y autenticidad de los datos del usuario en su viaje entre los nodos que conforman el enlace. Estos requisitos se logran generalmente mediante el uso de mecanismos criptográficos tales como las herramientas de cifrado, funciones de hashing o resumen, y firmas digitales \citep{stallingsCryptographyNetworkSecurity2005}.

La suite de protocolos TCP/IP en la que está basada Internet \citep{comer2014internetworking} divide la funcionalidad de las comunicaciones en capas. TCP/IP provee las especificaciones de la comunicación de datos de extremo a extremo, establece cómo deben empaquetarse, direccionarse, transmitirse, enrutarse y recibirse los datos. Las capas del modelo TCP/IP, también conocida como Arquitectura TCP/IP o Arquitectura de Internet, son cuatro \citep{peterson2007computer}, a saber:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Aplicación: administra el intercambio de datos entre procesos.
	\li Transporte: administra la comunicación entre los procesos.
	\li Red: provee la comunicación entre redes independientes.
	\li Enlace: brinda los mecanismos para la transmisión y recepción de datos en un segmento simple de red.
\end{itemizenospace}

Cada capa opera según reglas definidas por protocolos, y las computadoras en la red hacen uso de implementaciones de software de estos protocolos para gestionar las comunicaciones. Los protocolos de Capa de Aplicación más utilizados no implementan mecanismos de seguridad en sí mismos, sino que delegan estas funcionalidades a una subcapa denominada Capa de Conexión Segura (SSL, por sus siglas en inglés) \citep{freierSecureSocketsLayer2011} o Seguridad en Capa de Transporte (TLS, por sus siglas en inglés) \citep{dierksTransportLayerSecurity2008}. Así, por ejemplo, un protocolo común como HTTP \citep{fieldingHypertextTransferProtocol2014} combinado con la capa de seguridad SSL/TLS conforma el protocolo seguro HTTPS (HTTP over/sobre SSL/TLS) \citep{rescorla2000http}. Lo mismo ocurre con otros protocolos de Aplicación de uso común como SMTP \citep{klensinSimpleMailTransfer2001}, IMAP \citep{crispinINTERNETMESSAGEACCESS2003}, OpenVPN \citep{openvpnproject/openvpninc.OpenVPNOpenSource2018a} o FTP \citep{postel1985rfc} . 

Debido al incremento de adopción del protocolo HTTPS como alternativa a HTTP, buscadores como Google comenzaron a tener en cuenta el su uso en los algoritmos de posicionamiento \citep{bahajjiHTTPSRankingSignal2014a}, y navegadores como Chrome o Firefox comenzaron a advertir a los usuarios cuando los sitios accedidos no fueran seguros \citep{cimpanuFirefoxPreparesMark2017} \citep{schechter_secure_2018}. Incluso Firefox permite bloquear el acceso a sitios web que no provean seguridad en HTTP mediante el modo \qq{HTTPS-only} (Sólo-HTTPS) disponible desde la versión 83 \citep{kerschbaumer2021https}.

SSL/TLS le brinda a HTTP algoritmos de autenticación basados en certificados digitales X.509 \citep{itu-t509InformationTechnology2019} y criptografía asimétrica \citep{lucenalopezCriptografiaSeguridadComputadores2011}. Este tipo de algoritmos se basan generalmente en funciones matemáticas que hacen uso de supuestos de complejidad computacional, es decir, problemas matemáticos muy simples de calcular, pero extremadamente difíciles de revertir. Así la generación de claves asimétricas, por ejemplo, puede llevarse a cabo multiplicando dos números primos grandes. El producto de estos dos valores es relativamente sencillo de computar para un procesador actual, pero encontrar estos dos números primos que dieron origen al producto resulta una tarea sumamente difícil aplicando algoritmos de factorización y de fuerza bruta que hagan uso de las capacidades de cálculo disponibles hoy en día \citep{bernsteinPostquantumCryptographydealingFallout2017}. Por otro lado, una computadora cuántica, gracias a las características intrínsecas de su arquitectura, podría hallar estas claves de cifrado de manera más eficiente que un procesador clásico, vulnerando así la seguridad brindada por los algoritmos asimétricos tradicionales \citep{bernsteinPostQuantumCryptography2009}.

Una computadora cuántica podría encontrar claves de cifrado utilizando algoritmos cuánticos especialmente diseñados para este tipo de procesadores. Uno de los algoritmos cuánticos con mayor relevancia en el campo de la criptografía es el algoritmo de Shor \citep{shorPolynomialTimeAlgorithmsPrime1997}. Se lo considera el primer algoritmo cuántico no trivial (es decir, que plantea una solución compleja y no obvia) que demostró un potencial de crecimiento exponencial de velocidad sobre los mecanismos clásicos \citep{chu2016beginning}. Este algoritmo permite obtener el resultado de manera estocástica y con un determinado grado de acierto según la cantidad de iteraciones a la que se lo someta.

El algoritmo de Shor logra encontrar una clave RSA \citep{rivestMethodObtainingDigital1987} descomponiéndola en factores en un tiempo $O((\log N)^3)$, siendo $N$ el numero primo que representa la clave pública. Por su parte, los algoritmos clásicos no pueden realizar dicha factorización en un tiempo menor a $O((\log N)k)$ para ningún $k$, por lo que RSA sigue siendo considerado, en la actualidad, un algoritmo seguro \citep{bernsteinPostQuantumCryptography2009,weissteinRSA640Factored2005}. Isaac Chuang en diciembre de 2001, liderando un grupo de trabajo de computación cuántica en IBM, logró factorizar el número 15 mediante una computadora cuántica de 7 qubits \citep{vandersypen2001experimental}, y en marzo de 2016 un grupo de investigadores del MIT, entre los que se encontraba el mismo Chuang, pudo factorizar el numero 15 con un 99\% de certeza en una computadora cuántica de 5 qubits, cada uno representado por un átomo individual \citep{chu2016beginning}. De aquí se deduce que la aparición de las primeras computadoras cuánticas de cientos de qubits con una reducción aceptable del ruido cuántico dejarían obsoletos a la mayoría de los algoritmos asimétricos actuales \citep{bernsteinPostQuantumCryptography2009, etsiQuantumSafeCryptography2015, takagiPostQuantumCryptography2016, perlnerQuantumResistantPublic2009}.

Para resolver este potencial problema se han diseñado algoritmos asimétricos resistentes al criptoanálisis cuántico, conocidos como algoritmos post-cuánticos \citep{bernsteinPostquantumCryptographydealingFallout2017}. Estos algoritmos pueden ejecutarse en computadoras tradicionales y ser desplegados en las redes de datos actuales, facilitando de esta forma su despliegue. Esto les da una gran ventaja sobre otros mecanismos de intercambio de claves tales como la criptografía cuántica, que requieren montar una nueva infraestructura de comunicaciones \citep{gisinQuantumCryptography2002}.

Existen varias implementaciones de software de SSL/TLS, sin embargo ninguna soporta algoritmos post-cuánticos de manera nativa. Las implementaciones de código abierto más utilizadas son OpenSSL \citep{opensslsoftwarefoundationOpenSSLCryptographySSL2018a} y wolfSSL \citep{wolfsslWolfSSLEmbeddedSSL2020}, una alternativa centrada en la eficiencia para su ejecución en sistemas embebidos y dispositivos IoT. Aunque OpenSSL no incluye algoritmos post-cuánticos de manera nativa, el proyecto Open Quantum Safe \citep{stebilaPostQuantumKeyExchange2017} ha generado un \textit{fork} que soporta una gran variedad de algoritmos asimétricos y de intercambio de claves post-cuánticos integrando OpenSSL mediante una biblioteca de código denominada \textit{liboqs} \citep{openquantumsafeLiboqs2020}. Esta librería ha sido usada por proyectos de terceros como la VPN experimental de Microsoft, PQCrypto-VPN \citep{microsoftWelcomePQCryptoVPNProject2018a}, o el cliente beta de VPN de Mullvad \citep{amagicom_ab_introducing_2017}, que utiliza intercambio de claves post-cuántico. Por otro lado, wolfSSL recientemente incorporó en su implementación NTRU, un algoritmo asimétrico post-cuántico que provee una seguridad similar a la de RSA, pero haciendo uso de claves más cortas y con un mejor rendimiento \citep{hoffstein1998ntru}.

En esta tesis se estudiaron wolfSSL y dos versiones de OQS-OpenSSL. En todos los casos se realizaron experimentos que permitieron verificar el uso de cipher suites (conjunto de mecanismos criptográficos que dan seguridad a la conexión) y algoritmos de intercambio de claves resistentes al criptoanálisis cuántico durante negociación TLS. En cuanto a OQS-OpenSSL se analizó una integración con dos implementaciones de servicio HTTP, Apache \citep{theapachesoftwarefoundationApacheHttpServer2018a} y NGINX \citep{nginxinc.Nginx2018a}, y luego se montó una prueba de concepto de un enlace VPN utilizando PQCrypto-VPN. Respecto a wolfSSL se llevó a cabo una adaptación básica de un servidor Apache para lograr su integración con esta implementación, y se logró la negociación del algoritmo NTRU (N-Th Degree TRUncated Polynomial Ring, Anillo Polinomial Truncado de N-ésimo grado) \citep{baktu_ntru_2017} en el establecimiento de una conexión HTTPS. Además, se logró montar una prueba de concepto de la integración de wolfSSL con OpenVPN aunque, en este último caso, sin la incorporación de criptografía post-cuántica.


\begin{comment}
Más allá de que la criptografía resulte transparente para la gran mayoría de los usuarios de Internet, es fundamental para mantener la confidencialidad y la autenticidad de todas las interacciones en la red, especialmente para la World Wide Web. Un ejemplo muy sencillo es el protocolo \textbf{https}, un protocolo web que cifra la información transmitida entre un cliente y un servidor que ofrece un sitio web. Esta información incluye datos sensibles que van desde nombres de usuario y contraseña de acceso a una plataforma como una red social o una interfaz de \textit{home banking}, hasta números de tarjetas de crédito. Otro uso interesante es el almacenamiento cifrado de contraseñas de servicios, ya sea en un ordenador personal como en un servidor. Estas contraseñas deben almacenarse localmente en cada caso, y deben hacerlo de manera que no sea visible a un supuesto atacante que quiera hacerse con la información.

Por otro lado, actualmente las mayores empresas de Internet, como Google, Microsoft, o Amazon, por no mencionar más, tienen relativa simplicidad para implementar nuevos mecanismos criptográficos en sus comunicaciones. Por ejemplo, Si a Google se le ocurre implementar un nuevo algoritmo, sólo tiene que montarlo en sus servidores, y actualizar su cliente Chrome. La implementación será completa cuando un gran porcentaje de los navegadores de los clientes reciban la actualización. Por el contrario, resulta sumamente difícil remover un algoritmo una vez que ha sido implementado en Internet con gran adopción. Un ejemplo de esto es el algoritmo de hash MD5, que en su momento era seguro, pero con los avances tecnológicos se encontró vulnerable en 2005, y ha tomado literalmente casi una década ser eliminado de muchos sistemas de amplio uso, por ejemplo, Microsoft publicó recién en 2014 un parche de seguridad que desactivaba MD5 en el programa de certificados raíz \citep{microsoft_microsoft_2014}.
\end{comment}

\section{OBJETIVOS}

El presente trabajo de investigación se centra en el análisis de algunas implementaciones de SSL/TLS que incorporan algoritmos criptográficos post-cuánticos en la negociación cliente-servidor, y que a su vez se encuentran en un estado relativamente activo de desarrollo.

Como objetivo general el trabajo plantea implementar pruebas de concepto de algoritmos criptográficos post-cuánticos en protocolos HTTP y VPN. 

Los objetivos específicos que se desprenden de este son los siguientes.

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Realizar un análisis minucioso de las implementaciones de SSL/TLS post cuánticas que se encuentran en un estado de desarrollo más avanzado y activo, a saber, OQS-OpenSSL y wolfSSL, con la intención de determinar su nivel de estabilidad, bugs y vulnerabilidades, respecto de la última release de OpenSSL.
	\li Montar las pruebas de concepto de servicio HTTPS con cifrado post-cuántico utilizando Apache y/o Nginx integrado con las implementaciones OpenSSL antes mencionadas.
	\li Montar prueba de concepto de servicio de VPN en capa 7 basado en OpenVPN con cifrado post-cuántico mediante la combinación con las suites OpenSSL antes mencionadas.
\end{itemizenospace}


\section{ORGANIZACIÓN DEL TRABAJO}

La presente tesis se divide en tres partes: Marco teórico, Desarrollo y Conclusiones. Se incluye un capítulo inicial, Introducción, que detalla el planteamiento del problema que aborda la tesis, especifica el objetivo general y los específicos, y describe la organización del trabajo.

La primera parte, MARCO TEÓRICO, aborda la teoría necesaria para comprender el desarrollo del trabajo. Esta parte se divide en 4 capítulos:

\begin{itemizenospace}
\setlength\itemsep{0em}
\li El Capítulo \ref{section:criptografiaaplicada} \qq{\nameref{section:criptografiaaplicada}} introduce los conceptos básicos del uso de la criptografía en redes TCP/IP, y especialmente los detalles del protocolo TLS.
\li El Capítulo \ref{section:ssltls} \qq{\nameref{section:ssltls}} realiza una introducción a SSL/TLS y a la cipher suite QSH (Quantum Safe Hybrid).
\li El Capítulo \ref{section:quantumcomputing} \qq{\nameref{section:quantumcomputing}} trata sobre la amenaza que representa la computación cuántica a los algoritmos asimétricos actuales.
%\li El Capítulo \ref{section:quantumcrypto} \qq{\nameref{section:quantumcrypto}} realiza una introducción breve a la criptografía cuántica como una alternativa al intercambio de claves basado en Diffie-Hellman.
\li El Capítulo \ref{section:pqcrypto} \qq{\nameref{section:pqcrypto}} presenta los detalles de la criptografía post-cuántica utilizada en el desarrollo.
\li El capítulo \ref{section:implementaciones} \qq{\nameref{section:implementaciones}} introduce el proyecto Open Quantum Safe, la librería liboqs, algoritmos soportados, y finalmente reseña el proyecto wolfSSL.
\end{itemizenospace}

La segunda parte, DESARROLLO, detalla los análisis y experimentos prácticos realizados. Esta parte se divide en 4 capítulos:

\begin{itemizenospace}
\li El capítulo \ref{section:oqs_pruebas} \qq{\nameref{section:oqs_pruebas}} realiza un análisis de las características de OQS-OpenSSL en sus versiones 1.0.2 y 1.1.1.
\li El capítulo \ref{section:oqs_experiment} \qq{\nameref{section:oqs_experiment}} detalla los experimentos realizados con OQS-OpenSSL v1.0.2
y v1.1.1, y su integración con Apache, Nginx y OpenVPN.
\li El capítulo \ref{section:wolfssl_pruebas} \qq{\nameref{section:wolfssl_pruebas}} realiza el análisis y experimentación de wolfSSL, NTRU, Apache y OpenVPN.
\li El capítulo \ref{section:otrasimplementaciones} \qq{\nameref{section:otrasimplementaciones}} reseña otras implementaciones de software de interés no incluidas en el presente trabajo, y reseña los trabajos relacionados con este proyecto.
\end{itemizenospace}

Finalmente, la tercera parte, CONCLUSIONES, expone las conclusiones obtenidas del desarrollo, y algunas líneas de investigación que podrían desprenderse de esta tesis.%, y breves comentarios sobre las perspectivas a corto y mediano plazo en la migración a criptografía post-cuántica en Internet.
