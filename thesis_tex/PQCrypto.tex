\cleardoublepage
\chapter{CRIPTOGRAFÍA POST-CUÁNTICA}
\label{section:pqcrypto}

En este capítulo se partirá de las motivaciones que dan origen a la criptografía post-cuántica, y se detallarán algunas características de sus principales criptosistemas. Se mencionarán también algunos desafíos que tiene por delante la criptografía post-cuántica, y sus diferencias con su par cuántico. Finalmente se explicará brevemente el proceso de estandarización y los niveles de seguridad de los algoritmos, ya las implementaciones utilizadas para las pruebas hacen referencia a los mismos.

La seguridad de los algoritmos post-cuánticos descansa en problemas matemáticos que se cree que no pueden ser resueltos por computadoras cuánticas. Igualmente, y como en cualquier rama de la criptografía, debe tenerse en cuenta que la seguridad de estos algoritmos no puede establecerse contra técnicas criptoanalíticas hasta hoy desconocidas \citep{napQuantumComputingProgress2019}.

Como se vio en la Sección \ref{section:algoritmoscuanticosyseguridad}, la seguridad post-cuántica en los algoritmos de cifrado simétrico y de hash puede obtenerse aumentando el tamaño de la clave de cifrado o la salida de la función resumen respectivamente. En primera instancia algoritmos de cifrado como AES-GCM de 256 bits y hashes como SHA256 serían resistentes al criptoanálisis cuántico.

Por su parte, los algoritmos de cifrado asimétrico, de firma digital y los protocolos de intercambio de claves se consideran vulnerables a ataques cuánticos. Así, algoritmos como RSA, ECDH o ECDSA deberán ser actualizados a nuevos esquemas de cifrado que incorporen mecanismos post-cuánticos, o criptosistemas híbridos.

\begin{comment}
	El NIST ya ha iniciado un proyecto de estandarización en criptografía post-cuántica para facilitar a la comunidad científica remitir propuestas y evaluarlas \citep{nist_post-quantum_2020}. Más adelante en la Sección \ref{section:estandarizacion} se especifican algunos detalles adicionales de este proceso de estandarización.

\end{comment}
No hay duda de que el cifrado de clave pública es necesario para las comunicaciones en Internet, así como también es un hecho que estos criptosistemas pueden volverse inseguros ante el criptoanálisis cuántico \citep{schmidtPostQuantumCryptography2010}. Si bien una computadora cuántica puede romper varios algoritmos actuales en un tiempo que resulte útil al atacante, esto no implica que la criptografía de clave pública se vuelva obsoleta.

\section{CANDIDATOS POST-CUÁNTICOS}

Existen varios sistemas criptográficos de clave pública resistentes a ataques cuánticos, candidatos prometedores que resultan de interés en el presente trabajo. Entre ellos se encuentran:

\begin{itemizenospace}
	\setlength\itemsep{0em}
	\li Criptografía basada en hash
	\li Criptografía basada en código
	\li Criptografía basada en retículos
	\li Criptografía basada en ecuaciones cuadráticas multivariadas
	\li Criptografía basada en isogenias supersingulares
	\li Criptografía de clave secreta
\end{itemizenospace}

\subsection{Criptografía basada en hash}

Los sistemas de firma digital basados en funciones hash que datan de los años '80 se suponen seguros si se utilizan funciones hash consideradas invulnerables al ataque cuántico. El mayor problema de estos esquemas es que generan firmas de una longitud relativa grande, y por ello pueden ser utilizadas sólo en ciertos ambientes, como la firma de paquetes de software en gestores de aplicaciones.

\begin{comment}
	Un ejemplo serían las aplicaciones de actualización de paquetes en algunos sistemas operativos. Debido a que los paquetes de software suelen ser relativamente grandes, agregar una firma digital grande redunda en un pequeño incremento en el volumen del paquete. Esto permitiría a los desarrolladores y fabricantes migrar sus firmas digitales actuales, basadas en RSA y en ECDSA, a firmas basadas en algoritmos post-cuánticos de hash.

\end{comment}
Un ejemplo representativo es el sistema de firma de clave pública de Merkle (1979) construido sobre la idea de firma de un mensaje de Lamport y Diffie \citep{becker2008merkle}. Otra propuesta interesante es el esquema de firma LMSS \\
 (Leighton-Micali Signature Scheme, Esquema de Firma de Leighton-Micali) \citep{mcgrewRFC8554LeightonMicali2019}.

\subsection{Criptografía basada en código}

La teoría de códigos es la ciencia que estudia los esquemas de codificación que permiten a dos partes comunicarse sobre un canal ruidoso. El emisor codifica un mensaje de modo que el receptor puede decodificarlo incluso si el canal presenta un ruido acotado. Los investigadores en esta rama se encuentran estudiando algunos esquemas de codificación que resultan difíciles de decodificar. De hecho, para ciertos esquemas el mejor algoritmo para decodificar requiere un tiempo exponencial en computadoras clásicas. Además, el problema de la decodificación parece ser difícil incluso para computadoras cuánticas, de modo que los criptógrafos están empleando este tipo de codificación para construir criptosistemas post-cuánticos.

Un ejemplo típico es el sistema de cifrado asimétrico de código Goppa oculto de McEliece \citep{mceliece1978public, bernsteinPostquantumCryptographydealingFallout2017} que puede ser usado para realizar intercambio de claves post-cuántico. Recientemente surgieron algunas variantes prometedoras como CAKE \citep{barreto2017cake}.

\subsection{Criptografía basada en retículos}

Un retículo o \textit{lattice} es un conjunto parcialmente ordenado en el que, para cada par de elementos, existen un supremo y un ínfimo. Si se tiene un conjunto parcialmente ordenado $L$, se lo denomina retículo si tiene un supremo por pares, y tiene un ínfimo por pares, que también pertenece a $L$.

Uno de los problemas computacionales más conocidos en este aspecto es el de encontrar el vector más corto en una grilla. Todos los algoritmos clásicos actuales que resuelven este problema lo hacen en un tiempo exponencial respecto de la dimensión de la grilla, y existen evidencias de que también tomaría un tiempo exponencial en computación cuántica \citep{regev2009lattices}. Se han presentado varias propuestas en esta categoría, y si se verificara que una computadora cuántica no puede resolver de manera sencilla estos problemas, los algoritmos basados en retículos serían seguros.

El ejemplo que más relevancia tiene es el sistema de cifrado de clave pública Hoffstein-Pipher-Silverman \qq{NTRU} \citep{hoffstein1998ntru}. Otros candidatos interesantes son New Hope \citep{alkim_post-quantum_2016} y Frodo \citep{bosFrodoTakeRing2016}. Tanto Frodo como New Hope hacen uso de problemas computacionales de Aprendizaje con errores sobre anillos, o RLWE (Ring Learning with Errors, Anillo de Aprendizaje con Errores). Se consideran algoritmos post-cuánticos porque se presume que los problemas RLWE son difíciles de resolver, incluso para computadoras cuánticas \citep{cryptoeprint:2012:230}.

Google ha estado experimentando con New Hope en su navegador Chrome \citep{braithwaite_experimenting_nodate}. Uno de los resultados obtenidos arrojó que el incremento de tiempo en que se incurre al utilizar New Hope es despreciable y no resulta un impedimento para su implementación práctica.

Por otro lado NTRU es un criptosistema que consiste en dos algoritmos: NTRUEncrypt y NTRUSign para cifrado/descifrado y firma digital respectivamente. En 2017 se liberó bajo dominio público y puede ser utilizado en aplicaciones libres bajo licencia GPL. NTRU puede ejecutar operaciones con un nivel de seguridad similar al de RSA, pero con mayor eficiencia \citep{baktu_ntru_2017}. Esto es debido a que el tiempo que demora RSA en realizar las operaciones se incrementa con el cubo del tamaño de la clave, mientras que en NTRU el incremento de tiempo es cuadrático. Se han realizado experimentos con una GPU GTX280 y se pudo procesar NTRU con un nivel de seguridad de 256 bits a una tasa sólo 20 veces más lenta que la de un cifrador simétrico AES \citep{hutchison_speed_2010}. A diferencia de RSA y ECDSA, NTRU es resistente a las técnicas criptoanalíticas cuánticas conocidas. \citep{perlnerQuantumResistantPublic2009,stehle_making_2013} menciona a NTRU como una alternativa para cifrado y firma digital resistentes al algoritmo de Shor, y hace alusión a que, de todos los algoritmos criptográficos basados en el esquema de retículo, NTRU parece ser el más práctico, incluso más que versiones alternativas como Stehle-Steinfeld NTRU.

\subsection{Criptografía multivariable}

La criptografía multivariable o basada en ecuaciones cuadráticas multivariable es un término genérico para funciones de criptografía asimétrica basada en ecuaciones polinomiales multivariable sobre un campo finito $F$. Si estos polinomios son de grado 2 hablamos de ecuaciones cuadráticas multivariable. Los problemas matemáticos basados en este tipo de ecuaciones son de tipo NP-completos \citep{garey_computers_1979}, característica que los convierte en candidatos post-cuánticos. La criptografía multivariable ha sido muy productiva en términos de diseño y criptoanálisis, sobre todo ahora que han pasado varios años de estudio y se consideran algoritmos estables y robustos. Este tipo de mecanismos permite generar firmas digitales más cortas que las proporcionadas por otros algoritmos post-cuánticos.

Un ejemplo relevante es el sistema de firma de clave pública basadas en ecuaciones de campo oculto (HFE, por sus siglas en inglés) \citep{patarin1996hidden}.

\subsection{Criptografía basada en isogenias supersingulares}

Los gráficos de isogenia supersingular son un tipo de gráficas de expansión provenientes de la teoría de números computacionales, y se ven aplicados comúnmente en la criptografía de curva elíptica. Se considera que los algoritmos basados en isogenia supersingular son post-cuánticos.

Un candidato reciente en esta categoría es Diffie-Hellman de isogenias supersingulares (SIDH, por sus siglas en inglés) \citep{costelloEfficientCompressionSIDH2016}, un algoritmo de intercambio de claves basado en Diffie-Hellman. SIDH genera menos tráfico de red que el algoritmo de retículos New Hope, y aunque requiere más tiempo de cálculo en ambos extremos, el tiempo total del intercambio resulta menor \citep{costelloEfficientCompressionSIDH2016}. SIDH además utiliza compresión, por lo que es uno de los algoritmos que utiliza claves relativamente más pequeñas. Aunque no se conocen ataques que vulneren a SIDH, este tipo de problemas comenzó a estudiarse recientemente, y se necesita más tiempo de análisis para que pueda ganar confianza respecto de su seguridad post-cuántica.

Entre los algoritmos basados en SIDH se encuentra el mecanismo de encapsulamiento de claves SIKE \citep{jaoSIKESupersingularIsogeny2019}. SIKE ofrece los niveles de seguridad recomendados 1, 2, 3 y 5 (véase Sección \ref{section:estandarizacion}). El principal inconveniente de SIKE es que su rendimiento no es mejor que el de otros esquemas de cifrado de curva elíptica clásicos, o esquemas post-cuánticos. Igualmente se ha realizado experimentos que mejoran el rendimiento de SIKE reduciendo el tiempo total de cálculo \citep{koziel_high-performance_2018, koziel_post-quantum_2016}.

A diferencia de otros competidores bien conocidos como NTRU y RingLWE, SIDH/SIKE soporta PFS, por lo que si un atacante obtuviera la clave de sesión actual, esto no comprometerá las sesiones futuras.

\subsection{Criptografía de clave secreta}

Como se mencionó en la Sección \ref{section:cifradosimetrico} muchos algoritmos de clave simétrica actuales se supone que son resistentes a ataques cuánticos con solamente incrementar el tamaño de la clave. El ejemplo que lidera en esta categoría es el cifrador Daemen-Dijmen \qq{Rijndael}, conocido como AES.


\section{PROCESO DE ESTANDARIZACIÓN}
\label{section:estandarizacion}

Los algoritmos de cifrado post-cuántico aún no se encuentran estandarizados, y es por ello que el el Instituto Nacional de Estándares y Tecnología de los Estados Unidos, NIST \citep{nistNationalInstituteStandards2020} promueve un concurso que tiene como objetivo 
evaluar y generar estándares de cifrado post-cuántico para la industria. Para ello solicita a la comunidad criptográfica algoritmos de clave pública que se crean resistentes a ataques cuánticos con la intención de analizar alternativas \citep{nistPostQuantumCryptographyStandardization2018}.

Este concurso lanzó su primera ronda de recepción de propuestas en el 2017, la segunda en el 2019 y la tercera el 22 de julio de 2020 \citep{nistPostQuantumCryptographyStandardization2018}. El proceso de estandarización debería concluir entre el 2022 y el 2024 \citep{napQuantumComputingProgress2019}. Cada ronda finaliza con la presentación de algoritmos candidatos en las conferencias PQCrypto y Conferencia de estandarización de criptografía post-cuántica. Los algoritmos que se posicionen como candidatos serios podrán ser considerados por instituciones de estandarización como el Grupo de Trabajo de Ingeniería de Internet (IETF, por sus siglas en inglés), la Organización Internacional para la Estandarización) (ISO, por sus siglas en inglés) o la Unión Internacional de Telecomunicaciones (ITU, por sus siglas en inglés). Con estos estándares los proveedores de servicios en Internet podrán comenzar a incorporar criptografía post-cuántica en su infraestructura.

El NIST planteó una lista de niveles de seguridad a los que deben ajustarse las propuestas. Estos niveles representan la fortaleza del algoritmo a un ataque cuántico, y tienen un equivalente con algoritmos resistentes actuales. Se han definido cinco niveles para los nuevos algoritmos, siendo el nivel 1 el que comprende los algoritmos más débiles, y el 5 el que incluye a los más fuertes. La Tabla \ref{table:nist_seclevel} muestra la equivalencia entre estos niveles y la resistencia cuántica de los algoritmos actuales considerados.


\begin{table}[htp]
	\centering
	\caption{Niveles de seguridad del NIST y equivalencias.}
	\begin{tabular}{|c|l|l|}
		\hline
%		\textbf{Nivel de Seguridad} & \textbf{Equivalencia} & \textbf{Resistencia Cuántica} \\
		\thead{Nivel de Seguridad} & \thead{Equivalencia} & \thead{Resistencia Cuántica} \\
		\hline
		\textbf{L1} & AES-128 & Débil	\\
		\hline
		\textbf{L2} & SHA-256/SHA3-256 & Fuerte \\
		\hline
		\textbf{L3} & AES-192 & Más fuerte \\
		\hline
		\textbf{L4} & SHA-384/SHA3-384 & Muy fuerte \\
		\hline
		\textbf{L5} & AES-256 & El más fuerte\\
		\hline
	\end{tabular}
	\label{table:nist_seclevel}
\end{table}

Si bien el NIST tiene en cuenta que estas cinco categorías son resistentes a ataques cuánticos, hace algunas aclaraciones. Los algoritmos de nivel L1 se consideran probablemente seguros a menos que las computadoras cuánticas mejoren su rendimiento más rápido de lo que se prevé. Es por ello que muchos expertos no consideran a los algoritmos L1 verdaderamente seguros a largo plazo. El NIST igualmente los publica como alternativas aceptables al momento de liberar estándares, los califica como un paso intermedio a esquemas más seguros en el mediano plazo.

\begin{comment}
	Esto igual es difícil debido a que el uso de Grover en estas computadoras no puede romper el algoritmo AES-128, pero esto no implica que en poco tiempo se descubran nuevos ataques que sí puedan hacerlo \citep{furrerRogerGrimesCryptography2020}.
\end{comment}

Los niveles L2 y L3 se consideran probablemente seguros de forma previsible, y los niveles L4 y L5 se consideran muy seguros. Los algoritmos L4 y L5 pueden ser difíciles de llevar a la práctica por su arquitectura e implementación, y además, en general, no tiene buen rendimiento. %Por ejemplo, algunas aplicaciones actuales que hacen uso de AES-128 de forma predeterminada no pueden usar AES-256 o un algoritmo similar con claves más grandes.

La mayoría de los investigadores que presentan sus propuestas en general lo hacen para los niveles L1, L3 y L5, aunque hay excepciones \citep{nteu_prime_ntru_2020, hamburg_post-quantum_2019, ducas_crystals-dilithium_2018, chen_mqdss_2020, falcon_falcon_2020}.

%Existen igual algunas excepciones como NTRU Prime, que no presentó para L1 y L5 \citep{nteu_prime_ntru_2020}, o ThreeBears que no lo hizo para L1 y L3 \citep{hamburg_post-quantum_2019}. Por su parte, CRYSTALS-Dilithium \citep{ducas_crystals-dilithium_2018} y MQDSS \citep{chen_mqdss_2020} no presentaron implementaciones para L5, y FALCON \citep{falcon_falcon_2020} no presentó para L2, L3 y L4.

Existen variantes híbridas que combinan algoritmos post-cuánticos con curvas elípticas NIST tradicionales según su nivel de seguridad. Los niveles de seguridad para cada algoritmo propuesto pueden verse en \citep{openquantumsafeLiboqs2020}.

\begin{comment}

%	NTRU
%	NTRU-KE: A Lattice-based Public Key Exchange Protocol.
%	NTRU Post Quantum Cryptography
%	SIDH SIKE
%	https://en.wikipedia.org/wiki/Supersingular_isogeny_key_exchange



Usos posibles de la criptografía post-cuántica
Quantum Safe Cryptography and Security
Certificados x509, IKEc2, SSL/TLS1.2, S/MIME, SSH v2, VPN
Usos en SSL/TLS - Implementaciones de software (OpenSSL, WolfSSL, LibreSSL, BoringSSL)
Usos en VPN - Implementaciones de software (OpenVPN, SSH, StrongSwan/IPSec)
Achieving 128-bit security against quantum attacks in OpenVPN
File ssl_tls
Integrating Quantum Cryptography into SSL_335
Post-quantum key exchange for the TLS protocol from the ring learning with errors problem

\end{comment}
