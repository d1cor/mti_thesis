# Criptografía Post-Cuántica aplicada a entornos de producción Open Source
*Versión: 5.2*

## Tesis: Maestría en Teleinformática
### Facultad de Ingeniería - Universidad de Mendoza



### Resumen

Actualmente la mayoría de los protocolos de aplicación en Internet delegan su seguridad a la capa TLS por medio de implementaciones de software tales como OpenSSL y wolfSSL. Esta capa genera un canal seguro entre cliente y servidor para ofrecer privacidad, autenticidad e integridad a los datos que transitan la red. TLS utiliza criptografía asimétrica para lograr la autenticación e intercambio de las claves. Estos métodos se basan en supuestos de complejidad computacional, por lo que hacen uso de funciones matemáticas muy simples de calcular, pero extremadamente difíciles de revertir haciendo uso de los procesadores actuales. No obstante, estos mecanismos se consideran vulnerables al criptoanálisis realizado desde computadoras cuánticas. Aún así, existen algoritmos asimétricos que se consideran resistentes a este tipo de ataque: los algoritmos post-cuánticos.

Este trabajo estudió el estado del arte en el campo de la criptografía post-cuántica, mencionó los avance en el proceso de estandarización y los algoritmos candidatos más prometedores en la actualidad. Además, analizó las implementaciones de software y librerías de código abierto que han alcanzado mayor nivel de madurez, y que a su vez cuentan con un desarrollo activo. Se detallaron dos implementaciones de TLS: wolfSSL y las variantes de OpenSSL provistas por el proyecto Open Quantum Safe, OQS-OpenSSL; y se realizaron experimentos de integración de estas implementaciones con HTTP y OpenVPN como protocolos de capa de Aplicación.

A pesar de los avances que este campo ha tenido en los últimos años, esta tesis da cuenta del nivel experimental de las implementaciones de software disponibles y de cómo, tanto éstas como el desarrollo de nuevos estándares, se encuentran aún en etapas tempranas de su evolución.

### Estructura del repositorio:

El presente repositorio Git contiene tanto el documento escrito de mi tesis de Maestría en Teleinformática,  como los recursos utilizados y generados durante su elaboración.

Este repositorio contiene los siguientes elementos:

* `thesis_tex/`: Archivos fuente (LaTeX) del docuemento de tesis, y su archivos PDF compilado.
* `thesis_tex/img/`: Diagramas e imágenes utilizadas en el escrito.
* `keys/`: Claves públicas, privadas y certificados digitales utilizados durante los experimentos.
* `modulefiles/`: Archivos de configuración de módulos de entorno (env-modules) utilizados durante los experimentos.
* `algos/`: Listas completas de algoritmos y cipher suites soportados por cada una de las implementaciones de TLS y OpenVPN utilizadas en los experimentos.
* `etc/`: Archivos adicionales generados, utilizados en los experimentos y referenciados en el texto.

La última versión disponible del PDF de la tesis puede descargarse desde:
[thesis_tex/Main.pdf](thesis_tex/Main.pdf)

---

Esta obra está publicada bajo una Licencia **Creative Commons Atribución - CompartirIgual 4.0 Internacional** (CC BY-SA 4.0 Internacional). No puede usar estos archivos excepto en conformidad con la Licencia. Puede obtener una copia de la Licencia en [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).

*Diego Córdoba*

*Septiembre 24 de 2022.*
